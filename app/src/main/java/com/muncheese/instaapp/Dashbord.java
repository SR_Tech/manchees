package com.muncheese.instaapp;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.ui.AppBarConfiguration;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.muncheese.instaapp.activity.LoginScreen;
import com.muncheese.instaapp.fragment.AboutUS;
import com.muncheese.instaapp.fragment.ContactUs;
import com.muncheese.instaapp.fragment.HomeScreen;
import com.muncheese.instaapp.fragment.OrderListScreen;
import com.muncheese.instaapp.fragment.Profile;
import com.muncheese.instaapp.util.Comman;
import com.muncheese.instaapp.util.CustomDialogs;
import com.muncheese.instaapp.util.DialogsUtils;
import com.muncheese.instaapp.util.Preferences;
import com.muncheese.instaapp.util.RetrofitClient;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Dashbord extends AppCompatActivity implements View.OnClickListener {


    private AppBarConfiguration mAppBarConfiguration;
    BottomNavigationView bubbleNavigationLinearView;
    DrawerLayout drawer;
    NavigationView navigationView;
    Boolean doubleBackToExitPressedOnce = false;
    LinearLayout lly_myprofile, lly_aboutus, lly_contactus, lly_logout, lly_login;
    ImageView imgback;

    //    lly_myorder,
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashbord);
        forceUpdate();
        Preferences.appContext = getApplicationContext();
        drawer = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);


//        mAppBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.nav_pro, R.id.nav_ord, R.id.nav_about, R.id.nav_contact, R.id.nav_logout)
//                .setDrawerLayout(drawer)
//                .build();


        lly_myprofile = navigationView.findViewById(R.id.nav_pro);
//        lly_myorder = navigationView.findViewById(R.id.nav_ord);
        lly_aboutus = navigationView.findViewById(R.id.nav_about);
        lly_contactus = navigationView.findViewById(R.id.nav_contact);
        lly_logout = navigationView.findViewById(R.id.nav_logout);
        lly_login = navigationView.findViewById(R.id.nav_login);

        imgback = navigationView.findViewById(R.id.img_back);
        imgback.setOnClickListener(this);
        lly_myprofile.setOnClickListener(this);
//        lly_myorder.setOnClickListener(this);
        lly_aboutus.setOnClickListener(this);
        lly_contactus.setOnClickListener(this);
        lly_logout.setOnClickListener(this);
        lly_login.setOnClickListener(this);


        init();
    }


    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion, Dashbord.this).execute();
    }

    public class ForceUpdateAsync extends AsyncTask<String, String, JSONObject> {

        private String latestVersion;
        private String currentVersion;
        private Context context;

        public ForceUpdateAsync(String currentVersion, Context context) {
            this.currentVersion = currentVersion;
            this.context = context;
        }

        @Override
        protected JSONObject doInBackground(String... params) {

            try {
                latestVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(3) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
                Log.e("latestversion", "---" + latestVersion);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if (latestVersion != null) {

                Log.e("Dashboard", "Current version: "+currentVersion );
                Log.e("Dashboard", "Latest version: "+latestVersion );

                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    // Toast.makeText(context,"update is available.",Toast.LENGTH_LONG).show();
                    if (!(context instanceof Dashbord)) {
                        if (!((Activity) context).isFinishing()) {
                            showForceUpdateDialog();
                        }
                    }
                }
            }
            super.onPostExecute(jsonObject);
        }

        public void showForceUpdateDialog() {

            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
        }

    }

    void init() {

        if (Preferences.getUserProfile().equals("0")) {
            lly_myprofile.setVisibility(View.GONE);
//            lly_myorder.setVisibility(View.GONE);
            lly_aboutus.setVisibility(View.VISIBLE);
            lly_contactus.setVisibility(View.VISIBLE);
            lly_logout.setVisibility(View.GONE);
            lly_login.setVisibility(View.VISIBLE);

        } else if (Preferences.getUserProfile().equals("2")) {
            lly_myprofile.setVisibility(View.GONE);
//            lly_myorder.setVisibility(View.GONE);
            lly_aboutus.setVisibility(View.VISIBLE);
            lly_contactus.setVisibility(View.VISIBLE);
            lly_logout.setVisibility(View.VISIBLE);
            lly_login.setVisibility(View.GONE);
        } else {
            lly_myprofile.setVisibility(View.VISIBLE);
//            lly_myorder.setVisibility(View.VISIBLE);
            lly_aboutus.setVisibility(View.VISIBLE);
            lly_contactus.setVisibility(View.VISIBLE);
            lly_logout.setVisibility(View.VISIBLE);
            lly_login.setVisibility(View.GONE);
        }
        bottomnav();

    }

    void bottomnav() {
        bubbleNavigationLinearView = findViewById(R.id.navigation);
        if (Preferences.getUserProfile().equals("0")) {
            bubbleNavigationLinearView.findViewById(R.id.m_item_order).setVisibility(View.GONE);
        } else {
            bubbleNavigationLinearView.findViewById(R.id.m_item_order).setVisibility(View.VISIBLE);

        }
        bubbleNavigationLinearView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment selectedFragment = null;


                switch (menuItem.getItemId()) {
                    case R.id.m_item_home:
                        selectedFragment = new HomeScreen();


                        break;
                    case R.id.m_item_cart:
                        if (!Preferences.getUserProfile().equals("0")) {
                            selectedFragment = new CartScreen();
                        } else {
                            return false;

                        }


                        break;


                    case R.id.m_item_order:

                        selectedFragment = new OrderListScreen();


                        break;
                    case R.id.m_item_more: {
                        if (!drawer.isDrawerOpen(Gravity.RIGHT))
                            drawer.openDrawer(Gravity.RIGHT);
                        else drawer.closeDrawer(Gravity.RIGHT);
                        return false;
                    }


//
                }
                replaceFragment(selectedFragment);
                return true;
            }
        });


        replaceFragment(new HomeScreen());

    }


    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                finish();
                return;
            } else {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
//                finish();
            }
        } else {
            super.onBackPressed();
        }
    }


    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.parent_foter, fragment);
//        ft.addToBackStack(backStateName);
            ft.commit();
        }
    }


    private void dialogLogout() {
        Dialog dialog = CustomDialogs.dialogCancelOkBtn(this, getResources().getString(R.string.are_you_sure_want_to_logout));
        Button okbtn = dialog.findViewById(R.id.btn_ok);
        Button cancelbtn = dialog.findViewById(R.id.btn_cancel);
        okbtn.setText("Yes");
        cancelbtn.setText("No");
        okbtn.setOnClickListener(v1 -> {
            if (Comman.isConnectingToInternet(this)) {
                logout();
                dialog.dismiss();
            }
        });
        cancelbtn.setOnClickListener(v1 -> {
            dialog.dismiss();
        });
    }

    void logout() {
        ProgressDialog dialog = DialogsUtils.showProgressDialog(this, "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getapi()
                .logout(Preferences.gettotal());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

//
                if (response.code() == 200) {
                    dialog.dismiss();
                    Preferences.setLoginStatus(false);
                    Preferences.setUserProfile("0");
                    Preferences.cleare();
                    Intent intent = new Intent(Dashbord.this, LoginScreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } else {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public void onClick(View view) {
        Fragment selectedFragment = null;
        switch (view.getId()) {

            case R.id.nav_login: {

                Intent intent = new Intent(Dashbord.this, LoginScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                drawer.closeDrawers();

                //your code.

            }
            break;
            case R.id.nav_pro: {
                selectedFragment = new Profile();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.parent_foter, selectedFragment);
                transaction.commit();
                drawer.closeDrawers();

                //your code.

            }
            break;
//            case R.id.nav_ord: {
//                selectedFragment = new OrderListScreen();
//                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                transaction.replace(R.id.parent_foter, selectedFragment);
//                transaction.commit();
//                drawer.closeDrawers();
//
//                //your code.
//
//            }
//            break;
            case R.id.nav_about: {

                selectedFragment = new AboutUS();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.parent_foter, selectedFragment);
                transaction.commit();
                drawer.closeDrawers();

                //your code.

            }
            break;
            case R.id.nav_contact: {
                selectedFragment = new ContactUs();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.parent_foter, selectedFragment);
                transaction.commit();
                drawer.closeDrawers();

                //your code.

            }
            break;
            case R.id.nav_logout: {
                dialogLogout();
                drawer.closeDrawers();

                //your code.

            }
            break;
            case R.id.img_back: {
                if (!drawer.isDrawerOpen(Gravity.RIGHT))
                    drawer.openDrawer(Gravity.RIGHT);
                else drawer.closeDrawer(Gravity.RIGHT);

            }
            break;
        }
    }
}