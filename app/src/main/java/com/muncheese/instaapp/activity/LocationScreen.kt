package com.muncheese.instaapp.activity

import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muncheese.instaapp.R
import com.muncheese.instaapp.adpter.LocationAdpter
import com.muncheese.instaapp.model.request.LocationRequest
import com.muncheese.instaapp.model.response.LocationResponse
import com.muncheese.instaapp.util.CustomDialogs
import com.muncheese.instaapp.util.DialogsUtils
import com.muncheese.instaapp.util.Preferences
import com.muncheese.instaapp.util.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class LocationScreen : AppCompatActivity() {
    var rlc_cart: RecyclerView? = null
    var lolationListResponses: ArrayList<LocationResponse>? = null
    var locationAdpter: LocationAdpter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_screen)
        init()

    }

    fun init() {
        val headerText: TextView = findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "My Location"
        lolationListResponses = ArrayList<LocationResponse>()
        locationAdpter = LocationAdpter(lolationListResponses, this)
        rlc_cart = findViewById(R.id.rlc_loc_screen)
        rlc_cart!!.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rlc_cart!!.adapter = locationAdpter

        locationAdpter!!.setAdapterInterface { id, postion ->

        }


        getlocation()
    }

    fun getlocation() {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")

        val call: Call<LocationRequest> = RetrofitClient.getInstance().getapi()
            .location(
                "Token " + Preferences.getToken()
            )
        call.enqueue(object : Callback<LocationRequest?> {
            override fun onResponse(
                call: Call<LocationRequest?>,
                response: Response<LocationRequest?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var datas = ArrayList(Arrays.asList(*response.body()!!.results))
                    locationAdpter!!.updateAdapter(datas, this@LocationScreen)
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(this@LocationScreen)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        this@LocationScreen,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<LocationRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }
}