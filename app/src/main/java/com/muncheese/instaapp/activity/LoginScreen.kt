package com.muncheese.instaapp.activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.model.MainLoginResponse
import com.muncheese.instaapp.model.request.UserRestrout
import com.muncheese.instaapp.model.request.UseridRequest
import com.muncheese.instaapp.model.sendModel.EmailLogin
import com.muncheese.instaapp.model.sendModel.MainLogin
import com.muncheese.instaapp.model.sendModel.UserResetLogin
import com.muncheese.instaapp.model.sendModel.usermodel
import com.muncheese.instaapp.util.Comman
import com.muncheese.instaapp.util.DialogsUtils
import com.muncheese.instaapp.util.Preferences
import com.muncheese.instaapp.util.RetrofitClient
import kotlinx.android.synthetic.main.activity_login_screen.*
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class LoginScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_screen)
        init()
    }

    fun init() {
        if (Preferences.getpostcode()== null) {
            Preferences.setpostcode("0")
        }
        if (Preferences.getdips()== null) {
            Preferences.setdips("0")
        }
        if(Preferences.getpizzadeliver()==null) {
            Preferences.setpizzdeliver("0")

        }
        if (Preferences.getpizz2adeliver()==null){
            Preferences.setpizz2deliver("0")

        }
        if (Preferences.getpizzacolle()==null){
            Preferences.setpizzacolle("0")

        }
        click()
    }

    fun click() {
        forgot_user.setOnClickListener(View.OnClickListener {
            dialogForgotPasswordUser("user", this)
        })

        forgot_password.setOnClickListener(View.OnClickListener {
            dialogForgotPasswordUser("password", this)
        })
        btn_guest.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@LoginScreen, MobileVerication::class.java)
            startActivity(intent)
        })
        txt_sign_up.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@LoginScreen, Register::class.java)
            startActivity(intent)
        })
        btn_back.setOnClickListener {
            if (Preferences.getUserProfile()==null){
                Preferences.setUserProfile("0")
            }
            val intent = Intent(this@LoginScreen, Dashbord::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        btn_login.setOnClickListener(View.OnClickListener {
            if (edt_lemail.getText().toString().isEmpty()) {
                Comman.getToast(
                    this@LoginScreen,
                    resources.getString(R.string.enter_email_validate)
                )
            } else if (edt_lpassword.getText().toString().isEmpty()) {
                Comman.getToast(
                    this@LoginScreen,
                    getString(R.string.enter_password_validate),
                )

            } else {
                userlogincheck()
            }


        })
    }

    fun userlogincheck() {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")
        val call: Call<MainLoginResponse> = RetrofitClient.getInstance().getapi()
            .loginx(
                MainLogin(
                    edt_lemail.getText().toString(),
                    edt_lpassword.getText().toString(),
                    getResources().getString(R.string.restaurant_id)
                )
            )
        call.enqueue(object : Callback<MainLoginResponse> {
            override fun onResponse(
                call: Call<MainLoginResponse>,
                response: Response<MainLoginResponse?>
            ) {
                val p = response.body()




                if (response.code() == 200) {

                    myDialog.dismiss()
                    Preferences.setUserId(p?.getId().toString())
                    Preferences.setToken(p?.getToken())
                    Preferences.setLoginStatus(true)
                    Preferences.setUserProfile("1")
                    val intent = Intent(this@LoginScreen, Dashbord::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    Comman.getToast(
                        this@LoginScreen,
                        resources.getString(R.string.credential_wrong)
                    )
                } else if (response.code() == 403) {
//                    Log.d("TAG", "onResponse: " + response?.message())
//                    Log.d("TAG", "onResponse: " + response.body().toString())
                    val jObjError = JSONObject(response.errorBody()!!.string())
                    Log.d("TAG", "onResponse1: " + jObjError)
                    Log.d("TAG", "onResponse1: " + jObjError.get("msg"))
                    myDialog.dismiss()
                    if (jObjError.get("msg") == resources.getString(R.string.compaire_massage1)) {
                        retro("", this@LoginScreen)
                    } else {
                        Comman.getToast(this@LoginScreen, p?.msg)
                    }
                } else {
                    Comman.getToast(this@LoginScreen, resources.getString(R.string.no_response))
                }
            }

            override fun onFailure(call: Call<MainLoginResponse>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun mainlogin() {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")

        val call: Call<MainLoginResponse> = RetrofitClient.getInstance().getapi()
            .login(
                MainLogin(
                    getResources().getString(R.string.app_username),
                    getResources().getString(R.string.app_password),
                    getResources().getString(R.string.restaurantid)
                )
            )
        call.enqueue(object : Callback<MainLoginResponse> {
            override fun onResponse(
                call: Call<MainLoginResponse?>,
                response: Response<MainLoginResponse?>
            ) {

                if (response.code() == 200) {
                    val p: MainLoginResponse? = response.body()
                    myDialog.dismiss()
                    useridcall(p!!.token)
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    Comman.getToast(
                        this@LoginScreen,
                        resources.getString(R.string.credential_wrong)
                    )
                } else if (response.code() == 403) {
                    myDialog.dismiss()
                    Toast.makeText(
                        this@LoginScreen,
                        resources.getString(R.string.email_not_verified),
                        Toast.LENGTH_LONG
                    ).show()

                } else {
                    Comman.getToast(this@LoginScreen, resources.getString(R.string.no_response))
                }
            }

            override fun onFailure(call: Call<MainLoginResponse?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun useridcall(token: String) {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")

        val call: Call<UseridRequest> = RetrofitClient.getInstance().getapi()
            .getuser(
                "Token " + token, edt_lemail.getText().toString()
            )
        call.enqueue(object : Callback<UseridRequest?> {
            override fun onResponse(
                call: Call<UseridRequest?>,
                response: Response<UseridRequest?>
            ) {
                val p: UseridRequest? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    val p = response.body()
                    var s = ArrayList(Arrays.asList(*response.body()!!.results))

                    val id: Int = s[0].getId()
                    userdata(id, getResources().getString(R.string.restaurant_id), token)


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    Toast.makeText(
                        this@LoginScreen,
                        resources.getString(R.string.email_not_verified),
                        Toast.LENGTH_LONG
                    ).show()

                } else if (response.code() == 403) {
                    myDialog.dismiss()
                    Comman.getToast(
                        this@LoginScreen,
                        resources.getString(R.string.email_not_verified)
                    )


                } else {
                    myDialog.dismiss()
                    Comman.getToast(this@LoginScreen, resources.getString(R.string.no_response))

                }
            }

            override fun onFailure(call: Call<UseridRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }


    fun userdata(id: Int, retro_id: String, token: String) {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")

        val call: Call<UserRestrout> = RetrofitClient.getInstance().getapi()
            .insertuser(
                "Token " + token, usermodel(
                    id,
                    retro_id
                )
            )
        call.enqueue(object : Callback<UserRestrout?> {
            override fun onResponse(
                call: Call<UserRestrout?>,
                response: Response<UserRestrout?>
            ) {
                val p: UserRestrout? = response.body()
                if (response.code() == 201) {
                    myDialog.dismiss()
                    val p = response.body()

                    //  ArrayList<RetrarentResponse> s = new ArrayList<RetrarentResponse>(Arrays.asList(response.body().getResults()));
                    userlogincheck()

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    Comman.getToast(
                        this@LoginScreen,
                        resources.getString(R.string.credential_wrong)
                    )
                } else if (response.code() == 403) {
                    myDialog.dismiss()
                    Comman.getToast(
                        this@LoginScreen,
                        resources.getString(R.string.email_not_verified)
                    )


                } else {
                    myDialog.dismiss()
                    Comman.getToast(this@LoginScreen, resources.getString(R.string.no_response))

                }
            }

            override fun onFailure(call: Call<UserRestrout?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }


    fun retro(type: String?, context: Context?) {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog.setContentView(R.layout.retro_dilog)
        val edtEmail = dialog.findViewById<TextView>(R.id.txt_msg)
        dialog.setCancelable(true)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT

        edtEmail.text =
            resources.getString(R.string.confirm_user)
        (dialog.findViewById<View>(R.id.btn_ok) as Button).setOnClickListener { view: View? ->
            if (Comman.isConnectingToInternet(context)) {
                mainlogin()
            }
        }
        (dialog.findViewById<View>(R.id.btn_cancel) as Button).setOnClickListener { view: View? -> dialog.dismiss() }
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        dialog.window!!.attributes = lp
    }

    fun forgotemail(email: String) {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")

        val call: Call<ResponseBody> = RetrofitClient.getInstance().getapi()
            .username_reset(
                Preferences.getToken(),
                EmailLogin(
                    email
                )
            )
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {

                if (response.code() == 200) {

                    myDialog.dismiss()
                    Comman.getToast(this@LoginScreen, resources.getString(R.string.username_snt))
                } else if (response.code() == 400) {
                    myDialog.dismiss()
                    Comman.getToast(
                        this@LoginScreen,
                        resources.getString(R.string.credential_wrong)
                    )
                } else {
                    myDialog.dismiss()
                    Comman.getToast(
                        this@LoginScreen,
                        resources.getString(R.string.credential_wrong)
                    )
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun forgotpassword(email: String) {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")

        val call: Call<ResponseBody> = RetrofitClient.getInstance().getapi()
            .password_reset(
                Preferences.getToken(),
                UserResetLogin(
                    email
                )
            )
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    Comman.getToast(this@LoginScreen, resources.getString(R.string.password_snt))
                } else if (response.code() == 400) {
                    myDialog.dismiss()
                    Comman.getToast(
                        this@LoginScreen,
                        resources.getString(R.string.credential_wrong)
                    )
                } else {
                    myDialog.dismiss()
                    Comman.getToast(
                        this@LoginScreen,
                        resources.getString(R.string.credential_wrong)
                    )
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    //============================================dialogs ===========================
    fun dialogForgotPasswordUser(type: String, context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog.setContentView(R.layout.dialog_password_reset)
        val edtEmail = dialog.findViewById<EditText>(R.id.txt_msg)
        dialog.setCancelable(true)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        if (type.equals("password", ignoreCase = true)) edtEmail.hint =
            "Enter your username" else edtEmail.hint =
            "Enter your email"
        (dialog.findViewById<View>(R.id.btn_ok) as Button).setOnClickListener { view: View? ->
            if (Comman.isConnectingToInternet(context)) {
                if (!edtEmail.text.toString().equals("", ignoreCase = true)) {
                    if (type.equals(
                            "password",
                            ignoreCase = true
                        )
                    ) this.forgotpassword(
                        edtEmail.text.toString().toLowerCase()
                    ) else this.forgotemail(edtEmail.text.toString().toLowerCase())
                    dialog.dismiss()
                } else if (type.equals("password", ignoreCase = true)) Comman.getToast(
                    context,
                    context.resources.getString(R.string.pass_username)
                ) else Comman.getToast(
                    context,
                    context.resources.getString(R.string.pass_email)
                )
            }
        }
        (dialog.findViewById<View>(R.id.btn_cancel) as Button).setOnClickListener { view: View? -> dialog.dismiss() }
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        dialog.window!!.attributes = lp
    }
}