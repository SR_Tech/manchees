package com.muncheese.instaapp.activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.model.response.MobileVerifyResponse
import com.muncheese.instaapp.model.sendModel.MobileVerification
import com.muncheese.instaapp.model.sendModel.MobileVerify
import com.muncheese.instaapp.util.Comman
import com.muncheese.instaapp.util.DialogsUtils
import com.muncheese.instaapp.util.Preferences
import com.muncheese.instaapp.util.RetrofitClient
import kotlinx.android.synthetic.main.activity_login_screen.*
import kotlinx.android.synthetic.main.activity_mobile_verication.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MobileVerication : AppCompatActivity() {
    private var CountryID: String? = null
    var context: Context? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_verication)
        init()
    }

    fun init() {
        context = this@MobileVerication
        mobileservice()
        btn_mobilecancel.setOnClickListener {

            val intent = Intent(this@MobileVerication, LoginScreen::class.java)
            startActivity(intent)
        }
    }

    fun mobileservice() {
//        val manager = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
//        CountryID = manager.simCountryIso.toUpperCase()
//
//        CountryID = if (CountryID.equals("IN", ignoreCase = true)) "+91" else "+1"
//
//        edt_mcode.setText(CountryID)
        btn_msend_motp.setOnClickListener { v ->

            if (edt_m_mno.text?.length!! < 10) {
                Comman.getToast(this, "Enter valid mobile no.")
            } else if (edt_m_mno.text.toString().isEmpty()) {
                Comman.getToast(this, resources.getString(R.string.mb_validate))
            } else {
                if (Comman.isConnectingToInternet(this)) {

                    getOTP()
                }
            }
        }
    }

    fun getOTP() {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")

        val call: Call<ResponseBody> = RetrofitClient.getInstance().getapi()
            .getotp(
                MobileVerification(
                    edt_mcode.text.toString() +
                            edt_m_mno.text.toString()
                )

            )
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {
                val p: ResponseBody? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    dialogEnterOTP(context!!)
                    Comman.getToast(context, resources.getString(R.string.otp_send_successfully))
                } else {
                    Comman.getToast(context, resources.getString(R.string.no_response))

                    myDialog.dismiss()

                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun verifyOTP(otp: String) {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")

        val call: Call<MobileVerifyResponse> = RetrofitClient.getInstance().getapi()
            .verifyotp(
                MobileVerify(
                    edt_mcode.text.toString() + edt_m_mno.text.toString(),
                    otp
                )

            )
        call.enqueue(object : Callback<MobileVerifyResponse?> {
            override fun onResponse(
                call: Call<MobileVerifyResponse?>,
                response: Response<MobileVerifyResponse?>
            ) {
                val p: MobileVerifyResponse? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    Preferences.setUserId(p?.id.toString())
                    Preferences.setToken(p?.token.toString())
                    Preferences.setLoginStatus(true)
                    Preferences.setUserProfile("2")
                    val intent = Intent(context, Dashbord::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }else if (response.code() == 400) {
                    myDialog.dismiss()
                    Comman.getToast(context, "User verification failed")
                } else {
                    Comman.getToast(context, resources.getString(R.string.no_response))

                    myDialog.dismiss()
                }
            }

            override fun onFailure(call: Call<MobileVerifyResponse?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }


    fun dialogEnterOTP(context: Context): Dialog {
        val dialog = Dialog(context)
        try {
            dialog.setContentView(R.layout.dialog_with_editbox)
            val edtOTP = dialog.findViewById<View>(R.id.edt_text) as EditText
            val btnSubmit = dialog.findViewById<View>(R.id.bt_submit) as Button
            btnSubmit.text = "Submit"
            edtOTP.hint = "Enter the OTP"
            dialog.setCancelable(false)
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(dialog.window!!.attributes)
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            btnSubmit.setOnClickListener { view: View? ->
                if (Comman.isConnectingToInternet(context)) {

                    if (edtOTP.text.toString().isEmpty()) {
                        Comman.getToast(
                            context,
                            context.resources.getString(R.string.enter_otp_validate)
                        )
                    } else {
                        verifyOTP(edtOTP.text.toString())
                      //  dialog.dismiss()
                    }
                }
            }
            (dialog.findViewById<View>(R.id.btn_cancel) as Button).setOnClickListener { v: View? -> dialog.dismiss() }
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            dialog.window!!.attributes = lp
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dialog
    }

}