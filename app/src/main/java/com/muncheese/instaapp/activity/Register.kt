package com.muncheese.instaapp.activity

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.muncheese.instaapp.R
import com.muncheese.instaapp.model.MainLoginResponse
import com.muncheese.instaapp.model.request.UserCreateRequest
import com.muncheese.instaapp.model.request.UserRegistrationRequest
import com.muncheese.instaapp.model.sendModel.MainLogin
import com.muncheese.instaapp.model.sendModel.UserCreate
import com.muncheese.instaapp.model.sendModel.UserRegister
import com.muncheese.instaapp.util.Comman
import com.muncheese.instaapp.util.DialogsUtils
import com.muncheese.instaapp.util.RetrofitClient
import kotlinx.android.synthetic.main.activity_login_screen.*
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class Register : AppCompatActivity() {
    private var salutationArray: ArrayList<String>? = null
    var contex: Context? = null
    var token22: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }

    fun init() {
        contex = this@Register
        salutationArray = ArrayList()
        salutationArray!!.add("Mr.")
        salutationArray!!.add("Mrs.")
        salutationArray!!.add("Miss.")
        salutationArray!!.add("Ms.")



        clicks()
        vailidation()
    }

    fun clicks() {
        edt_rsalutation.setOnClickListener {
            setSalutation()
        }
        txt_already_registered.setOnClickListener {
            val intent = Intent(this@Register, LoginScreen::class.java)
            startActivity(intent)
            finish()
        }
    }

    fun vailidation() {
        btn_sign_up.setOnClickListener {

            if (edt_rsalutation.getText().toString().isEmpty()) {
                Comman.getToast(
                    contex,
                    resources.getString(R.string.salutation_validate)
                )
            } else if (edt_ruser_name.getText().toString().isEmpty()) {
                Comman.getToast(
                    contex,
                    resources.getString(R.string.username_validate)
                )
            } else if (edt_rf_name.getText().toString().isEmpty()) {
                Comman.getToast(
                    contex,
                    resources.getString(R.string.fname_validate)
                )
            } else if (edt_rl_name.getText().toString().isEmpty()) {
                Comman.getToast(
                    contex,
                    resources.getString(R.string.lname_validate)
                )
            } else if (edt_remail.getText().toString().isEmpty()|| Comman.isEnailValid(edt_remail.getText().toString())) {
                Comman.getToast(
                    contex,
                    resources.getString(R.string.enter_email_validate)
                )
            } else if (edt_rm_no.getText().toString().isEmpty()) {
                Comman.getToast(
                    contex,
                    resources.getString(R.string.mb_validate)
                )
            } else if (edt_rpassword.getText().toString().isEmpty()) {
                Comman.getToast(
                    contex,
                    resources.getString(R.string.password_validate)
                )
            } else if (edt_rpassword.getText().toString().length <= 8) {
                Comman.getToast(
                    contex,
                    resources.getString(R.string.password_validate_character)
                )
            } else if (edt_c_password.getText().toString().isEmpty()) {
                Comman.getToast(
                    contex,
                    resources.getString(R.string.confrm_password_validate)
                )
            } else if (!edt_rpassword.getText().toString()
                    .equals(edt_c_password.getText().toString())
            ) {
                Comman.getToast(
                    contex,
                    resources.getString(R.string.password_not_match_validate)
                )
            } else {

                if (Comman.isConnectingToInternet(contex))
                    userlogincheck()
//                val intent = Intent(this@Register, LoginScreen::class.java)
//                startActivity(intent)
//                finish()

            }


        }

    }

    fun userlogincheck() {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")
        val call: Call<MainLoginResponse> = RetrofitClient.getInstance().getapi()
            .login(
                MainLogin(
                    getResources().getString(R.string.app_username),
                    getResources().getString(R.string.app_password),
                    getResources().getString(R.string.restaurantid)
                )
            )
        call.enqueue(object : Callback<MainLoginResponse?> {
            override fun onResponse(
                call: Call<MainLoginResponse?>,
                response: Response<MainLoginResponse?>
            ) {
                val p: MainLoginResponse? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    token22 = p?.getToken().toString()

                    userregistration(p?.getToken().toString())
                } else {
                    myDialog.dismiss()
                    Comman.getToast(contex, resources.getString(R.string.no_response))
                }

            }

            override fun onFailure(call: Call<MainLoginResponse?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun userregistration(token: String) {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")
        val call: Call<UserRegistrationRequest> = RetrofitClient.getInstance().getapi()
            .register_user(
                "Token " + token,
                UserRegister(
                    edt_ruser_name.text.toString(),
                    edt_remail.text.toString(),
                    edt_rf_name.text.toString(),
                    edt_rl_name.text.toString(),
                    edt_rpassword.text.toString(),
                    "N",
                    getResources().getString(R.string.restaurant_id)
                )
            )
        call.enqueue(object : Callback<UserRegistrationRequest?> {
            override fun onResponse(
                call: Call<UserRegistrationRequest?>,
                response: Response<UserRegistrationRequest?>
            ) {
                val p: UserRegistrationRequest? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    dialogShowMsg(contex, p?.id.toString())

                } else if (response.code() == 400) {
                    myDialog.dismiss()
                    Comman.getToast(contex, resources.getString(R.string.user_exit_withinstaapp))
                } else {
                    myDialog.dismiss()
                    Comman.getToast(contex, resources.getString(R.string.no_response))
                }

            }

            override fun onFailure(call: Call<UserRegistrationRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun usercreate(id: String) {
        val myDialog = DialogsUtils.showProgressDialog(this, "Loading...")

        val call: Call<UserCreateRequest> = RetrofitClient.getInstance().getapi()
            .create_user(
                "Token " + token22,
                UserCreate(
                    id,
                    Comman.CurrentTimeStamp(),
                    "extra",
                    edt_rsalutation.text.toString(),
                    edt_code.text.toString() + edt_rm_no.text.toString(),
                    edt_rf_name.text.toString(),
                    edt_rl_name.text.toString(),
                    edt_remail.text.toString(),

                    )
            )
        call.enqueue(object : Callback<UserCreateRequest?> {
            override fun onResponse(
                call: Call<UserCreateRequest?>,
                response: Response<UserCreateRequest?>
            ) {
                val p: UserCreateRequest? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    startActivity(
                        Intent(
                            contex,
                            LoginScreen::class.java
                        ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    )
               } else if (response.code() == 400) {
                    myDialog.dismiss()
                    Comman.getToast(contex, resources.getString(R.string.email_already_present))
                } else {
                    myDialog.dismiss()
                    Comman.getToast(contex, resources.getString(R.string.no_response))
                }

            }

            override fun onFailure(call: Call<UserCreateRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun dialogShowMsg(context: Context?, text: String): Dialog? {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog.setContentView(R.layout.loginsuccefullydiaolog)
        val msgTxt = dialog.findViewById<TextView>(R.id.txt_msg)
        //        msgTxt.setText(text);
        dialog.setCancelable(false)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        (dialog.findViewById<View>(R.id.btn_ok) as Button).setOnClickListener {
            dialog.dismiss()
            usercreate(text)
        }
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        dialog.window!!.attributes = lp
        return dialog
    }

    //salutation dialog
    private fun setSalutation() {
        // setup the alert builder
        val builder = AlertDialog.Builder(contex)
        builder.setTitle("Select Salutation")
        // add a list
        builder.setItems(Comman.GetStringArray(salutationArray),
            DialogInterface.OnClickListener { dialog, position ->
                edt_rsalutation.setText(salutationArray?.get(position))
                dialog.dismiss()
            })
        // create and show the alert dialog
        val dialog = builder.create()
        dialog.show()
    }
}