package com.muncheese.instaapp.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.util.Preferences

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash_screen)
        Preferences.appContext = applicationContext
        val th: Thread = object : Thread() {
            override fun run() {
                try {
                    sleep(3000)
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    Log.d("TAG", "run: " + Preferences.getcartId())
                    if (Preferences.getpostcode() == null) {
                        Preferences.setpostcode("0")
                    }

                    if (Preferences.getUserProfile() == null) {
                        Preferences.setUserProfile("0")
                    }

                    if (Preferences.getdips() == null) {
                        Preferences.setdips("0")
                    }
                    if (Preferences.getLoginStatus()) {

                        val intent = Intent(this@SplashScreen, Dashbord::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        val intent = Intent(this@SplashScreen, Dashbord::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
            }
        }
        th.start()
    }


}