package com.muncheese.instaapp.adpter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.muncheese.instaapp.R
import com.muncheese.instaapp.model.fetchsize.Size
import com.muncheese.instaapp.model.productaddon.Addon
import com.muncheese.instaapp.util.Comman
import kotlinx.android.synthetic.main.single_item_base.view.*
import kotlinx.android.synthetic.main.single_item_size.view.*

class BaseAdpter(val adapterOnClick: AdapterholderOnClick) : ListAdapter<Addon, BaseAdpter.BaseViewHolder>(DiffUtil()) {

    class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: Addon) {
            val name=itemView.findViewById<TextView>(R.id.txtbasename)
            val dis=itemView.findViewById<TextView>(R.id.txtbasedis)

            name.setText(item.addon_title)
            if (!item.selection_method.equals("any one")){
                dis.setText("Select "+item.selection_method+" options")

            }else{
                dis.setText("Select "+item.selection_method)

            }
        }
    }

    @SuppressLint("ResourceType")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_base, parent, false)
        return BaseViewHolder(view)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)


        holder.itemView.lly_base.setOnClickListener {
            adapterOnClick.onholdrClick(item.addon_id,item.selection_method,item.addon_title,holder, position)
        }
    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<Addon>() {
        override fun areItemsTheSame(oldItem: Addon, newItem: Addon): Boolean {
            return oldItem.addon_id == newItem.addon_id
        }
        override fun areContentsTheSame(oldItem: Addon, newItem: Addon): Boolean {
            return oldItem == newItem
        }
    }

}