package com.muncheese.instaapp.adpter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.muncheese.instaap.fragment.DetailScreen;
import com.muncheese.instaapp.Dashbord;
import com.muncheese.instaapp.R;
import com.muncheese.instaapp.fragment.EditFragment;
import com.muncheese.instaapp.model.cartlist.AddonContent;
import com.muncheese.instaapp.model.cartlist.Result;
import com.muncheese.instaapp.util.Comman;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CartAdpter extends RecyclerView.Adapter<CartAdpter.AllIndiaTestHolder> {

    Context context;
    List<Result> cartListResponses;
    int count = 1;

    FilterItemInterface filterItemInterface;

    public CartAdpter(List<Result> cartListResponses, Context context) {
        this.cartListResponses = cartListResponses;
        this.context = context;
    }


    public void updateAdapter(List<Result> cartListResponses, Context context) {
        this.cartListResponses = cartListResponses;
        this.context = context;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllIndiaTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_item_cart, parent, false);
        AllIndiaTestHolder holder = new AllIndiaTestHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull AllIndiaTestHolder holder, @SuppressLint("RecyclerView") int position) {
        if (cartListResponses.get(position).getProduct().getProduct_name() != null)
            holder.txttital.setText(cartListResponses.get(position).getProduct().getProduct_name());

        if (cartListResponses.get(position).getQuantity() != 0)
            holder.txtquty.setText(cartListResponses.get(position).getQuantity() + "");


        if (cartListResponses.get(position).getProduct().getCategory() == 323) {
            if (cartListResponses.get(position).getProduct().getProduct_url() != null && !cartListResponses.get(position).getProduct().getProduct_url().equals(""))

                Picasso.get().load(cartListResponses.get(position).getProduct().getProduct_url()).into(holder.img);

        } else if (cartListResponses.get(position).getProduct().getCategory() == 327) {
            if (cartListResponses.get(position).getProduct().getProduct_url() != null && !cartListResponses.get(position).getProduct().getProduct_url().equals(""))

                Picasso.get().load(cartListResponses.get(position).getProduct().getProduct_url()).into(holder.img);

        } else if (cartListResponses.get(position).getProduct().getCategory() == 329) {
            if (cartListResponses.get(position).getProduct().getProduct_url() != null && !cartListResponses.get(position).getProduct().getProduct_url().equals(""))

                Picasso.get().load(cartListResponses.get(position).getProduct().getProduct_url()).into(holder.img);

        } else if (cartListResponses.get(position).getProduct().getCategory() == 330) {
            if (cartListResponses.get(position).getProduct().getProduct_url() != null && !cartListResponses.get(position).getProduct().getProduct_url().equals(""))

                Picasso.get().load(cartListResponses.get(position).getProduct().getProduct_url()).into(holder.img);

        } else {
            holder.img.setVisibility(View.GONE);
        }

        holder.price.setText(Comman.currencyType + cartListResponses.get(position).getPrice());

        String strIngredient = "";
        List<AddonContent> ingredientarray = cartListResponses.get(position).getAddon_content();
        Log.d("TAG", "onBindViewHolder: " + cartListResponses.get(position).getAddon_content().size());
        int a = cartListResponses.get(position).getAddon_content().size();
        double ingrprice = 0;
        double ingrsingleprice = 0;
        for (int i = 0; i < a; i++) {
            Log.d("TAG", "onBindViewHolder: " + i);
            if (ingredientarray.get(i).getAddon_content().getParent_addon().getAddon_title().equals("Toppings") || ingredientarray.get(i).getAddon_content().getParent_addon().getAddon_title().equals("Extra Toppings")) {
              if(cartListResponses.get(position).getSize()!=null){
                  if (cartListResponses.get(position).getSize().getSize().equals("Small")) {
                      int qty = cartListResponses.get(position).getQuantity();
                      double price = (1.10) * qty;
                      String s = String.format("%.2f", price);
                      ingrprice += price;
                      ingrsingleprice += 1.10;
                      strIngredient = strIngredient + (i + 1) + "." + ingredientarray.get(i).getAddon_content().getParent_addon().getAddon_title() + " :" + ingredientarray.get(i).getAddon_content().getTitle() +
                              " " + Comman.currencyType + s + " \n";


                  } else if (cartListResponses.get(position).getSize().getSize().equals("Medium")) {
                      int qty = cartListResponses.get(position).getQuantity();
                      double price = (1.20) * qty;
                      String s = String.format("%.2f", price);
                      ingrprice += price;
                      ingrsingleprice += 1.20;
//                    Double prodprice = Double.valueOf(cartListResponses.get(position).getPrice());
//                    double finalprice = prodprice + price;
//                    String p = String.format("%.2f", finalprice);
//
//                    holder.price.setText(Comman.currencyType + p);

                      strIngredient = strIngredient + (i + 1) + "." + ingredientarray.get(i).getAddon_content().getParent_addon().getAddon_title() + " :" + ingredientarray.get(i).getAddon_content().getTitle() +
                              " " + Comman.currencyType + s + " \n";

                  } else if (cartListResponses.get(position).getSize().getSize().equals("Large")) {


                      int qty = cartListResponses.get(position).getQuantity();
                      double price = (1.30) * qty;
                      String s = String.format("%.2f", price);

                      ingrprice += price;
                      ingrsingleprice += 1.30;
                      strIngredient = strIngredient + (i + 1) + "." + ingredientarray.get(i).getAddon_content().getParent_addon().getAddon_title() + " :" + ingredientarray.get(i).getAddon_content().getTitle() +
                              " " + Comman.currencyType + s + " \n";


                  } else {
                      Double price = Double.valueOf(ingredientarray.get(i).getAddon_content().getPrice());
                      int qty = cartListResponses.get(position).getQuantity();
                      ingrprice += price * qty;
                      ingrsingleprice += price;
                      strIngredient = strIngredient + (i + 1) + "." + ingredientarray.get(i).getAddon_content().getParent_addon().getAddon_title() + " :" + ingredientarray.get(i).getAddon_content().getTitle() +
                              " " + Comman.currencyType + (price * qty) + " \n";
                  }
              } else {
                  Double price = Double.valueOf(ingredientarray.get(i).getAddon_content().getPrice());
                  int qty = cartListResponses.get(position).getQuantity();
                  ingrprice += price * qty;
                  String s = String.format("%.2f", (price * qty));

                  ingrsingleprice += price;
                  strIngredient = strIngredient + (i + 1) + "." + ingredientarray.get(i).getAddon_content().getParent_addon().getAddon_title() + " :" + ingredientarray.get(i).getAddon_content().getTitle() +
                          " " + Comman.currencyType + s + " \n";
              }
              }else {
                Double price = Double.valueOf(ingredientarray.get(i).getAddon_content().getPrice());
                int qty = cartListResponses.get(position).getQuantity();
                ingrprice += price * qty;
                String s = String.format("%.2f", (price * qty));

                ingrsingleprice += price;
                strIngredient = strIngredient + (i + 1) + "." + ingredientarray.get(i).getAddon_content().getParent_addon().getAddon_title() + " :" + ingredientarray.get(i).getAddon_content().getTitle() +
                        " " + Comman.currencyType + s + " \n";
            }


        }
        Log.d("TAG", "onBindViewHolderadd: " + ingrprice);
        if (ingredientarray.size() > 0) {
            holder.linearLayout.setVisibility(View.VISIBLE);
        } else {
            holder.linearLayout.setVisibility(View.GONE);
        }
        if (cartListResponses.get(position).getSize() != null)
            holder.product_size.setText("Size :" + cartListResponses.get(position).getSize().getSize());

        holder.txt_detail_see_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.txt_detail_see_more.getText().toString().equals("See More")) {
                    holder.txt_ingredient.setMaxLines(Integer.MAX_VALUE);//your TextView
                    holder.txt_detail_see_more.setText("Show Less");
                } else {
                    holder.txt_ingredient.setMaxLines(3);//your TextView
                    holder.txt_detail_see_more.setText("See More");
                }
            }
        });
        holder.txt_ingredient.setText(strIngredient);


        holder.img_productdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String catid = cartListResponses.get(position).getCart_item_id() + "";

                filterItemInterface.getSelectedItemPosition(catid, position, "", 0.0, "", "delete", "");
            }
        });


        double finalIngrprice = ingrprice;
        double finalIngrprice2 = ingrsingleprice;
        holder.txtadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count > 0) {
                    if (cartListResponses.get(position).getSize() == null) {
                        String catid = cartListResponses.get(position).getProduct().getProduct_id() + "";

                        String cartitemid = cartListResponses.get(position).getCart_item_id() + "";
                        Double prodprice = Double.valueOf(cartListResponses.get(position).getProduct_price());
                        int qty = cartListResponses.get(position).getQuantity();

                        count = qty + 1;
                        Double b = java.lang.Double.valueOf(
                                holder.price.getText().toString()
                                        .replace(Comman.currencyType, "")
                        );
                        Log.d("TAG", "onClick:add1 " + finalIngrprice2);
                        Log.d("TAG", "onClick:add1 " + finalIngrprice);
                        Double price = ( prodprice+b+finalIngrprice2);


                        filterItemInterface.getSelectedItemPosition(catid, position, "", price, String.valueOf(count), "", cartitemid);

                    } else {
                        String catid = cartListResponses.get(position).getProduct().getProduct_id() + "";
                        String seqid = cartListResponses.get(position).getSize().getCategory_size_id() + "";

                        String cartitemid = cartListResponses.get(position).getCart_item_id() + "";
                        Double prodprice = Double.valueOf(cartListResponses.get(position).getProduct_price());
                        int qty = cartListResponses.get(position).getQuantity();

                        count = qty + 1;
                        Double b = java.lang.Double.valueOf(
                                holder.price.getText().toString()
                                        .replace(Comman.currencyType, "")
                        );
                        Log.d("TAG", "onClick:add1 " + finalIngrprice2);
                        Double price = (prodprice+b) +  finalIngrprice2;

//                        Log.d("TAG", "onClick: aad2" + finalIngrprice);
//                        Log.d("TAG", "onClick: aad3" + (count * prodprice));
//
//                        Log.d("TAG", "onClick:add4 " + price.toString());
                        filterItemInterface.getSelectedItemPosition(catid, position, seqid, price, String.valueOf(count), "", cartitemid);

                    }

                }
//                filterItemInterface.getSelectedItemPosition(catid, "", seqid);

            }
        });
        holder.txtminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = cartListResponses.get(position).getQuantity();
                count = qty;
                if (count != 1) {
                    if (cartListResponses.get(position).getSize() == null) {
                        String catid = cartListResponses.get(position).getProduct().getProduct_id() + "";

                        String cartitemid = cartListResponses.get(position).getCart_item_id() + "";
                        Double prodprice = Double.valueOf(cartListResponses.get(position).getProduct_price());
                        //  int qty = cartListResponses.get(position).getQuantity();
                        count = qty - 1;
                        Double b = java.lang.Double.valueOf(
                                holder.price.getText().toString()
                                        .replace(Comman.currencyType, "")
                        );
                        Double price = b-  (prodprice+finalIngrprice2);
                        filterItemInterface.getSelectedItemPosition(catid, position, "", price, String.valueOf(count), "", cartitemid);

                    } else {
                        String catid = cartListResponses.get(position).getProduct().getProduct_id() + "";
                        String seqid = cartListResponses.get(position).getSize().getCategory_size_id() + "";

                        String cartitemid = cartListResponses.get(position).getCart_item_id() + "";
                        Double prodprice = Double.valueOf(cartListResponses.get(position).getProduct_price());

                        //  int qty = cartListResponses.get(position).getQuantity();

                        Log.d("TAG", "onClick: " + finalIngrprice2 + "");
                        count = qty - 1;
                        Log.d("TAG", "onClick: " + count + "");
                        Log.d("TAG", "onClick: " + prodprice + "");
                        Double b = java.lang.Double.valueOf(
                                holder.price.getText().toString()
                                        .replace(Comman.currencyType, "")
                        );
                        Double price =b-  (prodprice+finalIngrprice2);
                        Log.d("TAG", "onClick: " + price.toString());
                        filterItemInterface.getSelectedItemPosition(catid, position, seqid, price, String.valueOf(count), "", cartitemid);

                    }
                } else {
                    if (cartListResponses.get(position).getSize() == null) {
                        String catid = cartListResponses.get(position).getCart_item_id() + "";
//                        String seqid = cartListResponses.get(position).getSize().getCategory_size_id() + "";
                        filterItemInterface.getSelectedItemPosition(catid, position, "", 0.0, "", "delete", "");
                    }else {
                        String catid = cartListResponses.get(position).getCart_item_id() + "";
                        String seqid = cartListResponses.get(position).getSize().getCategory_size_id() + "";
                        filterItemInterface.getSelectedItemPosition(catid, position, seqid, 0.0, "", "delete", "");

                    }
                }
            }
        });
        holder.img_product_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dashbord myActivity = (Dashbord) context;
                EditFragment fragment2 = new EditFragment();
                Bundle bundle = new Bundle();

                bundle.putInt("postion",holder.getAdapterPosition());
                bundle.putSerializable("data", (Serializable) cartListResponses);
                fragment2.setArguments(bundle);

                FragmentManager fragmentManager = myActivity.getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.parent_foter, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

    }

    public void clearProductLiist() {
        cartListResponses = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return cartListResponses.size();
    }

    public class AllIndiaTestHolder extends RecyclerView.ViewHolder {

        TextView txttital;
        TextView price, product_size;
        ImageView img, img_productdelete, img_product_edit;
        TextView txtadd;
        TextView txtquty;
        TextView txtminus;

        LinearLayout linearLayout;
        TextView txt_ingredient;
        TextView txt_detail_see_more;


        public AllIndiaTestHolder(View itemView) {
            super(itemView);
            txttital = itemView.findViewById(R.id.txt_product_name);
            price = itemView.findViewById(R.id.txt_item_cost);
            img = itemView.findViewById(R.id.icon_dash2);
            img_productdelete = itemView.findViewById(R.id.img_product_delete);


            txtadd = itemView.findViewById(R.id.btn_add);
            txtquty = itemView.findViewById(R.id.txt_quntity);
            txtminus = itemView.findViewById(R.id.btn_minus);
            linearLayout = itemView.findViewById(R.id.ll_ingredient);
            txt_ingredient = itemView.findViewById(R.id.txt_ingredient);
            txt_detail_see_more = itemView.findViewById(R.id.txt_detail_see_more);

            product_size = itemView.findViewById(R.id.productcart_size);
            img_product_edit = itemView.findViewById(R.id.img_product_edit);

        }
    }

    public interface FilterItemInterface {
        void getSelectedItemPosition(String product_id, int postion, String sequence, double price, String count, String check, String cartitemid);

    }

    public void setAdapterInterface(FilterItemInterface filterItemInterface) {
        this.filterItemInterface = filterItemInterface;
    }
}
