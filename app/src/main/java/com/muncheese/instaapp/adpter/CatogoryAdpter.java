package com.muncheese.instaapp.adpter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.muncheese.instaapp.R;
import com.muncheese.instaapp.model.request.CategoryListRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class CatogoryAdpter extends RecyclerView.Adapter<CatogoryAdpter.AllIndiaTestHolder> {

    Context context;
    ArrayList<CategoryListRequest> categoryResponses;
    FilterItemInterface filterItemInterface;
    String status;

    public CatogoryAdpter(ArrayList<CategoryListRequest> categoryResponses, Context context, String status) {
        this.categoryResponses = categoryResponses;
        this.context = context;
        this.status = status;
    }


    public void updateAdapter(ArrayList<CategoryListRequest> categoryResponses, Context context, String status) {
        this.categoryResponses = categoryResponses;
        this.context = context;
        this.status = status;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllIndiaTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_item_category, parent, false);
        AllIndiaTestHolder holder = new AllIndiaTestHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull AllIndiaTestHolder holder, int position) {
        if (status.equals("Closed")) {
            holder.linearLayout.setAlpha(Float.valueOf((float) 0.3));
            if (categoryResponses.get(position).getCategory() != null)
                holder.txttital.setText(categoryResponses.get(position).getCategory());
            if (categoryResponses.get(position).getCategoryUrl() != null)
                Picasso.get().load(categoryResponses.get(position).getCategoryUrl()).into(holder.image);
        } else {
            if (categoryResponses.get(position).getCategory() != null)
                holder.txttital.setText(categoryResponses.get(position).getCategory());
            if (categoryResponses.get(position).getCategoryUrl() != null)
                Picasso.get().load(categoryResponses.get(position).getCategoryUrl()).into(holder.image);
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("TAG", "init: " + "njnkjnskl111");
                        String catid = categoryResponses.get(position).getCategoryId().toString();
                        String catname = categoryResponses.get(position).getCategory().toString();
                        filterItemInterface.getSelectedItemPosition(catid, catname);


                }
            });
        }


    }

    public void clearProductLiist() {
        categoryResponses = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return categoryResponses.size();
    }

    public class AllIndiaTestHolder extends RecyclerView.ViewHolder {


        TextView txttital;
        RoundedImageView image;
        LinearLayout linearLayout;

        public AllIndiaTestHolder(View itemView) {
            super(itemView);
            txttital = itemView.findViewById(R.id.tab_category);
            image = itemView.findViewById(R.id.category_img);
//            txttital = itemView.findViewById(R.id.txt_menu_option);
            linearLayout = itemView.findViewById(R.id.lly_cate);
        }
    }

    public interface FilterItemInterface {
        void getSelectedItemPosition(String cat_id, String postion);

    }

    public void setAdapterInterface(FilterItemInterface filterItemInterface) {
        this.filterItemInterface = filterItemInterface;
    }
}
