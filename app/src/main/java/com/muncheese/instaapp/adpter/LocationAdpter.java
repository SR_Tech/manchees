package com.muncheese.instaapp.adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.muncheese.instaapp.R;
import com.muncheese.instaapp.model.response.LocationResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class LocationAdpter extends RecyclerView.Adapter<LocationAdpter.AllIndiaTestHolder> {

    Context context;
    ArrayList<LocationResponse> lolationListResponses;

   FilterItemInterface filterItemInterface;
    public LocationAdpter(ArrayList<LocationResponse> lolationListResponses, Context context) {
        this.lolationListResponses = lolationListResponses;
        this.context = context;
    }


    public void updateAdapter(ArrayList<LocationResponse> lolationListResponses, Context context) {
        this.lolationListResponses = lolationListResponses;
        this.context = context;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllIndiaTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_item_location, parent, false);
        AllIndiaTestHolder holder = new AllIndiaTestHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull AllIndiaTestHolder holder, int position) {

        if (lolationListResponses.get(position).getName() != null)
            holder.txt_locname.setText(lolationListResponses.get(position).getName());
        if (lolationListResponses.get(position).getCity() != null)
            holder.txt_locationcity.setText(lolationListResponses.get(position).getCity());
        if (lolationListResponses.get(position).getAddress() != null)
            holder.txt_locaddress.setText(lolationListResponses.get(position).getAddress());
        if (lolationListResponses.get(position).getRestaurantUrl() != null && !lolationListResponses.get(position).getRestaurantUrl().equals(""))
            Picasso.get().load(lolationListResponses.get(position).getRestaurantUrl()).into(holder.img_location);
        holder.btn_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String retroname = lolationListResponses.get(position).getAddress().toString();
                String retroid = lolationListResponses.get(position).getRestaurantId().toString();
                filterItemInterface.getSelectedItemPosition(retroname, retroid);
            }
        });

    }

    public void clearProductLiist() {
        lolationListResponses = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return lolationListResponses.size();
    }

    public class AllIndiaTestHolder extends RecyclerView.ViewHolder {


        RelativeLayout linearLayout;
        RoundedImageView img_location;
        TextView txt_locname;
        TextView txt_locationcity;
        TextView txt_locaddress;
        Button btn_location;

        public AllIndiaTestHolder(View itemView) {
            super(itemView);

            txt_locname = itemView.findViewById(R.id.txt_locname);
            txt_locationcity = itemView.findViewById(R.id.txt_locationcity);
            txt_locaddress = itemView.findViewById(R.id.txt_locaddress);
            btn_location = itemView.findViewById(R.id.btn_location);

            linearLayout = itemView.findViewById(R.id.rly_location);
            img_location = itemView.findViewById(R.id.img_location);

        }
    }
    public interface FilterItemInterface {
        void getSelectedItemPosition(String id, String postion);

    }

    public void setAdapterInterface(FilterItemInterface filterItemInterface) {
        this.filterItemInterface = filterItemInterface;
    }
}
