package com.muncheese.instaapp.adpter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.muncheese.instaap.fragment.DetailScreen;
import com.muncheese.instaapp.Dashbord;
import com.muncheese.instaapp.R;

import com.muncheese.instaapp.model.response.MenuResponse;
import com.muncheese.instaapp.util.Comman;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MenuAdpter extends RecyclerView.Adapter<MenuAdpter.AllIndiaTestHolder> {

    Context context;
    ArrayList<MenuResponse> menuResponses;
    String status;


    public MenuAdpter(ArrayList<MenuResponse> menuResponses, Context context, String status) {
        this.menuResponses = menuResponses;
        this.context = context;
        this.status = status;
    }


    public void updateAdapter(ArrayList<MenuResponse> menuResponses, Context context, String status) {
        this.menuResponses = menuResponses;
        this.context = context;
        this.status = status;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllIndiaTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_item_menu, parent, false);
        AllIndiaTestHolder holder = new AllIndiaTestHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull AllIndiaTestHolder holder, @SuppressLint("RecyclerView") int position) {
        if (status.equals("Closed")) {
            holder.linearLayout.setAlpha(Float.valueOf((float) 0.3));
            if (menuResponses.get(position).getProductName() != null) {
                holder.txttital.setText(menuResponses.get(position).getProductName());
            } else {
                holder.txttital.setText(menuResponses.get(position).getProductName1());

            }

            if (menuResponses.get(position).getExtra() != null)
                holder.txtdis.setText(menuResponses.get(position).getExtra());
            if (menuResponses.get(position).getPrice() != null)
                holder.txtprice.setText(Comman.currencyType + menuResponses.get(position).getPrice());


//            if (menuResponses.get(position).getCategory1() != null) {
//                if (menuResponses.get(position).getCategory1().getCategory() == "323") {
//                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))
//
//                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);
//
//                } else if (menuResponses.get(position).getCategory1().getCategory() == "327") {
//                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))
//
//                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);
//
//
//                } else if (menuResponses.get(position).getCategory1().getCategory() == "329") {
//                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))
//
//                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);
//
//                } else if (menuResponses.get(position).getCategory1().getCategory() == "330") {
//                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))
//
//                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);
//
//                } else {
//                    holder.image.setVisibility(View.GONE);
//                }
//            } else {

                if (menuResponses.get(position).getCategory() == 323) {
                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))

                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);

                } else if (menuResponses.get(position).getCategory() == 327) {
                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))

                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);


                } else if (menuResponses.get(position).getCategory() == 329) {
                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))

                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);

                } else if (menuResponses.get(position).getCategory() == 330) {
                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))

                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);

                } else {
                    holder.image.setVisibility(View.GONE);
                }
//            }
//            if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals("")) {
//                Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);
//            } else {
//                Picasso.get().load(menuResponses.get(position).getProductUrl1()).into(holder.image);
//            }
        } else {
            if (menuResponses.get(position).getProductName() != null) {
                holder.txttital.setText(menuResponses.get(position).getProductName());
            } else {
                holder.txttital.setText(menuResponses.get(position).getProductName1());

            }

            if (menuResponses.get(position).getExtra() != null)
                holder.txtdis.setText(menuResponses.get(position).getExtra());
            if (menuResponses.get(position).getPrice() != null)
                holder.txtprice.setText(Comman.currencyType + menuResponses.get(position).getPrice());

//            if (menuResponses.get(position).getCategory1() != null) {
//                if (menuResponses.get(position).getCategory1().getCategory() == "323") {
//                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))
//
//                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);
//
//                } else if (menuResponses.get(position).getCategory1().getCategory() == "327") {
//                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))
//
//                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);
//
//
//                } else if (menuResponses.get(position).getCategory1().getCategory() == "329") {
//                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))
//
//                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);
//
//                } else if (menuResponses.get(position).getCategory1().getCategory() == "330") {
//                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))
//
//                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);
//
//                } else {
//                    holder.image.setVisibility(View.GONE);
//                }
//            } else {
                if (menuResponses.get(position).getCategory() == 323) {
                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))

                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);

                } else if (menuResponses.get(position).getCategory() == 327) {
                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))

                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);


                } else if (menuResponses.get(position).getCategory() == 329) {
                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))

                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);

                } else if (menuResponses.get(position).getCategory() == 330) {
                    if (menuResponses.get(position).getProductUrl() != null && !menuResponses.get(position).getProductUrl().equals(""))

                        Picasso.get().load(menuResponses.get(position).getProductUrl()).into(holder.image);

                } else {
                    holder.image.setVisibility(View.GONE);
                }
//            }


            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String id, producturl, productname;
                    if (menuResponses.get(position).getProductId() != null) {
                        id = String.valueOf(menuResponses.get(position).getProductId());
                    } else {
                        id = String.valueOf(menuResponses.get(position).getProductid());
                    }
                    if (menuResponses.get(position).getProductUrl() != null) {
                        producturl = String.valueOf(menuResponses.get(position).getProductUrl());
                    } else {
                        producturl = String.valueOf(menuResponses.get(position).getProductUrl1());
                    }
                    if (menuResponses.get(position).getProductName() != null) {
                        productname = String.valueOf(menuResponses.get(position).getProductName());
                    } else {
                        productname = String.valueOf(menuResponses.get(position).getProductName1());
                    }
                    int catid = menuResponses.get(position).getCategory();

                    String prise = String.valueOf(menuResponses.get(position).getPrice());


                    Log.d("TAG", "onClick: ");
                    Dashbord myActivity = (Dashbord) context;
                    DetailScreen fragment2 = new DetailScreen();
                    Bundle bundle = new Bundle();
                    bundle.putString("id", id);
                    bundle.putInt("catid", catid);
                    bundle.putString("producturl", producturl);
                    bundle.putString("prise", prise);
                    bundle.putString("productname", productname);
                    fragment2.setArguments(bundle);

                    FragmentManager fragmentManager = myActivity.getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.parent_foter, fragment2);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });
        }


    }

    public void clearProductLiist() {
        menuResponses = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return menuResponses.size();
    }

    public class AllIndiaTestHolder extends RecyclerView.ViewHolder {

        TextView txttital;
        TextView txtdis;
        TextView txtprice;
        ImageView image;
        LinearLayout linearLayout;

        public AllIndiaTestHolder(View itemView) {
            super(itemView);

            txttital = itemView.findViewById(R.id.menu_name);
            txtdis = itemView.findViewById(R.id.menu_desc);
            txtprice = itemView.findViewById(R.id.price);
            image = itemView.findViewById(R.id.image);
//            txttital = itemView.findViewById(R.id.txt_menu_option);
            linearLayout = itemView.findViewById(R.id.lly_menu);
        }
    }

}
