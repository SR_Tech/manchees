package com.muncheese.instaapp.adpter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.muncheese.instaapp.R
import com.muncheese.instaapp.model.productcontain.ProductContainItem
import com.muncheese.instaapp.util.Comman
import kotlinx.android.synthetic.main.single_item_multiselection.view.*
import kotlinx.android.synthetic.main.single_item_size.view.*

class MultiselectionContainAdpter(val onmultiClick: AdapterOnmultiClick,val size:String) : ListAdapter<ProductContainItem, MultiselectionContainAdpter.BooksViewHolder>(DiffUtil()) {

    class BooksViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: ProductContainItem) {
            val name = itemView.findViewById<TextView>(R.id.txt_check)


        }


    }


    @SuppressLint("ResourceType")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksViewHolder {
        val view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.single_item_multiselection, parent, false)
        return BooksViewHolder(view)
    }

    override fun onBindViewHolder(holder: BooksViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
        if (size == "1") {
            holder.itemView.txt_check.setText(item.title+"("+Comman.currencyType+"1.10"+")")

        } else if (size == "2") {
            holder.itemView.txt_check.setText(item.title+"("+Comman.currencyType+"1.20"+")")


        } else if (size == "3") {
            holder.itemView.txt_check.setText(item.title+"("+Comman.currencyType+"1.30"+")")

        }else{
            holder.itemView.txt_check.setText(item.title+"("+Comman.currencyType+item.price+")")

        }
        holder.itemView.txt_check.setOnClickListener {
            if (size == "1") {
                onmultiClick.onmultiClick(item.addon_content_id,item.title,"1.10")

            } else if (size == "2") {

                onmultiClick.onmultiClick(item.addon_content_id,item.title,"1.20")

            } else if (size == "3") {
                onmultiClick.onmultiClick(item.addon_content_id,item.title,"1.30")

            }else{
                onmultiClick.onmultiClick(item.addon_content_id,item.title,item.price)

            }
        }
    }




    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<ProductContainItem>() {
        override fun areItemsTheSame(oldItem: ProductContainItem, newItem: ProductContainItem): Boolean {
            return oldItem.addon_content_id == newItem.addon_content_id

        }

        override fun areContentsTheSame(oldItem: ProductContainItem, newItem: ProductContainItem): Boolean {
            return oldItem == newItem
        }


    }



}