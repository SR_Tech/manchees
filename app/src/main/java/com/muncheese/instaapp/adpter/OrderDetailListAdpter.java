package com.muncheese.instaapp.adpter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.muncheese.instaapp.R;
import com.muncheese.instaapp.model.orderdetail.AddonContent;
import com.muncheese.instaapp.model.orderdetail.Result;
import com.muncheese.instaapp.model.response.IngredientResponse;
import com.muncheese.instaapp.model.response.OrderDetailPageResponse;
import com.muncheese.instaapp.util.Comman;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class OrderDetailListAdpter extends RecyclerView.Adapter<OrderDetailListAdpter.AllIndiaTestHolder> {

    Context context;
    List<Result> orderDetailListResponses;


    public OrderDetailListAdpter(List<Result> orderDetailListResponses, Context context) {
        this.orderDetailListResponses = orderDetailListResponses;
        this.context = context;
    }


    public void updateAdapter(List<Result> orderDetailListResponses, Context context) {
        this.orderDetailListResponses = orderDetailListResponses;
        this.context = context;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllIndiaTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_item_order_detail, parent, false);
        AllIndiaTestHolder holder = new AllIndiaTestHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull AllIndiaTestHolder holder, int position) {
        if (orderDetailListResponses.get(position).getProduct().getProduct_name() != null)
            holder.txtname.setText(orderDetailListResponses.get(position).getProduct().getProduct_name().toString());
        if (orderDetailListResponses.get(position).getQuantity() != 0)
            holder.txtqty.setText("Qty : "+orderDetailListResponses.get(position).getQuantity()+"");
        if (orderDetailListResponses.get(position).getProduct_price() != null)
            holder.txtttotal.setText(Comman.currencyType +orderDetailListResponses.get(position).getProduct_price().toString());
//        if (orderDetailListResponses.get(position).getProductUrl() != null)
//            Picasso.get().load(orderDetailListResponses.get(position).getProductUrl()).into(holder.img);

        if (orderDetailListResponses.get(position).getSize()!= null)
            holder.product_size.setText("Size:  "+orderDetailListResponses.get(position).getSize().getSize());

        String strIngredient = "";
        List<AddonContent> ingredientarray = orderDetailListResponses.get(position).getAddon_content();
//        Log.d("TAG", "onBindViewHolder: " + orderDetailListResponses.get(position).g().size());

        for (int i = 0; i < ingredientarray.size(); i++) {
            strIngredient = strIngredient + (i + 1) + "." + ingredientarray.get(i).getAddon_content().getParent_addon().getAddon_title() + " :" + ingredientarray.get(i).getAddon_content().getTitle()+
                    "    Price :"+ Comman.currencyType + ingredientarray.get(i).getAddon_content().getPrice()+" \n" ;
        }
        if (ingredientarray.size() > 0) {
            holder.linearLayout.setVisibility(View.VISIBLE);
        } else {
            holder.linearLayout.setVisibility(View.GONE);
        }


        holder.txt_detail_see_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.txt_detail_see_more.getText().toString().equals("See More")) {
                    holder.txt_ingredient.setMaxLines(Integer.MAX_VALUE);//your TextView
                    holder.txt_detail_see_more.setText("Show Less");
                } else {
                    holder.txt_ingredient.setMaxLines(3);//your TextView
                    holder.txt_detail_see_more.setText("See More");
                }
            }
        });
        holder.txt_ingredient.setText(strIngredient);

    }

    public void clearProductLiist() {
        orderDetailListResponses = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return orderDetailListResponses.size();
    }

    public class AllIndiaTestHolder extends RecyclerView.ViewHolder {

        TextView txtname;
        TextView txtqty,product_size;
        TextView txtttotal;
        ImageView img;

        LinearLayout linearLayout;
        TextView txt_ingredient;
        TextView txt_detail_see_more;

        public AllIndiaTestHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.product_detailimage);
            txtname = itemView.findViewById(R.id.product_name);
            txtqty = itemView.findViewById(R.id.product_qty);
            txtttotal = itemView.findViewById(R.id.product_total);
            product_size = itemView.findViewById(R.id.product_size);

            linearLayout = itemView.findViewById(R.id.ll_ingredient);
            txt_ingredient = itemView.findViewById(R.id.txt_ingredient);
            txt_detail_see_more = itemView.findViewById(R.id.txt_detail_see_more);
        }
    }

}
