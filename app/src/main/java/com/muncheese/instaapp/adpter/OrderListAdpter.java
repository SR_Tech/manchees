package com.muncheese.instaapp.adpter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.muncheese.instaapp.Dashbord;
import com.muncheese.instaapp.R;
import com.muncheese.instaapp.fragment.OrderDetailScreen;
import com.muncheese.instaapp.model.request.OrderListRequest;
import com.muncheese.instaapp.util.Comman;

import java.util.ArrayList;


public class OrderListAdpter extends RecyclerView.Adapter<OrderListAdpter.AllIndiaTestHolder> {

    Context context;

    ArrayList<OrderListRequest> orderListResponses;


    public OrderListAdpter(ArrayList<OrderListRequest> orderListResponses, Context context) {
        this.orderListResponses = orderListResponses;
        this.context = context;
    }


    public void updateAdapter(ArrayList<OrderListRequest> orderListResponses, Context context) {
        this.orderListResponses = orderListResponses;
        this.context = context;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllIndiaTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_item_order_list, parent, false);
        AllIndiaTestHolder holder = new AllIndiaTestHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull AllIndiaTestHolder holder, @SuppressLint("RecyclerView") int position) {
        if (orderListResponses.get(position).getOrder().getOrderId() != null)
            holder.ordno.setText(orderListResponses.get(position).getOrder().getOrderId().toString());
        if (orderListResponses.get(position).getOrder().getCreatedAt() != null)
            holder.oderdate.setText(Comman.ConvertIntoDateFormat("yyyy-MM-dd", "dd MMMM yyyy", orderListResponses.get(position).getCreatedAt().toString()));
        if (orderListResponses.get(position).getOrder().getTotal() != null)
            holder.txttotal.setText(Comman.currencyType+orderListResponses.get(position).getOrder().getTotal());

        if (orderListResponses.get(position).getPaymentMethod() != null)
            holder.txtstatus.setText(orderListResponses.get(position).getPaymentMethod());
        if (orderListResponses.get(position).getStatus() != null) {
            if (orderListResponses.get(position).getStatus().equals("READY_TO_FULFILL")) {
                holder.txtconfirm.setText("Ready to Fulfil");
            } else if (orderListResponses.get(position).getStatus().equals("CONFIRMED")) {
                holder.txtconfirm.setText("Confirmed");
            } else if (orderListResponses.get(position).getStatus().equals("SUCCESS")) {
                holder.txtconfirm.setText("Success");
            } else if (orderListResponses.get(position).getStatus().equals("FAIL")) {
                holder.txtconfirm.setText("Failed");
            } else if (orderListResponses.get(position).getStatus().equals("READY_TO_PICK")) {
                holder.txtconfirm.setText("Ready for Pickup");
            } else if (orderListResponses.get(position).getStatus().equals("PICKED")) {
                holder.txtconfirm.setText("Picked");
            } else if (orderListResponses.get(position).getStatus().equals("COMPLETE")) {
                holder.txtconfirm.setText("Completed");
            } else if (orderListResponses.get(position).getStatus().equals("CANCELED")) {
                holder.txtconfirm.setText("Cancelled");
            } else {
                holder.txtconfirm.setText(orderListResponses.get(position).getStatus());
            }
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String orderno = orderListResponses.get(position).getOrder().getOrderId().toString();
                String orderdate = orderListResponses.get(position).getCreatedAt().toString();
                String subtotal = orderListResponses.get(position).getOrder().getSubtotal().toString();
                String tip = orderListResponses.get(position).getOrder().getTip().toString();
                String tax = orderListResponses.get(position).getOrder().getTax().toString();
                String discount = orderListResponses.get(position).getOrder().getDiscount().toString();
                String ordertotal = orderListResponses.get(position).getAmount().toString();

                Dashbord myActivity = (Dashbord) context;
                OrderDetailScreen fragment2 = new OrderDetailScreen();

                Bundle bundle = new Bundle();
                bundle.putString("order_no", orderno);

                bundle.putString("order_date", orderdate);
                bundle.putString("subtotal", subtotal);
                bundle.putString("tip", tip);
                bundle.putString("tax", tax);
                bundle.putString("discount", discount);
                bundle.putString("order_total", ordertotal);
                bundle.putString("status", "orderlist");
                fragment2.setArguments(bundle);
                FragmentManager fragmentManager = myActivity.getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.parent_foter, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();


            }
        });


    }

    public void clearProductLiist() {
        orderListResponses = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return orderListResponses.size();
    }

    public class AllIndiaTestHolder extends RecyclerView.ViewHolder {

        TextView ordno;
        TextView oderdate;
        TextView txttotal;
        TextView txtconfirm;
        TextView txtstatus;
        RelativeLayout linearLayout;


        public AllIndiaTestHolder(View itemView) {
            super(itemView);
            ordno = itemView.findViewById(R.id.txt_ord);
            oderdate = itemView.findViewById(R.id.txt_date);
            txttotal = itemView.findViewById(R.id.txt_ordtotal);
            txtconfirm = itemView.findViewById(R.id.txt_status);
            txtstatus = itemView.findViewById(R.id.txt_pytmethod);

            linearLayout = itemView.findViewById(R.id.lly_order);

        }
    }

}
