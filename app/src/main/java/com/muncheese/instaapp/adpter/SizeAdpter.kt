package com.muncheese.instaapp.adpter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.muncheese.instaapp.R
import com.muncheese.instaapp.model.fetchsize.FetchSizeItem
import com.muncheese.instaapp.model.fetchsize.Size
import com.muncheese.instaapp.util.Comman
import kotlinx.android.synthetic.main.single_item_size.view.*

class SizeAdpter(val adapterOnClick: AdapterOnClick) : ListAdapter<FetchSizeItem, SizeAdpter.BooksViewHolder>(DiffUtil()) {
    private var lastCheckedPosition = -1
    class BooksViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: FetchSizeItem) {
            val name = itemView.findViewById<TextView>(R.id.txt_id)
            name.setText(item.size.size+"("+Comman.currencyType+item.price+")")

        }


    }


    @SuppressLint("ResourceType")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksViewHolder {
        val view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.single_item_size, parent, false)
        return BooksViewHolder(view)
    }

    override fun onBindViewHolder(holder: BooksViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)

        holder.itemView.txt_id.setChecked(position == lastCheckedPosition);
        holder.itemView.txt_id.setOnClickListener(View.OnClickListener {
            adapterOnClick.onClick(item.size.category_size_id,"size",item.price)

            val copyOfLastCheckedPosition: Int = lastCheckedPosition
            lastCheckedPosition =holder.adapterPosition
            notifyItemChanged(copyOfLastCheckedPosition)
            notifyItemChanged(lastCheckedPosition)
        })
    }




    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<FetchSizeItem>() {
        override fun areItemsTheSame(oldItem: FetchSizeItem, newItem: FetchSizeItem): Boolean {
            return oldItem.size.category_size_id == newItem.size.category_size_id

        }

        override fun areContentsTheSame(oldItem: FetchSizeItem, newItem: FetchSizeItem): Boolean {
            return oldItem == newItem
        }
    }


}