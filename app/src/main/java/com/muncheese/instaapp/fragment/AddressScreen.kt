package com.muncheese.instaapp.fragment

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.model.request.AddressUpdateRequest
import com.muncheese.instaapp.model.request.AddresslistRequest
import com.muncheese.instaapp.model.request.StatelistRequest
import com.muncheese.instaapp.model.response.AddresslistResponse
import com.muncheese.instaapp.model.sendModel.AddBillingAddress
import com.muncheese.instaapp.util.*
import kotlinx.android.synthetic.main.fragment_address_screen.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class AddressScreen : Fragment() {
    var contex: Context? = null
    var isBillAddrAvail = false
    private var stateNameList: ArrayList<String>? = null
    var addressid: String = ""
    lateinit var addresslist: ArrayList<AddresslistResponse>
    var pastalcode: String = ""
    var state: String = ""
    var city: String = ""
    var xaddress: String = ""
    var country: String = "UK"
    var name: String = ""
    var houseno: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_address_screen, container, false)
        init(view)
        return view
    }

    fun init(view: View) {
        contex = context

        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "Billing Address"

        view.findViewById<View>(R.id.title_bar_left).setVisibility(View.VISIBLE)
        view.findViewById<View>(R.id.title_bar_left)
            .setOnClickListener(View.OnClickListener { v: View? -> activity?.onBackPressed() })

        getbillingaddress(view)
     //   getstatelist()
        click(view)
        view.billing_edt_city.setOnClickListener {
            stateNameList = ArrayList()
            stateNameList!!.add("Luton")
            stateNameList!!.add("Charlton")
            stateNameList!!.add("Sundon")
            stateNameList!!.add("Toddington")
            stateNameList!!.add("Barton-le-Clay")
            stateNameList!!.add("Harlington")
            stateNameList!!.add("Streatley")
            stateNameList!!.add("Houghton Regis")
            setstateDialog("billing", view)

        }
    }



    //state dialog
    private fun setstateDialog(type: String, view: View) {

        // setup the alert builder
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Select City")
        // add a list
        builder.setItems(Comman.GetStringArray(stateNameList),
            DialogInterface.OnClickListener { dialog, position ->
                view.billing_edt_city.setText(stateNameList?.get(position))
                dialog.dismiss()
            })

        // create and show the alert dialog
        val dialog = builder.create()
        dialog.show()
    }


    fun truefase(view: View, check: String) {

        if (check.equals("true")) {
            view.billing_edt_name.setEnabled(true);
            view.billing_edt_address_line.setEnabled(true);
            view.billing_edt_city.setEnabled(true);
            view.billing_edt_zip_code.setEnabled(true);
            view.billing_edt_state.setEnabled(true);
            view.billing_edt_country.setEnabled(true);
            view.billing_edt_house_no.setEnabled(true);
        } else {
            view.billing_edt_name.setEnabled(false);
            view.billing_edt_address_line.setEnabled(false);
            view.billing_edt_city.setEnabled(false);
            view.billing_edt_zip_code.setEnabled(false);
            view.billing_edt_state.setEnabled(false);
            view.billing_edt_country.setEnabled(false);
            view.billing_edt_house_no.setEnabled(false);
        }

    }

    fun validation(view: View) {
        if (view.billing_edt_name.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.name)
            )
        } else if (view.billing_edt_address_line.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.address)
            )

        } else if (view.billing_edt_city.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.city)
            )

        } else if (view.billing_edt_zip_code.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.zip_postal)
            )

        }
        else if (view.billing_edt_state.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.state)
            )

        }
        else if (view.billing_edt_country.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.country)
            )

        } else {

            if (addresslist.size > 0) {
                updatebillingaddress(view)

            } else {
                addbillingaddress(view)
            }
        }

    }

    fun click(view: View) {
        view.checkbox_current_address.setOnClickListener {
            if (view.checkbox_current_address.isChecked) {
                truefase(view, "false")
                isBillAddrAvail = true

            } else {
                isBillAddrAvail = false;
                truefase(view, "true")
            }

        }


        view.btn_next.setOnClickListener {
            if (isBillAddrAvail) {
                val myActivity = context as Dashbord
                val fragment2 = PaymentScreen()
                // String backStateName = fragment2.getClass().getName();
                val bundle = Bundle()
                bundle.putString("cuntry", country)
                bundle.putString("city", city)
                bundle.putString("state", state)
                bundle.putString("zip", pastalcode)
                bundle.putString("address", xaddress)
                bundle.putString("house_no", houseno)
                bundle.putString("name", name)
                fragment2.arguments = bundle

                val fragmentManager = myActivity.supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.parent_foter, fragment2)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            } else {
                validation(view)
            }


        }

    }

    fun getbillingaddress(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<AddresslistRequest> = RetrofitClient.getInstance().getapi()
            .getbillingaddress(
                "Token " + Preferences.getToken(),
                Preferences.getUserId()
            )

        call.enqueue(object : Callback<AddresslistRequest> {
            override fun onResponse(
                call: Call<AddresslistRequest>,
                response: Response<AddresslistRequest>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    addresslist = ArrayList(Arrays.asList(*response.body()!!.results))
                    if (addresslist.size > 0) {
                        view.ll_current_address.visibility = View.VISIBLE
                        view.txt_note.visibility = View.VISIBLE
                        view.checkbox_current_address.isChecked = true
                        Log.d("TAG", "onResponse: " + addresslist.get(0).name)
                        addressid= addresslist.get(0).id.toString()
                        state = addresslist.get(0).state
                        pastalcode = addresslist.get(0).zip
                        city = addresslist.get(0).city
                        xaddress = addresslist.get(0).address
                        name = addresslist.get(0).name
                        houseno = addresslist.get(0).houseNumber

//                        + datas.get(0).state + ", "
                        view.edt_current_address.setText(
                            addresslist.get(0).name + ",\n" + addresslist.get(0).houseNumber + ",\n" +xaddress+
                                    ",\n" +addresslist.get(0).city +", "+ addresslist.get(0).country + ",\n" + addresslist.get(
                                0
                            ).zip
                        )
                        isBillAddrAvail = true;
                        truefase(view, "false")

                    } else {
                        truefase(view, "true")
                    }


                }  else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<AddresslistRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    fun getstatelist() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")


        stateNameList = ArrayList()
        val call: Call<StatelistRequest> = RetrofitClient.getInstance().getapi()
            .getstatelist("Token " + Preferences.getToken())

        call.enqueue(object : Callback<StatelistRequest> {
            override fun onResponse(
                call: Call<StatelistRequest>,
                response: Response<StatelistRequest>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var datas = ArrayList(Arrays.asList(*response.body()!!.results))
                    if (datas.size > 0) {

                        for (i in 0 until datas.size) {
                            stateNameList!!.add(datas.get(i).state)
                        }
                    } else {

                    }

                }  else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<StatelistRequest>, t: Throwable) {myDialog.dismiss()}
        })

    }

    fun addbillingaddress(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<AddressUpdateRequest> = RetrofitClient.getInstance().getapi()
            .addaddress(
                "Token " + Preferences.getToken(),
                AddBillingAddress(
                    "",
                    country,
                    view.billing_edt_house_no.text.toString(),
                    view.billing_edt_city.text.toString(),
                    view.billing_edt_zip_code.text.toString(),
                    view.billing_edt_address_line.text.toString(),
                    Preferences.getUserId(),
                    view.billing_edt_state.text.toString(),
                    1,
                    view.billing_edt_name.text.toString()
                )
            )

        call.enqueue(object : Callback<AddressUpdateRequest> {
            override fun onResponse(
                call: Call<AddressUpdateRequest>,
                response: Response<AddressUpdateRequest>
            ) {

                if (response.code() == 201) {
                    myDialog.dismiss()
                    val myActivity = context as Dashbord
                    val fragment2 = PaymentScreen()
                    val bundle = Bundle()
                    bundle.putString("cuntry", country)
                    bundle.putString("city", view.billing_edt_city.getText().toString())
                    bundle.putString("state",view.billing_edt_state.text.toString())
                    bundle.putString("zip", view.billing_edt_zip_code.getText().toString())
                    bundle.putString("address", view.billing_edt_address_line.getText().toString())
                    fragment2.arguments = bundle
                    val fragmentManager = myActivity.supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.parent_foter, fragment2)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }  else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<AddressUpdateRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    fun updatebillingaddress(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")


        val call: Call<AddressUpdateRequest> = RetrofitClient.getInstance().getapi()
            .updateaddress(
                "Token " + Preferences.getToken(), addressid,
                AddBillingAddress(
                    "",
                    country,
                    view.billing_edt_house_no.text.toString(),
                    view.billing_edt_city.text.toString(),
                    view.billing_edt_zip_code.text.toString(),
                    view.billing_edt_address_line.text.toString(),
                    Preferences.getUserId(),
                    view.billing_edt_state.text.toString(),
                    1,
                    view.billing_edt_name.text.toString()
                )
            )

        call.enqueue(object : Callback<AddressUpdateRequest> {
            override fun onResponse(
                call: Call<AddressUpdateRequest>,
                response: Response<AddressUpdateRequest>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    val myActivity = context as Dashbord
                    val fragment2 = PaymentScreen()
                    val bundle = Bundle()
                    bundle.putString("cuntry", "US")
                    bundle.putString("city", view.billing_edt_city.getText().toString())
                    bundle.putString("state",view.billing_edt_state.text.toString())
                    bundle.putString("zip", view.billing_edt_zip_code.getText().toString())
                    bundle.putString("house_no", view.billing_edt_house_no.getText().toString())
                    bundle.putString("address", view.billing_edt_address_line.getText().toString())
                    fragment2.arguments = bundle
                    val fragmentManager = myActivity.supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.parent_foter, fragment2)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()


                }  else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<AddressUpdateRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }
}