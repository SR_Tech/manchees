package com.muncheese.instaapp;

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.muncheese.instaapp.adpter.CartAdpter
import com.muncheese.instaapp.fragment.HomeScreen
import com.muncheese.instaapp.fragment.ShippingAdressScreen
import com.muncheese.instaapp.model.cartlist.CartListResponse
import com.muncheese.instaapp.model.cartlist.Result
import com.muncheese.instaapp.model.request.CartListRequest
import com.muncheese.instaapp.model.request.HourRequest
import com.muncheese.instaapp.util.*
import kotlinx.android.synthetic.main.fragment_cart_screen.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class CartScreen : Fragment() {
    var rlc_cart: RecyclerView? = null
    lateinit var cartListResponse: ArrayList<Result>
    var cartListAdpter: CartAdpter? = null
    var contex: Context? = null
    var badge: BadgeDrawable? = null

    var dipsavail = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_cart_screen, container, false)
        init(view)
        return view
    }

    @SuppressLint("UseRequireInsteadOfGet")
    fun init(view: View) {
        contex = context
        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "Cart"

        cartListResponse = ArrayList<Result>()
        cartListAdpter = CartAdpter(cartListResponse, context)
        rlc_cart = view.findViewById(R.id.rlc_cart_list)
        rlc_cart!!.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rlc_cart!!.adapter = cartListAdpter

        view.lly_cart.setOnClickListener {

//            Log.d("TAG", "init: " + price)
            if (badge?.number == 0) {
                Comman.getToast(context, resources.getString(R.string.no_items_to_place_order))
            } else {

                    if (dipsavail) {
                        Log.d("TAG", "init: " + "xyz1")

                        var price = java.lang.Double.valueOf(
                            view.txt_cartyotal.text.toString().replace(Comman.currencyType, "")
                        )
                        Preferences.setcarttotal(price.toString())
                        val myActivity = context as Dashbord
                        val fragment2 = ShippingAdressScreen()
                        val fragmentManager = myActivity.supportFragmentManager
                        val fragmentTransaction = fragmentManager.beginTransaction()
                        fragmentTransaction.replace(R.id.parent_foter, fragment2)
                        fragmentTransaction.addToBackStack(null)
                        fragmentTransaction.commit()

                    } else {
                        Log.d("TAG", "init: " + "xyz")
                        dialogcreate("Don't forget to add your dips.", view)

                    }





            }


        }
        cartListAdpter!!.setAdapterInterface { product_id, postion, sequence, price, count, check, cartitemid ->
            if (check.equals("delete")) {
                dialogDeleteCartItem(product_id, sequence, view)
            } else {
                updatecartitem(product_id, sequence, price, count, cartitemid, view, postion)
            }
        }


        val bottomNavigationView: BottomNavigationView = activity!!.findViewById(R.id.navigation)
        val menuItemId = bottomNavigationView.menu.getItem(1).itemId
        badge = bottomNavigationView.getOrCreateBadge(menuItemId)

        cartlist(view)
    }

    @SuppressLint("SetTextI18n")
    private fun dialogcreate(str: String, view: View) {
        val dialog: Dialog =
            CustomDialogs.dialogdips(context, str)
        val okbtn = dialog.findViewById<Button>(R.id.btn_ok)
        val cancelbtn = dialog.findViewById<Button>(R.id.btn_1cancel)
        okbtn.text = "Ok"
        cancelbtn.text = "Not Now"
        okbtn.setOnClickListener { v1: View? ->
            if (Comman.isConnectingToInternet(context)) {
                Preferences.setdips("331")
                val myActivity = context as Dashbord
                val fragment2 = HomeScreen()
                val fragmentManager = myActivity.supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.parent_foter, fragment2)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
                dialog.dismiss()
            }
        }
        cancelbtn.setOnClickListener { v1: View? ->

            dialog.dismiss()
            var price = java.lang.Double.valueOf(
                view.txt_cartyotal.text.toString().replace(Comman.currencyType, "")
            )
            Preferences.setcarttotal(price.toString())
            val myActivity = context as Dashbord
            val fragment2 = ShippingAdressScreen()
            val fragmentManager = myActivity.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.parent_foter, fragment2)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }

    }

    fun cartlist(view: View) {
        //
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        cartListAdpter?.clearProductLiist()
        val call: Call<CartListResponse> = RetrofitClient.getInstance().getapi()
            .getcartlistx(
                "Token " + Preferences.getToken(),
                Preferences.getcartId(), getResources().getString(R.string.restaurant_id)
            )

        call.enqueue(object : Callback<CartListResponse> {
            override fun onResponse(
                call: Call<CartListResponse>,
                response: Response<CartListResponse>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    val p: CartListResponse? = response.body()
                    var datas = ArrayList(p?.results)
                    cartListResponse = datas
                    if (p != null) {
                        if (p.results.size != 0) {

                            for (i in 0 until cartListResponse.size) {
                                if (cartListResponse.get(i).product.product_id.equals(20733)) {
                                    Preferences.setpizzacolle("20733")
                                } else if (cartListResponse.get(i).product.product_id.equals(20734)) {
                                    Preferences.setpizzdeliver("20734")
                                } else if (cartListResponse.get(i).product.product_id.equals(20735)) {
                                    Preferences.setpizz2deliver("20735")

                                }

                            }


                            var count = p?.count.toString()
                            Preferences.setcartcount(count)
                            badge?.number = count.toInt()

                            view.txt_cartcount.setText("Cart Items : " + p?.count.toString())
                            view.txt_cartyotal.setText(Comman.currencyType + p?.total_cost)
//                            var datas = Arrays.asList(p.results)
                            cartListAdpter!!.updateAdapter(p.results, context)
                            cartListAdpter!!.notifyDataSetChanged()


                            for (i in 0 until cartListResponse.size) {
                                if (cartListResponse.get(i).product.category == 331) {
                                    dipsavail=true
                                    break
                                } else {
                                    dipsavail=false

                                }
                            }
                        } else {
                            badge?.number = 0
                            view.txt_cartcount.setText("Cart Items : " + "0")
                            view.txt_cartyotal.setText(Comman.currencyType + "0")
                            cartListAdpter!!.updateAdapter(p.results, context)
                        }
                    }


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<CartListResponse>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    fun dialogDeleteCartItem(productid: String, sequenceid: String, view: View) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog.setContentView(R.layout.dialog_delete)
        dialog.setCancelable(true)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        (dialog.findViewById<View>(R.id.txt) as TextView).text =
            resources.getString(R.string.dialog_delete_item_txt)
        dialog.findViewById<View>(R.id.btn_cancel).setOnClickListener { dialog.dismiss() }
        dialog.findViewById<View>(R.id.btn_ok).setOnClickListener {
            dialog.dismiss()
            if (Comman.isConnectingToInternet(context)) deletcartitem(productid, sequenceid, view)
        }
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        dialog.window!!.attributes = lp
    }

    fun deletcartitem(productid: String, sequenceid: String, view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<HourRequest> = RetrofitClient.getInstance().getapi()
            .deletecartitemx(
                "Token " + Preferences.getToken(),
                productid
            )

        call.enqueue(object : Callback<HourRequest> {
            override fun onResponse(
                call: Call<HourRequest>,
                response: Response<HourRequest>
            ) {

                if (response.code() == 204) {
                    myDialog.dismiss()
                    cartlist(view)
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<HourRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    fun updatecartitem(
        productid: String,
        sequenceid: String,
        price: Double,
        count: String,
        cartitemid: String,
        view: View,
        posttion: Int
    ) {


        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val jsonArray1 = JsonArray()


        val jsonObject = JsonObject()
        for (i in 0 until cartListResponse.get(posttion).addon_content.size) {
            jsonArray1.add(cartListResponse.get(posttion).addon_content.get(i).addon_content.addon_content_id)
        }


        jsonObject.addProperty("price", price)
        jsonObject.addProperty("extra", "")
        jsonObject.addProperty("quantity", count)
        jsonObject.addProperty("cart_id", Preferences.getcartId())
        jsonObject.addProperty("product_id", productid)

        jsonObject.addProperty("size_id", sequenceid)

        jsonObject.add("addon_content_list", jsonArray1)


        val call: Call<CartListRequest> = RetrofitClient.getInstance().getapi()
            .updatecart(
                "Token " + Preferences.getToken(),
                cartitemid,
                jsonObject
            )

        call.enqueue(object : Callback<CartListRequest> {
            override fun onResponse(
                call: Call<CartListRequest>,
                response: Response<CartListRequest>
            ) {

                if (response.code() == 201) {
                    myDialog.dismiss()
                    cartlist(view)
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<CartListRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

}