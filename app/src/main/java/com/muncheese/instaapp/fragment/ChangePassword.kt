package com.muncheese.instaapp.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.activity.LoginScreen
import com.muncheese.instaapp.model.ChangePasswordResponse
import com.muncheese.instaapp.model.sendModel.ChangePassword
import com.muncheese.instaapp.util.*
import kotlinx.android.synthetic.main.fragment_change_password.view.*
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ChangePassword : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_change_password, container, false)
        init(view)
        return view;
    }

    fun init(view: View) {

        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "Change Password"
        view.findViewById<View>(R.id.title_bar_left).setVisibility(View.VISIBLE)
        view.findViewById<View>(R.id.title_bar_left)
            .setOnClickListener(View.OnClickListener { v: View? -> activity?.onBackPressed() })

        view.btn_update_password.setOnClickListener {

            if (view.current_password.text.toString().isEmpty()) {
                Comman.getToast(
                    context,
                    resources.getString(R.string.current_password_validate)
                )
            } else if (view.new_Password.text.toString().isEmpty()) {
                Comman.getToast(
                    context,
                    resources.getString(R.string.new_password_validate)
                )
            } else if (view.new_Password.text.toString().length <= 8) {
                Comman.getToast(
                    context,
                    resources.getString(R.string.password_validate_character)
                )
            } else if (view.confirm_Password.text.toString().isEmpty()) {
                Comman.getToast(
                    context,
                    resources.getString(R.string.confrm_password_validate)
                )
            } else if (!view.new_Password.text.toString()
                    .equals(view.confirm_Password.text.toString())
            ) {
                Comman.getToast(
                    context,
                    resources.getString(R.string.password_change_validate)
                )
            } else {
                getchangepassword(view)
            }
        }

    }


    fun getchangepassword(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<ChangePasswordResponse> = RetrofitClient.getInstance().getapi()
            .changepassword(
                "Token " + Preferences.getToken(),
                ChangePassword(
                    view.current_password.text.toString(),
                    Preferences.getusername(),
                    view.new_Password.text.toString(),
                    view.confirm_Password.text.toString()
                )
            )
        call.enqueue(object : Callback<ChangePasswordResponse?> {
            override fun onResponse(
                call: Call<ChangePasswordResponse?>,
                response: Response<ChangePasswordResponse?>
            ) {
                if (response.code() == 200) {
                    myDialog.dismiss()
                    Preferences.cleare()
                    Toast.makeText(
                        context,
                        "Password changed successfully",
                        Toast.LENGTH_LONG
                    ).show()
                    val intent = Intent(context,
                        LoginScreen::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else if (response.code() == 400)  {
                    myDialog.dismiss()

                        Toast.makeText(
                            context,
                            "Old password does not match in the system",
                            Toast.LENGTH_LONG
                        ).show()

                }else{

                }
            }

            override fun onFailure(call: Call<ChangePasswordResponse?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }
}