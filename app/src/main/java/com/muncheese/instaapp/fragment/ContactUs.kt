package com.muncheese.instaapp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.muncheese.instaapp.R
import com.muncheese.instaapp.model.request.RetrarentRequest
import com.muncheese.instaapp.util.CustomDialogs
import com.muncheese.instaapp.util.DialogsUtils
import com.muncheese.instaapp.util.Preferences
import com.muncheese.instaapp.util.RetrofitClient
import kotlinx.android.synthetic.main.fragment_contact_us.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ContactUs : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_contact_us, container, false)
        init(view)
        return view
    }

    fun init(view: View) {
        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "Contact Us"
        fetchdata(view)

    }

    fun fetchdata(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        //   API.postJson(new FooRequest("kit", "kat"));
        val call: Call<RetrarentRequest> = RetrofitClient.getInstance().getapi()
            .getabout(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id)
            )
        call.enqueue(object : Callback<RetrarentRequest?> {
            override fun onResponse(
                call: Call<RetrarentRequest?>,
                response: Response<RetrarentRequest?>
            ) {
                if (response.code() == 200) {
                    myDialog.dismiss()
                    var p = response.body();


                    view.txtcall.setText(p?.phone)
                    view.txt_mail.setText(p?.email)
                    view.txt_location.setText(p?.address+
                            " "+p?.city+", "+p?.state+" "+p?.zip)
                    view.txt_workhour.setText(p?.workingHours)



                }  else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<RetrarentRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }
}