package com.muncheese.instaap.fragment

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.activity.LoginScreen
import com.muncheese.instaapp.adpter.*
import com.muncheese.instaapp.model.fetchsize.FetchSize
import com.muncheese.instaapp.model.productaddon.Addon
import com.muncheese.instaapp.model.productaddon.ProductAddon
import com.muncheese.instaapp.model.productaddon.ProductAddonItem
import com.muncheese.instaapp.model.productcontain.ProductContain
import com.muncheese.instaapp.model.productcontain.ProductContainItem
import com.muncheese.instaapp.model.request.*
import com.muncheese.instaapp.model.sendModel.CreateCart
import com.muncheese.instaapp.util.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_detail_screen.view.*
import kotlinx.android.synthetic.main.single_item_base.view.*
import kotlinx.android.synthetic.main.single_item_multiselection.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class DetailScreen : Fragment(), AdapterOnClick, AdapterOnmultiClick, AdapterholderOnClick {
    var rlc_ingre: RecyclerView? = null
    var rlc_base1: RecyclerView? = null
    var rlc_pcontain1: RecyclerView? = null
    lateinit var stateNameList: ArrayList<String>

    lateinit var productContain: ArrayList<ProductContainItem>

    lateinit var productAddon: ArrayList<ProductAddonItem>
    lateinit var lly_spin: LinearLayout

    lateinit var holder: BaseAdpter.BaseViewHolder
    var holderposition = 0

    var productid: String? = null
    var producturl: String? = null
    var productcost: String? = null
    var productname: String? = null
    var count: Int = 1
    var sizeid: String = ""
    var multiselection: HashMap<Int, Any>? = null
    var singleselection: HashMap<Int, Any>? = null

    var multitextselection: HashMap<Int, String>? = null


    lateinit var txt_subtotaldetail: TextView
    var disspinner: Spinner? = null
    var checkcount = 0
    var finalproductprice = 0.0
    var toppingproductprice = 0.0


    var catid = 0
    var sizeapi = 0
    var sizeselect = false

    var productad = 0

    var txtqtyset: TextView? = null

    var adpter = MultiselectionContainAdpter(this, "")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_detail_screen, container, false)
        init(view)
        return view
    }

    override fun onClick(item: Any, type: String, prise: String) {
        if (type.equals("size")) {
            sizeselect = true
            sizeid = item.toString()
            productaddon(item as Int)
            txt_subtotaldetail.text = Comman.currencyType + prise
            txtqtyset?.setText("1")
            count = 1
//            sizeapi=0
//            productad=0
            checkcount = 0
            finalproductprice = java.lang.Double.valueOf(prise)
            lly_spin.visibility = View.GONE
            singleselection?.clear()
            multiselection?.clear()
            productAddon.clear()
            productContain.clear()
            multitextselection?.clear()
            adpter.notifyDataSetChanged()

        } else {


//            productcontain(item as Int, type, prise)


        }
        Log.d("TAG", "init: $item")
    }

    fun init(view: View) {

        stateNameList = ArrayList()

        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "Details"
        view.findViewById<View>(R.id.title_bar_left).setVisibility(View.VISIBLE)
        view.findViewById<View>(R.id.title_bar_left)
            .setOnClickListener(View.OnClickListener { v: View? -> activity?.onBackPressed() })
        txtqtyset = view.findViewById(R.id.txt_dquntity)

        multiselection = HashMap()
        singleselection = HashMap()
        multitextselection = HashMap()

        productContain = ArrayList<ProductContainItem>()
        productAddon = ArrayList<ProductAddonItem>()
        disspinner = view.findViewById<Spinner>(R.id.disspinner)
        productid = arguments?.getString("id")
        producturl = arguments?.getString("producturl")
        productcost = arguments?.getString("prise")
        productname = arguments?.getString("productname")
        catid = arguments?.getInt("catid")!!
        view.product_namedetail.setText(productname)
        view.txt_unit_detail.setText(Comman.currencyType + productcost)
        view.txt_subtotaldetail.setText(Comman.currencyType + productcost)

        txt_subtotaldetail = view.findViewById(R.id.txt_subtotaldetail)
        lly_spin = view.findViewById(R.id.lly_spin)


        //  Picasso.get().load(producturl).into(view.img_productdetail)

        if (catid == 323) {
            Picasso.get().load(producturl).into(view.img_productdetail)

        } else if (catid == 327) {
            Picasso.get().load(producturl).into(view.img_productdetail)

        } else if (catid == 329) {
            Picasso.get().load(producturl).into(view.img_productdetail)

        } else if (catid == 330) {
            Picasso.get().load(producturl).into(view.img_productdetail)

        } else {
            view.img_productdetail.setVisibility(View.GONE)
        }



        rlc_ingre = view.findViewById(R.id.rlc_size)
        rlc_ingre!!.layoutManager =
            GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
        rlc_base1 = view.findViewById(R.id.rlc_base)
        rlc_base1!!.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        rlc_pcontain1 = view.findViewById(R.id.rlc_pcontain)
        rlc_pcontain1!!.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)


        view.btn_add.setOnClickListener {
            if (sizeapi != 0 && sizeselect == false) {
                Toast.makeText(
                    requireContext(),
                    "Please select size",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (sizeapi == 0 && productad == 0) {
                if (count > 0) {
                    count = count + 1
                    view.txt_dquntity.setText(count.toString())

                    var c: Double = productcost?.toDouble()!!

                    var b: Double = java.lang.Double.valueOf(
                        view.txt_subtotaldetail.text.toString()
                            .replace(Comman.currencyType, "")
                    )

                    var prise = c + b
                    view.txt_subtotaldetail.setText(
                        Comman.currencyType + Comman.convertToUSDFormat(
                            prise.toString()
                        )
                    )
//                Log.d("TAG", "init: " + count.toString())
                }
            } else {
                if (count > 0) {
                    count = count + 1
                    view.txt_dquntity.setText(count.toString())

                    var c: Double = productcost?.toDouble()!!

                    var b: Double = java.lang.Double.valueOf(
                        view.txt_subtotaldetail.text.toString()
                            .replace(Comman.currencyType, "")
                    )
                    if (finalproductprice == 0.0) {
                        var aa = toppingproductprice + c
                        var prise = count * aa
                        view.txt_subtotaldetail.setText(
                            Comman.currencyType + Comman.convertToUSDFormat(
                                prise.toString()
                            )
                        )
                    } else {
                        var aa = toppingproductprice + finalproductprice
                        var prise = count * aa
                        view.txt_subtotaldetail.setText(
                            Comman.currencyType + Comman.convertToUSDFormat(
                                prise.toString()
                            )
                        )
                    }

//                Log.d("TAG", "init: " + count.toString())
                }

            }

        }
        view.btn_minus.setOnClickListener {
            if (sizeapi != 0 && sizeselect == false) {
                Toast.makeText(
                    requireContext(),
                    "Please select size",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (sizeapi == 0 && productad == 0) {
                if (count != 1) {
                    count = count - 1
                    view.txt_dquntity.setText(count.toString())

                    var c: Double = productcost?.toDouble()!!

                    var b: Double = java.lang.Double.valueOf(
                        view.txt_subtotaldetail.text.toString()
                            .replace(Comman.currencyType, "")
                    )


                    var prise = b - c
                    view.txt_subtotaldetail.setText(
                        Comman.currencyType + Comman.convertToUSDFormat(
                            prise.toString()
                        )
                    )
//                Log.d("TAG", "init: " + count.toString())
                }
            } else {
                if (count != 1) {
                    count = count - 1
                    view.txt_dquntity.setText(count.toString())
                    var c: Double = productcost?.toDouble()!!

                    var b: Double = java.lang.Double.valueOf(
                        view.txt_subtotaldetail.text.toString()
                            .replace(Comman.currencyType, "")
                    )
                    if (finalproductprice == 0.0) {
                        var aa = toppingproductprice + c
                        var prise = b - aa
                        view.txt_subtotaldetail.setText(
                            Comman.currencyType + Comman.convertToUSDFormat(
                                prise.toString()
                            )
                        )
                    } else {

                        var aa = toppingproductprice + finalproductprice

                        Log.d("TAG", "init: " + aa)
                        var prise = b - aa

                        view.txt_subtotaldetail.setText(
                            Comman.currencyType + Comman.convertToUSDFormat(
                                prise.toString()
                            )
                        )
                        Log.d("TAG", "init: " + count.toString())
                    }
                }

            }
        }
        click(view)

    }


    fun click(view: View) {
        checkcart()

        view.btn_add_to_cart.setOnClickListener {
            var count = 0
            Log.d("TAG", "please seelect productaddon" + productAddon.size)
            if (sizeapi != 0 && sizeselect == false) {
                Toast.makeText(
                    requireContext(),
                    "Please select size",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (sizeapi == 0 && productad == 0) {

                if (Preferences.getUserProfile().equals("0")) {
                    dialogcreate()
                } else {
                    addTocart(view)
                }
            } else if (sizeapi != 0 && productad == 0) {

                if (Preferences.getUserProfile().equals("0")) {
                    dialogcreate()
                } else {
                    addTocart(view)
                }
            } else {
                for (i in 0 until productAddon.size) {
                    Log.d(
                        "TAG",
                        "please seelect singel" + singleselection?.get(productAddon.get(i).addon.addon_id)
                            .toString()
                    )
                    Log.d("TAG", "please seelect productaddon" + singleselection.toString())
                    if (checkcount == 0) {
                        if (Preferences.getUserProfile().equals("0")) {
                            dialogcreate()
                        } else {
                            addTocart(view)
                        }
                    } else {
                        if (singleselection!!.containsKey(productAddon.get(i).addon.addon_id as Any)) {
                            count++
                            Log.d("TAG", "please seelect " + "ok" + count)
                            Log.d("TAG", "please seelect " + "ok" + checkcount)

                            if (count == checkcount) {
                                Log.d("TAG", "please seelect " + "ok")

                                if (Preferences.getUserProfile().equals("0")) {
                                    dialogcreate()
                                } else {
                                    addTocart(view)
                                }
                            }
                        } else {
                            if (productAddon.get(i).addon.selection_method.equals("any one")) {

                                Toast.makeText(
                                    requireContext(),
                                    "Please select " + productAddon.get(i).addon.addon_title,
                                    Toast.LENGTH_SHORT
                                ).show()
                                break
                                Log.d(
                                    "TAG",
                                    "please select " + productAddon.get(i).addon.addon_title
                                )


                            }
                        }
                    }

                }

            }


        }
        getsize(view)


    }

    private fun dialogcreate() {
        val dialog: Dialog =
            CustomDialogs.dialoglogin(context, resources.getString(R.string.logincheck))
        val okbtn = dialog.findViewById<Button>(R.id.btn_ok)
        val cancelbtn = dialog.findViewById<Button>(R.id.btn_cancel)
        okbtn.text = "Ok"
        cancelbtn.text = "Cancel"
        okbtn.setOnClickListener { v1: View? ->
            if (Comman.isConnectingToInternet(context)) {
                val intent = Intent(context, LoginScreen::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)

                dialog.dismiss()
            }
        }
        cancelbtn.setOnClickListener { v1: View? -> dialog.dismiss() }
    }

    fun checkcart() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<CartCheckRequest> = RetrofitClient.getInstance().getapi()
            .getcart(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id),
                Preferences.getUserId()
            )
        call.enqueue(object : Callback<CartCheckRequest?> {
            override fun onResponse(
                call: Call<CartCheckRequest?>,
                response: Response<CartCheckRequest?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    val p: CartCheckRequest? = response.body()
                    if (p?.results?.size != 0) {


                        Preferences.setcartId(p?.results?.get(0)?.id.toString())
                    } else {
                        myDialog.dismiss()
                        createcart()
                    }

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<CartCheckRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun createcart() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<CreateCartRequest> = RetrofitClient.getInstance().getapi()
            .createcart(
                "Token " + Preferences.getToken(),
                CreateCart(
                    Preferences.getUserId(),
                    getResources().getString(R.string.restaurant_id)
                )

            )
        call.enqueue(object : Callback<CreateCartRequest?> {
            override fun onResponse(
                call: Call<CreateCartRequest?>,
                response: Response<CreateCartRequest?>
            ) {
                val p: CreateCartRequest? = response.body()
                if (response.code() == 201) {
                    myDialog.dismiss()
                    Preferences.setcartcount("0")
                    Preferences.setcartId(p?.id.toString())
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<CreateCartRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }


///////new api call//////////

    fun getsize(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<FetchSize> = RetrofitClient.getInstance().getapi()
            .fetch_size(
                "Token " + Preferences.getToken(),
                productid
            )
        call.enqueue(object : Callback<FetchSize?> {
            override fun onResponse(
                call: Call<FetchSize?>,
                response: Response<FetchSize?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var p = response.body();
                    if (p!!.size != 0) {
                        var adpter = SizeAdpter(this@DetailScreen)
                        sizeapi = p.size
                        if (p != null) {
                            adpter.submitList(p)
                        }
                        rlc_ingre!!.adapter = adpter
                    } else {
                        sizeapi = 0
                        view.txt_ingredient.visibility = View.GONE
                        productaddon()
                    }


                }


            }

            override fun onFailure(call: Call<FetchSize?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }


    fun productaddon() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<ProductAddon> = RetrofitClient.getInstance().getapi()
            .product_addonwithoutsize(
                "Token " + Preferences.getToken(),
                productid
            )
        call.enqueue(object : Callback<ProductAddon?> {
            override fun onResponse(
                call: Call<ProductAddon?>,
                response: Response<ProductAddon?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var p = response.body();
                    var adpter = BaseAdpter(this@DetailScreen)


                    if (p != null) {
                        if (p.isEmpty()) {
                            productad = 0
                        } else {
                            productad = 1
                            var stateNameList: ArrayList<Addon>? = ArrayList()

                            productAddon = ArrayList(p)




                            for (i in 0 until p!!.size) {
                                stateNameList!!.add(p.get(i).addon)

                                if (p.get(i).addon.selection_method.equals("any one")) {
                                    checkcount++

                                }
                            }
                            if (p.size == 0) {
                                checkcount = 0
                            }
                            if (p != null) {
                                adpter.submitList(stateNameList)
                            }
                            rlc_base1!!.adapter = adpter
                        }
                    }


                } else {
                    var adpter = BaseAdpter(this@DetailScreen)


                    var stateNameList: ArrayList<Addon>? = ArrayList()

                    adpter.submitList(stateNameList)

                }

            }

            override fun onFailure(call: Call<ProductAddon?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun productaddon(catsizeid: Int) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<ProductAddon> = RetrofitClient.getInstance().getapi()
            .product_addon(
                "Token " + Preferences.getToken(),
                productid, catsizeid
            )
        call.enqueue(object : Callback<ProductAddon?> {
            override fun onResponse(
                call: Call<ProductAddon?>,
                response: Response<ProductAddon?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var p = response.body();
                    var adpter = BaseAdpter(this@DetailScreen)
                    var stateNameList: ArrayList<Addon>? = ArrayList()
                    if (p != null) {
                        if (p.size==0) {
                            productad = 0

                            stateNameList?.clear()
                            adpter.notifyDataSetChanged()
                        } else {
                            productad = 1


                            productAddon = ArrayList(p)




                            for (i in 0 until p!!.size) {
                                stateNameList!!.add(p.get(i).addon)

                                if (p.get(i).addon.selection_method.equals("any one")) {
                                    checkcount++

                                }
                            }
                            if (p.size == 0) {
                                checkcount = 0
                            }
                            if (p != null) {
                                adpter.submitList(stateNameList)
                            }
                            rlc_base1!!.adapter = adpter
                        }
                    }

                } else {
                    var adpter = BaseAdpter(this@DetailScreen)


                    var stateNameList: ArrayList<Addon>? = ArrayList()

                    adpter.submitList(stateNameList)
                }

            }

            override fun onFailure(call: Call<ProductAddon?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun productcontain(containid: Int, type: String, prise: String) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<ProductContain> = RetrofitClient.getInstance().getapi()
            .product_contents(
                "Token " + Preferences.getToken(),
                containid
            )
        call.enqueue(object : Callback<ProductContain?> {
            override fun onResponse(
                call: Call<ProductContain?>,
                response: Response<ProductContain?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var p = response.body();

                    productContain = ArrayList(p)


                    adpter = MultiselectionContainAdpter(this@DetailScreen, sizeid)

                    if (p != null) {
                        if (p.isEmpty()) {

                        } else {
                            if (type.equals("any one")) {
                                rlc_pcontain1!!.visibility = View.GONE
                                stateNameList.clear()
                                lly_spin.visibility = View.VISIBLE
                                for (i in 0 until p.size) {
                                    stateNameList!!.add(productContain.get(i).title)


                                }

                                val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                                    requireContext(),
                                    android.R.layout.simple_spinner_item,
                                    stateNameList
                                )
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                disspinner!!.adapter = adapter
                                disspinner!!.setSelected(false);  // must

                                disspinner?.onItemSelectedListener = object :
                                    OnItemSelectedListener {
                                    override fun onItemSelected(
                                        parent: AdapterView<*>,
                                        view: View,
                                        position: Int,
                                        id: Long
                                    ) {
                                        //                                        var districStr = p.get(position).addon_content_id
                                        Log.d("TAG", "onItemSelected: " + containid)
                                        var item = productContain.get(position).addon_content_id
                                        //                var prise = productAddon.get(position).
                                        holder.itemView.txtbasedis.setText(stateNameList[position])
                                        if (singleselection!!.isEmpty()) {


                                            singleselection?.put(containid, containid as Any)


                                        } else if (singleselection!!.containsKey(containid as Any)) {

                                            singleselection!!.remove(containid)
                                            singleselection?.put(containid, containid as Any)

                                        } else {
                                            singleselection?.put(containid, containid as Any)


                                        }
                                        Log.d("TAG", "onmultiClick: 1" + singleselection.toString())


                                        if (multiselection!!.isEmpty()) {

                                            Log.d("TAG", "onmultiClick: 1" + item.toString())

                                            multiselection?.put(containid as Int, item)

                                            //                    var c: Double = prise.toDouble()
                                            //
                                            //                    var b: Double = java.lang.Double.valueOf(
                                            //                        txt_subtotaldetail.text.toString()
                                            //                            .replace(Comman.currencyType, "")
                                            //                    )
                                            //
                                            //                    var prise = b + c
                                            //                    txt_subtotaldetail.setText(
                                            //                        Comman.currencyType + Comman.convertToUSDFormat(
                                            //                            prise.toString()
                                            //                        )
                                            //                    )

                                        } else if (multiselection!!.containsKey(containid as Any)) {

                                            multiselection!!.remove(containid)
                                            multiselection?.put(containid as Int, item)

                                            Log.d(
                                                "TAG",
                                                "onmultiClick: 2" + multiselection.toString()
                                            )

                                        } else {
                                            multiselection?.put(containid as Int, item)

                                            //                                            var c: Double = prise.toDouble()
                                            //
                                            //                                            var b: Double = java.lang.Double.valueOf(
                                            //                                                txt_subtotaldetail.text.toString()
                                            //                                                    .replace(Comman.currencyType, "")
                                            //                                            )
                                            //
                                            //                                            var prise = b + c
                                            //                                            txt_subtotaldetail.setText(
                                            //                                                Comman.currencyType + Comman.convertToUSDFormat(
                                            //                                                    prise.toString()
                                            //                                                )
                                            //                                            )
                                            Log.d(
                                                "TAG",
                                                "onmultiClick3: " + multiselection.toString()
                                            )

                                        }
                                    }

                                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                                }
                            } else {
                                lly_spin.visibility = View.GONE
                                rlc_pcontain1!!.visibility = View.VISIBLE

                                adpter.submitList(productContain)
                                rlc_pcontain1!!.adapter = adpter
                            }


                        }

                    }

                }
            }

            override fun onFailure(call: Call<ProductContain?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }


    fun addTocart(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")


        var b: Double = java.lang.Double.valueOf(
            view.txt_subtotaldetail.text.toString()
                .replace(Comman.currencyType, "")
        )


        var qty: Double = java.lang.Double.valueOf(
            view.txt_dquntity.text.toString()
                .replace(Comman.currencyType, "")
        )
        val jsonArray1 = JsonArray()


        val jsonObject = JsonObject()
        for (i in multiselection!!) {

            Log.d(
                "TAG", "initIngredientRecycler: 2" +
                        (multiselection!!.getValue(i.key.toInt()))
            )


            jsonArray1.add(multiselection!!.get(i.key).toString())

        }



        jsonObject.addProperty("price", b)
        jsonObject.addProperty("extra", view.edt_spcl_instruction.text.toString())
        jsonObject.addProperty("quantity", qty)
        jsonObject.addProperty("cart_id", Preferences.getcartId())
        jsonObject.addProperty("product_id", productid)

        jsonObject.addProperty("size_id", sizeid)

        jsonObject.add("addon_content_list", jsonArray1)


//        jsonArray.add(main)
//        jsonArray.add(jsonObject)

        val call: Call<CartListRequest> = RetrofitClient.getInstance().getapi()
            .addtocartnew(
                "Token " + Preferences.getToken(),
                jsonObject
            )
        call.enqueue(object : Callback<CartListRequest?> {
            override fun onResponse(
                call: Call<CartListRequest?>,
                response: Response<CartListRequest?>
            ) {
                val p: CartListRequest? = response.body()
                if (response.code() == 201) {
                    myDialog.dismiss()
                    dialogDeleteCartItem()


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<CartListRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun dialogDeleteCartItem() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog.setContentView(R.layout.dialog_one_line_text)
        dialog.setCancelable(true)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        (dialog.findViewById<View>(R.id.txt_msg) as TextView).text =
            resources.getString(R.string.product_added_into_cart)
        dialog.findViewById<View>(R.id.btn_ok).setOnClickListener {
            val intent = Intent(activity, Dashbord::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            dialog.dismiss()
//            if (Comman.isConnectingToInternet(context))  deletcartitem(productid, sequenceid,view)
        }
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        dialog.window!!.attributes = lp
    }

    override fun onmultiClick(item: Any, name: String, prise: String) {
        Log.d("TAG", "onmultiClick: " + item)

        if (multiselection!!.isEmpty()) {

            Log.d("TAG", "onmultiClick: 1" + item.toString())

            multiselection?.put(item as Int, item)
            multitextselection?.put(item as Int, name)

            var c: Double = prise.toDouble()
            toppingproductprice += c
            var b: Double = java.lang.Double.valueOf(
                txt_subtotaldetail.text.toString()
                    .replace(Comman.currencyType, "")
            )

            var prise = b + (c * count)
            txt_subtotaldetail.setText(
                Comman.currencyType + Comman.convertToUSDFormat(
                    prise.toString()
                )
            )


        } else if (multiselection!!.containsKey(item)) {
            var c: Double = prise?.toDouble()
            toppingproductprice -= c
            var b: Double = java.lang.Double.valueOf(
                txt_subtotaldetail.text.toString()
                    .replace(Comman.currencyType, "")
            )

            var prise = b - (c * count)
            txt_subtotaldetail.setText(
                Comman.currencyType + Comman.convertToUSDFormat(prise.toString())
            )
            multiselection!!.remove(item)
            multitextselection!!.remove(item)


            Log.d("TAG", "onmultiClick: 2" + multiselection.toString())

        } else {
            multiselection?.put(item as Int, item)
            multitextselection?.put(item as Int, name)

            var c: Double = prise.toDouble()
            toppingproductprice += c
            var b: Double = java.lang.Double.valueOf(
                txt_subtotaldetail.text.toString()
                    .replace(Comman.currencyType, "")
            )

            var prise = b + (c * count)
            txt_subtotaldetail.setText(
                Comman.currencyType + Comman.convertToUSDFormat(
                    prise.toString()
                )
            )


            Log.d("TAG", "onmultiClick312: " + multitextselection.toString())

        }
//        for (i in  multitextselection!!) {
//
//            Log.d(
//                "TAG", "initIngredientRecycler: 2" +
//                        (multitextselection!!.get(i.key).toString())
//            )

        val builder = StringBuilder()
        for (details in multitextselection!!) {
            builder.append(details.value + " " + Comman.currencyType + prise + "\n")
        }

        holder.itemView.txtbasedis.setText(builder.toString())
//            holder.itemView.txtbasedis.setText(multitextselection!!.get(i.key).toString()+" ,")


//        }
        Log.d("TAG", "onmultiClick: " + multiselection.toString())
    }

    override fun onholdrClick(
        item: Any,
        name: String,
        prise: String,
        holder1: BaseAdpter.BaseViewHolder,
        position: Int
    ) {
        holder = holder1
        holderposition = position
        productcontain(item as Int, name, prise)
    }
}


