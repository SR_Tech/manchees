package com.muncheese.instaapp.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.muncheese.instaapp.CartScreen
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.adpter.*
import com.muncheese.instaapp.model.fetchsize.FetchSize
import com.muncheese.instaapp.model.productaddon.Addon
import com.muncheese.instaapp.model.productaddon.ProductAddon
import com.muncheese.instaapp.model.productaddon.ProductAddonItem
import com.muncheese.instaapp.model.productcontain.ProductContain
import com.muncheese.instaapp.model.productcontain.ProductContainItem
import com.muncheese.instaapp.model.request.CartListRequest
import com.muncheese.instaapp.util.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_cart_screen.view.*
import kotlinx.android.synthetic.main.fragment_detail_screen.view.*
import kotlinx.android.synthetic.main.fragment_edit.view.*
import kotlinx.android.synthetic.main.fragment_edit.view.txt_unit_detail
import kotlinx.android.synthetic.main.single_item_base.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class EditFragment : Fragment(), AdapterOnClick, AdapterOnmultiClick, AdapterholderOnClick {
    var rlc_base1: RecyclerView? = null
    var rlc_pcontain1: RecyclerView? = null
    lateinit var stateNameList: ArrayList<String>
    lateinit var sizelist: ArrayList<String>

    lateinit var productContain: ArrayList<ProductContainItem>

    lateinit var productAddon: ArrayList<ProductAddonItem>
    lateinit var lly_spin: LinearLayout

    lateinit var holder: BaseAdpter.BaseViewHolder
    var holderposition = 0
    lateinit var splintru: EditText

    var productid: String? = null
    var producturl: String? = null
    var productcost: String? = null
    var productname: String? = null
    var count: Int = 1
    var sizeid: String = ""
    var multiselection: HashMap<Int, Any>? = null
    var singleselection: HashMap<Int, Any>? = null

    var multitextselection: HashMap<Int, String>? = null


    var txt_subtotaldetail: TextView? = null
    var disspinner: Spinner? = null
    var sizespin: Spinner? = null

    var checkcount = 0
    var finalproductprice = 0.0
    var toppingproductprice = 0.0


    var catid = 0
    var sizeapi = 0
    var sizeselect = false

    var productad = 0

    var txtqtyset: TextView? = null

    var adpter = MultiselectionContainAdpter(this, "")
    var postion = 0
    var item = 0
    lateinit var cartListResponses: ArrayList<com.muncheese.instaapp.model.cartlist.Result>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view: View = inflater.inflate(R.layout.fragment_edit, container, false)

        init(view)
        return view

    }

    private fun init(view: View) {
        cartListResponses =
            arguments?.getSerializable("data") as ArrayList<com.muncheese.instaapp.model.cartlist.Result>
        Log.d("TAG", "onCreateView: " + cartListResponses.toString())
        postion = requireArguments().getInt("postion")
        Log.d("TAG", "onCreateView: " + postion)

        view.txt_unit_detail.text = Comman.currencyType + cartListResponses.get(postion).price
        view.txt_qty.text = "Quantity :" + cartListResponses.get(postion).quantity
        productid = cartListResponses.get(postion).product.product_id.toString()
        producturl = cartListResponses.get(postion).product.product_url
        catid = cartListResponses.get(postion).product.category
        view.txt_prodname.setText(cartListResponses.get(postion).product.product_name)
        if (catid == 323) {
            Picasso.get().load(producturl).into(view.img_productdetail1)

        } else if (catid == 327) {
            Picasso.get().load(producturl).into(view.img_productdetail1)

        } else if (catid == 329) {
            Picasso.get().load(producturl).into(view.img_productdetail1)

        } else if (catid == 330) {
            Picasso.get().load(producturl).into(view.img_productdetail1)

        } else {
            view.img_productdetail1.setVisibility(View.GONE)
        }


        if (cartListResponses.get(postion).size != null) {
            view.txt_size.text = cartListResponses.get(postion).size.size

        } else {
            view.lly_sizee.visibility = View.GONE
        }
        val builder = StringBuilder()

        for (i in 0 until cartListResponses.get(postion).addon_content.size) {
            builder.append(cartListResponses.get(postion).addon_content.get(i).addon_content.parent_addon.addon_title + ": ")
            builder.append(cartListResponses.get(postion).addon_content.get(i).addon_content.title + " ")
            builder.append(Comman.currencyType + cartListResponses.get(postion).addon_content.get(i).addon_content.price + "\n")

        }
        view.txt_muliti.setText(builder)

        allvalue(view)
    }

    fun allvalue(view: View) {
        txt_subtotaldetail = view.findViewById(R.id.txt_unit_detail)
        lly_spin = view.findViewById(R.id.lly_spin)
        splintru = view.findViewById(R.id.edt_spcle_instruction)


        disspinner = view.findViewById<Spinner>(R.id.disspinner)
        sizespin = view.findViewById<Spinner>(R.id.sizespinner)

        stateNameList = ArrayList()
        multiselection = HashMap()
        singleselection = HashMap()
        multitextselection = HashMap()
        sizelist = ArrayList()
        productContain = ArrayList<ProductContainItem>()
        productAddon = ArrayList<ProductAddonItem>()

        rlc_base1 = view.findViewById(R.id.rlc_base)
        rlc_base1!!.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        rlc_pcontain1 = view.findViewById(R.id.rlc_pcontain)
        rlc_pcontain1!!.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        if (view.lly_sizee.visibility == View.VISIBLE) {

            view.txt_size.setOnClickListener {
                getsize(view)
            }


        }
        click(view)
    }

    fun getsize(view1: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<FetchSize> = RetrofitClient.getInstance().getapi()
            .fetch_size(
                "Token " + Preferences.getToken(),
                cartListResponses.get(postion).product.product_id.toString()
            )
        call.enqueue(object : Callback<FetchSize?> {
            override fun onResponse(
                call: Call<FetchSize?>,
                response: Response<FetchSize?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var p = response.body();
                    if (p!!.size != 0) {
                        sizeapi = p.size


                        sizelist.clear()
                        view1.lly_sizespin.visibility = View.VISIBLE
                        for (i in 0 until p.size) {
                            sizelist!!.add(p.get(i).size.size)


                        }

                        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                            requireContext(),
                            android.R.layout.simple_spinner_item,
                            sizelist
                        )
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        sizespin!!.adapter = adapter
                        sizespin!!.setSelected(false);  // must
//                        sizespin!!.setSelection(0,false)

                        sizespin?.onItemSelectedListener = object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>,
                                view: View,
                                position: Int,
                                id: Long
                            ) {

                                checkcount = 0
                                finalproductprice = java.lang.Double.valueOf(p.get(position).price)
                                lly_spin.visibility = View.GONE
                                singleselection?.clear()
                                multiselection?.clear()
                                productAddon.clear()
                                productContain.clear()
                                multitextselection?.clear()
                                adpter.notifyDataSetChanged()
                                item = p.get(position).size.category_size_id

                                view1.txt_size.setText(sizelist[position])
                                productaddon(item)
                                sizeselect = true
                                sizeid = item.toString()

                                txt_subtotaldetail?.text =
                                    Comman.currencyType + p.get(position).price

//            sizeapi=0
//            productad=0

                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        }


                    } else {
                        sizeapi = 0
                        view1.lly_sizespin.visibility = View.GONE
                        productaddon()
                    }


                }


            }

            override fun onFailure(call: Call<FetchSize?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }


    fun productaddon() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<ProductAddon> = RetrofitClient.getInstance().getapi()
            .product_addonwithoutsize(
                "Token " + Preferences.getToken(),
                productid
            )
        call.enqueue(object : Callback<ProductAddon?> {
            override fun onResponse(
                call: Call<ProductAddon?>,
                response: Response<ProductAddon?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var p = response.body();
                    var adpter = BaseAdpter(this@EditFragment)


                    if (p != null) {
                        if (p.isEmpty()) {
                            productad = 0
                        } else {
                            productad = 1
                            var stateNameList: ArrayList<Addon>? = ArrayList()

                            productAddon = ArrayList(p)




                            for (i in 0 until p!!.size) {
                                stateNameList!!.add(p.get(i).addon)

                                if (p.get(i).addon.selection_method.equals("any one")) {
                                    checkcount++

                                }
                            }
                            if (p.size == 0) {
                                checkcount = 0
                            }
                            if (p != null) {
                                adpter.submitList(stateNameList)
                            }
                            rlc_base1!!.adapter = adpter
                        }
                    }


                } else {
                    var adpter = BaseAdpter(this@EditFragment)


                    var stateNameList: ArrayList<Addon>? = ArrayList()

                    adpter.submitList(stateNameList)

                }

            }

            override fun onFailure(call: Call<ProductAddon?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun productaddon(catsizeid: Int) {
        productAddon.clear()
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<ProductAddon> = RetrofitClient.getInstance().getapi()
            .product_addon(
                "Token " + Preferences.getToken(),
                productid, catsizeid
            )
        call.enqueue(object : Callback<ProductAddon?> {
            override fun onResponse(
                call: Call<ProductAddon?>,
                response: Response<ProductAddon?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var p = response.body();
                    var adpter = BaseAdpter(this@EditFragment)

                    if (p != null) {
                        if (p.isEmpty()) {
                            productad = 0
                        } else {
                            productad = 1
                            var stateNameList: ArrayList<Addon>? = ArrayList()

                            productAddon = ArrayList(p)




                            for (i in 0 until p!!.size) {
                                stateNameList!!.add(p.get(i).addon)

                                if (p.get(i).addon.selection_method.equals("any one")) {
                                    checkcount++

                                }
                            }
                            if (p.size == 0) {
                                checkcount = 0
                            }
                            if (p != null) {
                                adpter.submitList(stateNameList)
                            }
                            rlc_base1!!.adapter = adpter
                            adpter.notifyDataSetChanged()
                        }
                    }

                } else {
                    var adpter = BaseAdpter(this@EditFragment)


                    var stateNameList: ArrayList<Addon>? = ArrayList()

                    adpter.submitList(stateNameList)
                }

            }

            override fun onFailure(call: Call<ProductAddon?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun productcontain(containid: Int, type: String, prise: String) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<ProductContain> = RetrofitClient.getInstance().getapi()
            .product_contents(
                "Token " + Preferences.getToken(),
                containid
            )
        call.enqueue(object : Callback<ProductContain?> {
            override fun onResponse(
                call: Call<ProductContain?>,
                response: Response<ProductContain?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var p = response.body();

                    productContain = ArrayList(p)


                    adpter = MultiselectionContainAdpter(this@EditFragment, sizeid)

                    if (p != null) {
                        if (p.isEmpty()) {

                        } else {
                            if (type.equals("any one")) {
                                rlc_pcontain1!!.visibility = View.GONE
                                stateNameList.clear()
                                lly_spin.visibility = View.VISIBLE
                                for (i in 0 until p.size) {
                                    stateNameList!!.add(productContain.get(i).title)


                                }

                                val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                                    requireContext(),
                                    android.R.layout.simple_spinner_item,
                                    stateNameList
                                )
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                disspinner!!.adapter = adapter
                                disspinner!!.setSelected(false);  // must

                                disspinner?.onItemSelectedListener = object :
                                    AdapterView.OnItemSelectedListener {
                                    override fun onItemSelected(
                                        parent: AdapterView<*>,
                                        view: View,
                                        position: Int,
                                        id: Long
                                    ) {
                                        //                                        var districStr = p.get(position).addon_content_id
                                        Log.d("TAG", "onItemSelected: " + containid)
                                        var item = productContain.get(position).addon_content_id
                                        //                var prise = productAddon.get(position).
                                        holder.itemView.txtbasedis.setText(stateNameList[position])
                                        if (singleselection!!.isEmpty()) {


                                            singleselection?.put(containid, containid as Any)


                                        } else if (singleselection!!.containsKey(containid as Any)) {

                                            singleselection!!.remove(containid)
                                            singleselection?.put(containid, containid as Any)

                                        } else {
                                            singleselection?.put(containid, containid as Any)


                                        }
                                        Log.d("TAG", "onmultiClick: 1" + singleselection.toString())


                                        if (multiselection!!.isEmpty()) {

                                            Log.d("TAG", "onmultiClick: 1" + item.toString())

                                            multiselection?.put(containid as Int, item)

                                            //                    var c: Double = prise.toDouble()
                                            //
                                            //                    var b: Double = java.lang.Double.valueOf(
                                            //                        txt_subtotaldetail.text.toString()
                                            //                            .replace(Comman.currencyType, "")
                                            //                    )
                                            //
                                            //                    var prise = b + c
                                            //                    txt_subtotaldetail.setText(
                                            //                        Comman.currencyType + Comman.convertToUSDFormat(
                                            //                            prise.toString()
                                            //                        )
                                            //                    )

                                        } else if (multiselection!!.containsKey(containid as Any)) {

                                            multiselection!!.remove(containid)
                                            multiselection?.put(containid as Int, item)

                                            Log.d(
                                                "TAG",
                                                "onmultiClick: 2" + multiselection.toString()
                                            )

                                        } else {
                                            multiselection?.put(containid as Int, item)

                                            //                                            var c: Double = prise.toDouble()
                                            //
                                            //                                            var b: Double = java.lang.Double.valueOf(
                                            //                                                txt_subtotaldetail.text.toString()
                                            //                                                    .replace(Comman.currencyType, "")
                                            //                                            )
                                            //
                                            //                                            var prise = b + c
                                            //                                            txt_subtotaldetail.setText(
                                            //                                                Comman.currencyType + Comman.convertToUSDFormat(
                                            //                                                    prise.toString()
                                            //                                                )
                                            //                                            )
                                            Log.d(
                                                "TAG",
                                                "onmultiClick3: " + multiselection.toString()
                                            )

                                        }
                                    }

                                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                                }
                            } else {
                                lly_spin.visibility = View.GONE
                                rlc_pcontain1!!.visibility = View.VISIBLE

                                adpter.submitList(productContain)
                                rlc_pcontain1!!.adapter = adpter
                            }


                        }

                    }

                }
            }

            override fun onFailure(call: Call<ProductContain?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    override fun onClick(item: Any, name: String, prise: String) {

    }

    fun click(view: View) {
        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "Edit Cart"
        view.findViewById<View>(R.id.title_bar_left).setVisibility(View.VISIBLE)
        view.findViewById<View>(R.id.title_bar_left)
            .setOnClickListener(View.OnClickListener { v: View? -> activity?.onBackPressed() })

        view.btn_update.setOnClickListener {
            var count = 0
            Log.d("TAG", "please seelect productaddon" + productAddon.size)
            if (sizeapi != 0 && sizeselect == false) {
                Toast.makeText(
                    requireContext(),
                    "Please select size",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (sizeapi == 0 && productad == 0) {

                if (Preferences.getUserProfile().equals("0")) {

                } else {
                    var price = java.lang.Double.valueOf(
                        view.txt_unit_detail.text.toString()
                            .replace(Comman.currencyType, "")
                    )
                    var sizeid = ""
                    if (cartListResponses.get(postion).size != null) {
                        if (item.equals(0)) {
                            sizeid = cartListResponses.get(postion).size.category_size_id.toString()

                        } else {
                            sizeid = item.toString()
                        }
                    }
                    updatecartitem(
                        productid.toString(),
                        sizeid,
                        price,
                        cartListResponses.get(postion).quantity.toString(),
                        cartListResponses.get(postion).cart_item_id.toString(),

                        postion
                    )
                }
            } else if (sizeapi != 0 && productad == 0) {

                if (Preferences.getUserProfile().equals("0")) {

                } else {
                    var price = java.lang.Double.valueOf(
                        view.txt_unit_detail.text.toString()
                            .replace(Comman.currencyType, "")
                    )
                    var sizeid = ""
                    if (cartListResponses.get(postion).size != null) {
                        if (item.equals(0)) {
                            sizeid = cartListResponses.get(postion).size.category_size_id.toString()

                        } else {
                            sizeid = item.toString()
                        }
                    }
                    updatecartitem(
                        productid.toString(),
                        sizeid,
                        price,
                        cartListResponses.get(postion).quantity.toString(),
                        cartListResponses.get(postion).cart_item_id.toString(),

                        postion
                    )
                }
            } else {
                for (i in 0 until productAddon.size) {
                    Log.d(
                        "TAG",
                        "please seelect singel" + singleselection?.get(productAddon.get(i).addon.addon_id)
                            .toString()
                    )
                    Log.d("TAG", "please seelect productaddon" + singleselection.toString())
                    if (checkcount == 0) {
                        if (Preferences.getUserProfile().equals("0")) {

                        } else {
                            var price = java.lang.Double.valueOf(
                                view.txt_unit_detail.text.toString()
                                    .replace(Comman.currencyType, "")
                            )
                            var sizeid = ""
                            if (cartListResponses.get(postion).size != null) {
                                if (item.equals(0)) {
                                    sizeid = cartListResponses.get(postion).size.category_size_id.toString()

                                } else {
                                    sizeid = item.toString()
                                }
                            }
                            updatecartitem(
                                productid.toString(),
                                sizeid,
                                price,
                                cartListResponses.get(postion).quantity.toString(),
                                cartListResponses.get(postion).cart_item_id.toString(),

                                postion
                            )
                        }
                    } else {
                        if (singleselection!!.containsKey(productAddon.get(i).addon.addon_id as Any)) {
                            count++
                            Log.d("TAG", "please seelect " + "ok" + count)
                            Log.d("TAG", "please seelect " + "ok" + checkcount)

                            if (count == checkcount) {
                                Log.d("TAG", "please seelect " + "ok")

                                if (Preferences.getUserProfile().equals("0")) {

                                } else {
                                    var price = java.lang.Double.valueOf(
                                        view.txt_unit_detail.text.toString()
                                            .replace(Comman.currencyType, "")
                                    )
                                    var sizeid = ""
                                    if (cartListResponses.get(postion).size != null) {
                                        if (item.equals(0)) {
                                            sizeid = cartListResponses.get(postion).size.category_size_id.toString()

                                        } else {
                                            sizeid = item.toString()
                                        }
                                    }
                                    updatecartitem(
                                        productid.toString(),
                                        sizeid,
                                        price,
                                        cartListResponses.get(postion).quantity.toString(),
                                        cartListResponses.get(postion).cart_item_id.toString(),

                                        postion
                                    )

                                }
                            }
                        } else {
                            if (productAddon.get(i).addon.selection_method.equals("any one")) {

                                Toast.makeText(
                                    requireContext(),
                                    "Please select " + productAddon.get(i).addon.addon_title,
                                    Toast.LENGTH_SHORT
                                ).show()
                                break
                                Log.d(
                                    "TAG",
                                    "please select " + productAddon.get(i).addon.addon_title
                                )


                            }
                        }
                    }

                }

            }


        }


    }


    override fun onmultiClick(item: Any, name: String, prise: String) {
        Log.d("TAG", "onmultiClick: " + item)

        if (multiselection!!.isEmpty()) {

            Log.d("TAG", "onmultiClick: 1" + item.toString())

            multiselection?.put(item as Int, item)
            multitextselection?.put(item as Int, name)

            var c: Double = prise.toDouble()
            toppingproductprice += c
            var b: Double = java.lang.Double.valueOf(
                txt_subtotaldetail?.text.toString()
                    .replace(Comman.currencyType, "")
            )

            var prise = b + (c * count)
            txt_subtotaldetail?.setText(
                Comman.currencyType + Comman.convertToUSDFormat(
                    prise.toString()
                )
            )


        } else if (multiselection!!.containsKey(item)) {
            var c: Double = prise?.toDouble()
            toppingproductprice -= c
            var b: Double = java.lang.Double.valueOf(
                txt_subtotaldetail?.text.toString()
                    .replace(Comman.currencyType, "")
            )

            var prise = b - (c * count)
            txt_subtotaldetail?.setText(
                Comman.currencyType + Comman.convertToUSDFormat(prise.toString())
            )
            multiselection!!.remove(item)
            multitextselection!!.remove(item)


            Log.d("TAG", "onmultiClick: 2" + multiselection.toString())

        } else {
            multiselection?.put(item as Int, item)
            multitextselection?.put(item as Int, name)

            var c: Double = prise.toDouble()
            toppingproductprice += c
            var b: Double = java.lang.Double.valueOf(
                txt_subtotaldetail?.text.toString()
                    .replace(Comman.currencyType, "")
            )

            var prise = b + (c * count)
            txt_subtotaldetail?.setText(
                Comman.currencyType + Comman.convertToUSDFormat(
                    prise.toString()
                )
            )


            Log.d("TAG", "onmultiClick312: " + multitextselection.toString())

        }
//        for (i in  multitextselection!!) {
//
//            Log.d(
//                "TAG", "initIngredientRecycler: 2" +
//                        (multitextselection!!.get(i.key).toString())
//            )

        val builder = StringBuilder()
        for (details in multitextselection!!) {
            builder.append(details.value + " " + Comman.currencyType + prise + "\n")
        }

        holder.itemView.txtbasedis.setText(builder.toString())
//            holder.itemView.txtbasedis.setText(multitextselection!!.get(i.key).toString()+" ,")


//        }
        Log.d("TAG", "onmultiClick: " + multiselection.toString())
    }

    override fun onholdrClick(
        item: Any,
        name: String,
        prise: String,
        holder1: BaseAdpter.BaseViewHolder,
        position: Int
    ) {
        holder = holder1
        holderposition = position
        productcontain(item as Int, name, prise)
    }


    fun updatecartitem(
        productid: String,
        sequenceid: String,
        price: Double,
        count: String,
        cartitemid: String,
        posttion: Int
    ) {


        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val jsonArray1 = JsonArray()


        val jsonObject = JsonObject()
//        for (i in 0 until cartListResponses.get(posttion).addon_content.size) {
//            jsonArray1.add(cartListResponses.get(posttion).addon_content.get(i).addon_content.addon_content_id)
//        }
        for (i in multiselection!!) {

            Log.d(
                "TAG", "initIngredientRecycler: 2" +
                        (multiselection!!.getValue(i.key.toInt()))
            )


            jsonArray1.add(multiselection!!.get(i.key).toString())

        }

        jsonObject.addProperty("price", price)
        jsonObject.addProperty("extra", splintru.text.toString())
        jsonObject.addProperty("quantity", count)
        jsonObject.addProperty("cart_id", Preferences.getcartId())
        jsonObject.addProperty("product_id", productid)

        jsonObject.addProperty("size_id", sequenceid)

        jsonObject.add("addon_content_list", jsonArray1)


        val call: Call<CartListRequest> = RetrofitClient.getInstance().getapi()
            .updatecart(
                "Token " + Preferences.getToken(),
                cartitemid,
                jsonObject
            )

        call.enqueue(object : Callback<CartListRequest> {
            override fun onResponse(
                call: Call<CartListRequest>,
                response: Response<CartListRequest>
            ) {

                if (response.code() == 201) {
                    myDialog.dismiss()
                    val myActivity = context as Dashbord
                    val fragment2 = CartScreen()
                    val fragmentManager = myActivity.supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.parent_foter, fragment2)
//                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
//                    cartlist(view)
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<CartListRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }
}