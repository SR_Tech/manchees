package com.muncheese.instaapp.fragment

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.muncheese.instaapp.R
import com.muncheese.instaapp.adpter.CatogoryAdpter
import com.muncheese.instaapp.adpter.MenuAdpter
import com.muncheese.instaapp.adpter.SearchAdpter
import com.muncheese.instaapp.model.MainLoginResponse
import com.muncheese.instaapp.model.cartlist.CartListResponse
import com.muncheese.instaapp.model.postcode.Postcoderesponse
import com.muncheese.instaapp.model.productserch.ProductSearch
import com.muncheese.instaapp.model.productserch.ProductSerchResponse
import com.muncheese.instaapp.model.request.*
import com.muncheese.instaapp.model.response.MenuResponse
import com.muncheese.instaapp.model.sendModel.MainLogin
import com.muncheese.instaapp.util.*
import kotlinx.android.synthetic.main.fragment_home_screen.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class HomeScreen : Fragment() {
    var rlc_cate: RecyclerView? = null
    var rlc_menu: RecyclerView? = null
    var menuResponses: ArrayList<MenuResponse>? = null
    var categoryResponses: ArrayList<CategoryListRequest>? = null
    var menuAdpter: MenuAdpter? = null
    var serchadpter: SearchAdpter?=null
    var catogoryAdpter: CatogoryAdpter? = null
    var contex: Context? = null
    var productSerchResponse: ArrayList<ProductSearch>? = null
    var itemCount = 1
    private var loading = true
    private val visibleThreshold = 3
    var categoryid: String = ""
    var catename: String = ""
    var adp: String = ""
    var mLayoutManager: LinearLayoutManager? = null
    var badge: BadgeDrawable? = null
    private var previousTotalItemCount = 0
    private val startingPageIndex = 1
    var txtpasscode: TextView? = null

    var count: Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_home_screen, container, false)
        init(view)
        return view
    }

    fun init(view: View) {

        contex = context


        categoryResponses = ArrayList<CategoryListRequest>()
        catogoryAdpter = CatogoryAdpter(categoryResponses, context, "")

        menuResponses = ArrayList<MenuResponse>()
        productSerchResponse= ArrayList<ProductSearch>()
        menuAdpter = MenuAdpter(menuResponses, context, "")
        serchadpter= SearchAdpter(productSerchResponse,context,"")
        rlc_cate = view.findViewById(R.id.rlc_cate)
        rlc_menu = view.findViewById(R.id.rlc_menu)
        txtpasscode = view.findViewById(R.id.txt_passcode)
        rlc_cate!!.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rlc_cate!!.adapter = catogoryAdpter

        rlc_menu!!.setHasFixedSize(true);
        mLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rlc_menu!!.layoutManager = mLayoutManager
        rlc_menu!!.adapter = menuAdpter

        Log.d("TAGx", "token: " + Preferences.getToken())

        catogoryAdpter!!.setAdapterInterface { cat_id, postion ->
            Log.d("TAG", "init: " + cat_id)
            categoryid = cat_id
            catename = postion
            adp = "adpter"
            view.search_box.setQuery("", false)

            view.txt_catname.visibility = View.VISIBLE
            itemCount = 1
            previousTotalItemCount = 0;
            count = 0
            menuResponses!!.clear()
            menuAdpter!!.notifyDataSetChanged()

            menulist(view)
            view.txt_catname.setText(postion)
//            view.txt_catname.setTextColor(Color.BLACK)
        }

        rlc_menu!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                var lastVisibleItemPosition = 0
                val totalItemCount = mLayoutManager!!.itemCount

                lastVisibleItemPosition =
                    (mLayoutManager as LinearLayoutManager).findLastVisibleItemPosition()

                Log.d("TAG", "totalItemCount : " + totalItemCount)
                Log.d("TAG", "lastVisibleItemPosition : " + lastVisibleItemPosition)

                if (totalItemCount < previousTotalItemCount) {
                    itemCount = startingPageIndex
                    previousTotalItemCount = totalItemCount
                    if (totalItemCount == 0) {
                        loading = true
                    }
                }

                if (loading && totalItemCount > previousTotalItemCount) {
                    loading = false
                    previousTotalItemCount = totalItemCount
                }

                if (!loading && lastVisibleItemPosition + visibleThreshold >= totalItemCount) {


                    if (count == 0) {
                        itemCount++
                        if (!adp.equals("search")) {
                            menulist(view)
                        }
                    }


                    Log.d("TAG", "init: " + "cat_final")
//                    onLoadMore(currentPage, totalItemCount, view)
                    loading = true
                }


            }
        })



        view.search_box.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }


            @RequiresApi(Build.VERSION_CODES.O)
            override fun onQueryTextSubmit(query: String): Boolean {
                searchStore(query.toString(), view)

                return false
            }

        })

        val bottomNavigationView: BottomNavigationView =
            requireActivity().findViewById(R.id.navigation)
        val menuItemId = bottomNavigationView.menu.getItem(1).itemId
        badge = bottomNavigationView.getOrCreateBadge(menuItemId)


        if (Preferences.getUserProfile().equals("0")) {
            Preferences.setcartcount("0")
            badge?.number = 0
            mainlogin(view)

        } else {

            retrohour(view)
            categorylist(view)

            checkcart(view)
        }
        if (Preferences.getpostcode().equals("0")) {
            dialogpostcode("first")

        } else {
            if(Preferences.getpostcode().equals("Pickup")){
                txtpasscode?.setText(Preferences.getpostcode())

            }else{
                txtpasscode?.setText("Delivery to - " + Preferences.getpostcode())

            }

        }
        view.bt_change.setOnClickListener {
            dialogpostcode("second")

        }


    }


    fun checkcart(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<CartCheckRequest> = RetrofitClient.getInstance().getapi()
            .getcart(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id),
                Preferences.getUserId()
            )
        call.enqueue(object : Callback<CartCheckRequest?> {
            override fun onResponse(
                call: Call<CartCheckRequest?>,
                response: Response<CartCheckRequest?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    val p: CartCheckRequest? = response.body()
                    if (p?.results?.size != 0) {


                        Preferences.setcartId(p?.results?.get(0)?.id.toString())
                        cartlist(view)
                    } else {
                        badge?.number = 0
                        myDialog.dismiss()

                    }

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<CartCheckRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun mainlogin(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(contex, "Loading...")

        val call: Call<MainLoginResponse> = RetrofitClient.getInstance().getapi()
            .login(
                MainLogin(
                    getResources().getString(R.string.app_username),
                    getResources().getString(R.string.app_password),
                    getResources().getString(R.string.restaurantid)
                )
            )
        call.enqueue(object : Callback<MainLoginResponse?> {
            override fun onResponse(
                call: Call<MainLoginResponse?>,
                response: Response<MainLoginResponse?>
            ) {
                val p: MainLoginResponse? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    Preferences.setToken(p?.token)
                    categorylist(view)
                    retrohour(view)


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<MainLoginResponse?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }


    fun retrohour(view: View) {


        /*
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<HourRequest> = RetrofitClient.getInstance().getapi()
            .gethour(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id).toInt()
            )
        call.enqueue(object : Callback<HourRequest?> {
            override fun onResponse(
                call: Call<HourRequest?>,
                response: Response<HourRequest?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    val p: HourRequest? = response.body()
                    if (p?.status.equals("closed")) {
                        view.txt_catname.setText("Closed")
                        view.txt_catname.setTextColor(Color.RED)
                    }


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<HourRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })


         */
    }

    fun categorylist(view: View) {


        view.search_box.isFocusable = false
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<ArrayList<CategoryListRequest>> = RetrofitClient.getInstance().getapi()
            .getcategory(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id)
            )
        call.enqueue(object : Callback<ArrayList<CategoryListRequest>?> {
            override fun onResponse(
                call: Call<ArrayList<CategoryListRequest>?>,
                response: Response<ArrayList<CategoryListRequest>?>
            ) {
                if (response.code() == 200) {
                    myDialog.dismiss()
                    val datas = response.body()

                    var status = view.txt_catname.text.toString()
                    if (status.equals("Closed")) {

                    } else {
                        view.txt_catname.setText(datas?.get(0)?.category)

                    }
                    if (categoryid == "") {

                        categoryid = datas?.get(0)?.categoryId.toString()

                    } else {
                        itemCount = 1
                        view.txt_catname.setText(catename)
                    }

                    catogoryAdpter!!.updateAdapter(datas, context, status)
                    if (!Preferences.getdips().equals("0")) {
                        categoryid=Preferences.getdips()
                        view.txt_catname.setText("Dips")
                        menulist(view)
                        Preferences.setdips("0")
                    } else {
                        menulist(view)
                    }



                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<ArrayList<CategoryListRequest>?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun cartlist(view: View) {
        //
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<CartListResponse> = RetrofitClient.getInstance().getapi()
            .getcartlistx(
                "Token " + Preferences.getToken(),
                Preferences.getcartId(), getResources().getString(R.string.restaurant_id)
            )

        call.enqueue(object : Callback<CartListResponse> {
            override fun onResponse(
                call: Call<CartListResponse>,
                response: Response<CartListResponse>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    val p: CartListResponse? = response.body()
                    var count = p?.count.toString()
                    Preferences.setcartcount(count)
                    badge?.number = count.toInt()


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<CartListResponse>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }


    fun menulist(view: View) {


        view.search_box.clearFocus()
        view.search_box.isFocusable = false

        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")


        val call: Call<MenuRequest> = RetrofitClient.getInstance().getapi()
            .getmenulist(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id), categoryid, itemCount
            )

        call.enqueue(object : Callback<MenuRequest> {
            override fun onResponse(
                call: Call<MenuRequest>,
                response: Response<MenuRequest>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()

                    var datas = ArrayList(Arrays.asList(*response.body()!!.results))


                    if (catename.equals("")) {

                        var status = view.txt_catname.text.toString()
                        if (status.equals("Closed")) {

                        } else {

//                            view.txt_catname.setTextColor(Color.BLACK)
                        }

                    }
                    var status = view.txt_catname.text.toString()

                    menuResponses?.addAll(datas)
                    menuAdpter!!.updateAdapter(menuResponses, context, status)

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else if (response.code() == 404) {
                    count++
                    myDialog.dismiss()

                } else {
                    myDialog.dismiss()
                }
            }

            override fun onFailure(call: Call<MenuRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun searchStore(product: String, view: View) {

        rlc_menu!!.setHasFixedSize(true);
        mLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rlc_menu!!.layoutManager = mLayoutManager
        rlc_menu!!.adapter = serchadpter
        view.txt_catname.visibility = View.GONE
        menuAdpter?.clearProductLiist()

        adp = "search"

        ///keybord hide
//        val imm: InputMethodManager =
//            activity?.getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

//        RequestBody.create(mediaType, "{\"query\":\"\\nquery productSearch ($token: String, $productName: String, $restaurantId: Int, $extra: String, $first: Int, $skip: Int ){\\n  # Category, ProductName is optional\\n  productSearch (token : $token, productName: $productName, restaurantId: $restaurantId, extra: $extra, ,first: $first, skip: $skip  ) {\\n    productName\\n    productId\\n    productUrl\\n    price\\n    taxExempt\\n    category{\\ncategoryId\\ncategory\\n}\\n  }\\n}\",\"variables\":{\"productName\":\"P\",\"restaurantId\":280,\"extra\":\"\",\"first\":100,\"skip\":0,\"token\":\"0o6jcui8mfhmp56we69kcmu5rkejtock\"}}");
//        RequestBody.create(mediaType, "{\"query\":\"\\nquery productSearch ($token: String, $productName: String, $restaurantId: Int, $extra: String, $first: Int, $skip: Int ){\\n  # Category, ProductName is optional\\n  productSearch (token : $token, productName: $productName, restaurantId: $restaurantId, extra: $extra, ,first: $first, skip: $skip  ) {\\n    productName\\n  productId\\n   productUrl\\n    price\\n    taxExempt\\n    category{\\ncategoryId\\ncategory\\n}\\n  }\\n}\",\"variables\":{\"productName\":\"P\",\"restaurantId\":280,\"extra\":\"\",\"first\":100,\"skip\":0,\"token\":\"0o6jcui8mfhmp56we69kcmu5rkejtock\"}}");

        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val ss = getResources().getString(R.string.restaurant_id)
        val sss = "{\"query\":\"\\nquery productSearch (\$token: String, \$productName: String, \$restaurantId: Int, \$extra: String, \$first: Int, \$skip: Int ){\\n  # Category, ProductName is optional\\n  productSearch (token : \$token, productName: \$productName, restaurantId: \$restaurantId, extra: \$extra, ,first: \$first, skip: \$skip  ) {\\n      productName\\n  productId\\n  productUrl\\n    price\\n    taxExempt\\n  extra\\n   category{\\n categoryId\\n category\\n  }\\n }\\n }\",\"variables\":{\"productName\":\"$product\",\"restaurantId\":\"$ss\",\"extra\":\"\",\"first\":100,\"skip\":0,\"token\":\"0o6jcui8mfhmp56we69kcmu5rkejtock\"}}"
        val mediaType = "application/json".toMediaTypeOrNull()
        val body = RequestBody.create(mediaType, sss)
        val call: Call<ProductSerchResponse> = RetrofitClient.getInstance().getapi()
            .serch_cart(body)
        call.enqueue(object : Callback<ProductSerchResponse> {
            override fun onResponse(call: Call<ProductSerchResponse>, response: Response<ProductSerchResponse>) {
                val p: ProductSerchResponse? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
//                    var datas = ArrayList(Arrays.asList(response.body()?.data?.productSearch))
                    var status = view.txt_catname.text.toString()
                    var datas = response.body()?.data?.productSearch
                    if (datas?.size == 0) {
                        Toast.makeText(
                            context,
                            resources.getString(R.string.serchmassage),
                            Toast.LENGTH_LONG
                        ).show()
                    } else {

                        serchadpter!!.updateAdapter(datas, context, status)
                    }

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<ProductSerchResponse>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun dialogpostcode(chech: String) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog.setContentView(R.layout.dialog_with_zipcode)
        dialog.setCancelable(true)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        val txtnote = dialog.findViewById<View>(R.id.txt_note) as TextView

        val btndever = dialog.findViewById<View>(R.id.btn_dilivery)
        val btnpickup = dialog.findViewById<View>(R.id.btn_pickup)
        val edt_text = dialog.findViewById<View>(R.id.edt_gettext) as EditText
        val btncheck = dialog.findViewById<View>(R.id.btn_check) as Button
        val btncancle = dialog.findViewById<View>(R.id.bt_close)
        if (chech.equals("first")) {
            btncancle.visibility = View.GONE

        } else {
            btncancle.visibility = View.VISIBLE

        }

        btnpickup.setBackgroundResource(R.drawable.squerpostbox)

        btndever.setBackgroundResource(R.drawable.squerbuttonbox)
        txtnote.text =
            "Note:Minimum purchase for delivery is " + Comman.currencyType + "10.00"

        btndever.setOnClickListener {
            btndever.setBackgroundResource(R.drawable.squerbuttonbox)
            btnpickup.setBackgroundResource(R.drawable.squerpostbox)
            btncheck.text = "Check"
            btncancle.visibility = View.GONE

            edt_text.visibility = View.VISIBLE
            txtnote.text =
                "Note:Minimum purchase for delivery is " + Comman.currencyType + "10.00"

//            if (Comman.isConnectingToInternet(context))  deletcartitem(productid, sequenceid,view)
        }
        btnpickup.setOnClickListener {
            btndever.setBackgroundResource(R.drawable.squerpostbox)
            btnpickup.setBackgroundResource(R.drawable.squerbuttonbox)
            btncheck.text = "OK"
            btncancle.visibility = View.GONE

            edt_text.visibility = View.GONE

            txtnote.text =
                "Note:You have to pickup the order from our restaurant."


//            if (Comman.isConnectingToInternet(context))  deletcartitem(productid, sequenceid,view)
        }
        dialog.findViewById<View>(R.id.bt_close).setOnClickListener {
            Preferences.setpostcode("Pickup")
            txtpasscode?.setText(Preferences.getpostcode())

            dialog.dismiss()
//            if (Comman.isConnectingToInternet(context))  deletcartitem(productid, sequenceid,view)
        }
        dialog.findViewById<View>(R.id.btn_check).setOnClickListener {
            if (edt_text.visibility == View.VISIBLE) {
                var final = edt_text.text.toString().trim()
                val code = removeSpace(final)
                Log.d("TAG", "dialogpostcode: $final")
                if (code.isEmpty()) {
                    Toast.makeText(
                        requireContext(),
                        "Enter post code for delivery",
                        Toast.LENGTH_SHORT
                    ).show()

                } else {
                    var s = "";
                    if (code.substring(0, 1).equals("L")) {
                        if (edt_text.text.toString().trim().length <= 3) {
                            Toast.makeText(
                                requireContext(),
                                "Invalid post code",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            s = code.substring(0, 4)
                            s.replace("\\s+$", "")
                            Log.d("TAG", "dialogpostcode: $code")

                            postcode(s, dialog,code)
                        }


                    } else if (code.substring(0, 1).equals("M")) {
                        if (edt_text.text.toString().trim().length <= 4) {
                            Toast.makeText(
                                requireContext(),
                                "Invalid post code",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        } else {
                            s = code.substring(0, 5)

                            postcode(s, dialog,code)
                        }

                    } else if (edt_text.text.toString().trim().length < 5) {
                        Toast.makeText(requireContext(), "Invalid post code", Toast.LENGTH_SHORT)
                            .show()

                    } else {
                        Log.d("TAG", "dialogpostcode: $s")

                        s = code
                        postcode(s, dialog,code)

                    }
                }

            } else {
                Preferences.setpostcode("Pickup")
                txtpasscode?.setText(Preferences.getpostcode())

                dialog.dismiss()
            }


        }
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        dialog.window!!.attributes = lp
    }

    fun removeSpace(s: String): String {
        var withoutspaces = ""
        for (i in 0 until s.length) {
            if (s[i] != ' ') withoutspaces += s[i]
        }
        return withoutspaces
    }

    private fun dialogcreate(str: String) {
        val dialog: Dialog =
            CustomDialogs.dialogRequestTimeOut(context, str)
        val okbtn = dialog.findViewById<Button>(R.id.btn_ok)
//        val cancelbtn = dialog.findViewById<Button>(R.id.btn_cancel)
        okbtn.text = "Ok"
//        cancelbtn.text = "Cancel"
        okbtn.setOnClickListener { v1: View? ->
            if (Comman.isConnectingToInternet(context)) {

                dialog.dismiss()
            }
        }
//        cancelbtn.setOnClickListener { v1: View? -> dialog.dismiss() }

    }


    fun postcode(postcose: String, dialog: Dialog,finalcode:String) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<Postcoderesponse> = RetrofitClient.getInstance().getapi()
            .postcodeapi(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id), postcose
            )

        call.enqueue(object : Callback<Postcoderesponse> {
            override fun onResponse(
                call: Call<Postcoderesponse>,
                response: Response<Postcoderesponse>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    if (response.body()?.message.equals("Data not exists")) {
                        dialogcreate("Delivery is not available in your area. Please select the Pickup option.")

                    } else {
                        dialogcreate("Delivery is available in your area.\nplease select your menu.")
                        Preferences.setpostcode(finalcode)
                        txtpasscode?.setText("Delivery to - " + Preferences.getpostcode())
                        dialog.dismiss()
                    }

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else if (response.code() == 404) {
                    count++
                    myDialog.dismiss()

                } else {
                    myDialog.dismiss()
                }
            }

            override fun onFailure(call: Call<Postcoderesponse>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

}
