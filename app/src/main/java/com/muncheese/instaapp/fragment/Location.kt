package com.muncheese.instaapp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muncheese.instaapp.R
import com.muncheese.instaapp.adpter.LocationAdpter
import com.muncheese.instaapp.model.request.LocationRequest
import com.muncheese.instaapp.model.response.LocationResponse
import com.muncheese.instaapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class Location : Fragment() {
    var rlc_cart: RecyclerView? = null
    var lolationListResponses: ArrayList<LocationResponse>? = null
    var locationAdpter: LocationAdpter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_location, container, false)
        init(view)
        return view
    }

    fun init(view: View) {
        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "My Location"
        lolationListResponses = ArrayList<LocationResponse>()
        locationAdpter = LocationAdpter(lolationListResponses, context)
        rlc_cart = view.findViewById(R.id.rlc_loc)
        rlc_cart!!.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rlc_cart!!.adapter = locationAdpter

        getlocation()
    }

    fun getlocation() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<LocationRequest> = RetrofitClient.getInstance().getapi()
            .location(
                "Token " + Preferences.getToken()
            )
        call.enqueue(object : Callback<LocationRequest?> {
            override fun onResponse(
                call: Call<LocationRequest?>,
                response: Response<LocationRequest?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var datas = ArrayList(Arrays.asList(*response.body()!!.results))
                    locationAdpter!!.updateAdapter(datas, context)
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<LocationRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }
}