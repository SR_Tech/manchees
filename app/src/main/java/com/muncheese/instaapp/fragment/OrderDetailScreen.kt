package com.muncheese.instaapp.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gauravk.bubblenavigation.BubbleNavigationLinearView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.adpter.OrderDetailListAdpter
import com.muncheese.instaapp.model.orderdetail.OrderDetail
import com.muncheese.instaapp.model.request.AddresslistRequest
import com.muncheese.instaapp.model.request.OrderDetailPageRequest
import com.muncheese.instaapp.model.response.OrderDetailPageResponse
import com.muncheese.instaapp.model.sendModel.PrintOrder
import com.muncheese.instaapp.util.*
import kotlinx.android.synthetic.main.fragment_order_detail_screen.view.*
import kotlinx.android.synthetic.main.toolbar.view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class OrderDetailScreen : Fragment() {
    var rlc_orderdetail: RecyclerView? = null
    var orderDetailListResponse: ArrayList<com.muncheese.instaapp.model.orderdetail.Result>? = null
    var orderDetailListAdpter: OrderDetailListAdpter? = null
    var orderno: String? = ""
    var retaurant: String? = null
    var orderdate: String? = null
    var subtotal: String? = null
    var tip: String? = null
    var tax: String? = null
    var discount: String? = null
    var ordertotal: String? = null
    var status: String? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_order_detail_screen, container, false)
        init(view)
        return view
    }

    fun init(view: View) {
        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        val leftarrow: RelativeLayout = view.findViewById<RelativeLayout>(R.id.title_bar_left)
        headerText.text = "Order Details"
        orderno = arguments?.getString("order_no")
//        retaurant = arguments?.getString("restaurant")
        orderdate = arguments?.getString("order_date")
        subtotal = arguments?.getString("subtotal")
        tip = arguments?.getString("tip")
        tax = arguments?.getString("tax")
        discount = arguments?.getString("discount")
        ordertotal = arguments?.getString("order_total")
        status = arguments?.getString("status")


        activity?.onBackPressedDispatcher?.addCallback(
            requireActivity(),
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (status.equals("payment")) {
                        // Log.d("TAG", "handleOnBackPressed: "+"nahi")
                    }else{
                        requireActivity().onBackPressed()
                    }
                }
            })

        if (status.equals("payment")) {

            view.btn_bottom.visibility = View.VISIBLE
            val bottomNavigationView: BottomNavigationView = requireActivity().findViewById(R.id.navigation)
            bottomNavigationView?.visibility=View.GONE


        } else {
            leftarrow.title_bar_left.visibility = View.VISIBLE
        }
        leftarrow.title_bar_left.setOnClickListener {
            if (leftarrow.title_bar_left.visibility == View.VISIBLE) {
                val myActivity = context as Dashbord
                val fragment2 = OrderListScreen()
                val fragmentManager = myActivity.supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.parent_foter, fragment2)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        }

        view.btn_bottom.setOnClickListener {
            if (view.btn_bottom.visibility == View.VISIBLE) {
                val intent = Intent(activity, Dashbord::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }

        view.txt_order_no.setText(orderno)
        view.txt_retrourent_name.setText(getResources().getString(R.string.restaurant_name))
        view.txt_order_date.setText(
            Comman.ConvertIntoDateFormat(
                "yyyy-MM-dd",
                "dd MMMM yyyy",
                orderdate.toString()
            )
        )
        view.txt_sub_total.setText(Comman.currencyType + subtotal)
        view.txt_tip.setText(Comman.currencyType + tip)
        view.txt_tax.setText(Comman.currencyType + tax)
        view.txt_discount.setText(Comman.currencyType + discount)
        view.txt_order_total.setText(Comman.currencyType + ordertotal)

        orderDetailListResponse = ArrayList<com.muncheese.instaapp.model.orderdetail.Result>()
        orderDetailListAdpter = OrderDetailListAdpter(orderDetailListResponse, context)
        rlc_orderdetail = view.findViewById(R.id.recycler_my_order_details)
        rlc_orderdetail!!.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rlc_orderdetail!!.adapter = orderDetailListAdpter

        getbillingaddress(view)
        fetorderdetail()
        printOrder()
    }


    fun getbillingaddress(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<AddresslistRequest> = RetrofitClient.getInstance().getapi()
            .getbillingaddress(
                "Token " + Preferences.getToken(),
                Preferences.getUserId()
            )

        call.enqueue(object : Callback<AddresslistRequest> {
            override fun onResponse(
                call: Call<AddresslistRequest>,
                response: Response<AddresslistRequest>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var datas = ArrayList(Arrays.asList(*response.body()!!.results))
                    if (datas.size > 0) {
//                        datas.get(0).state + ", " +
                        view.txt_shipping_address.setText(
                            datas.get(0).name + "\n" + datas.get(0).houseNumber + "\n" + datas.get(0).address + "\n" +
                                    datas.get(0).city + "\n" + datas.get(0).country + "\n" + datas.get(
                                0
                            ).zip
                        )


                    } else {

                    }


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<AddresslistRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    fun fetorderdetail() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<OrderDetail> = RetrofitClient.getInstance().getapi()
            .orderdetailx("Token " + Preferences.getToken(), orderno)
        call.enqueue(object : Callback<OrderDetail> {
            override fun onResponse(
                call: Call<OrderDetail>,
                response: Response<OrderDetail>
            ) {
//                val p: OrderDetailPageRequest? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    val datas =response.body()!!.results
                    if (datas.size == 0) {
                    } else {
                        orderDetailListAdpter?.updateAdapter(datas, context)
                    }
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<OrderDetail>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }


    //calliing printing api
    fun printOrder() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")


        val call: Call<ResponseBody> = RetrofitClient.getInstance().getapi()
            .printOrder(
                "Token " + Preferences.getToken(),
                PrintOrder(getResources().getString(R.string.restaurant_id), orderno)
            )
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {
                if (response.code() == 200) {
                    myDialog.dismiss()
                    Log.e("Prining call", "Success...")

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else if (response.code() == 400) {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.incorrect_cart_details),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }
}