package com.muncheese.instaapp.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muncheese.instaapp.R
import com.muncheese.instaapp.adpter.OrderListAdpter
import com.muncheese.instaapp.model.request.OrderListRequest
import com.muncheese.instaapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class OrderListScreen : Fragment() {
    lateinit var contex: Context
    var rlc_orderlist: RecyclerView? = null
    var orderListResponse: ArrayList<OrderListRequest>? = null
    var orderListAdpter: OrderListAdpter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_order_list_screen, container, false)
        init(view)
        return view
    }

    fun init(view: View) {
        contex = requireContext()
        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "Orders"
        orderListResponse = ArrayList<OrderListRequest>()
        orderListAdpter = OrderListAdpter(orderListResponse, context)
        rlc_orderlist = view.findViewById(R.id.rlc_order_list)
        rlc_orderlist!!.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rlc_orderlist!!.adapter = orderListAdpter
        orderlist()
    }

    fun orderlist() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<ArrayList<OrderListRequest>> = RetrofitClient.getInstance().getapi()
            .orderlist(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id),
                Preferences.getUserId()
            )
        call.enqueue(object : Callback<ArrayList<OrderListRequest>?> {
            override fun onResponse(
                call: Call<ArrayList<OrderListRequest>?>,
                response: Response<ArrayList<OrderListRequest>?>
            ) {
                val p: ArrayList<OrderListRequest>? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    val datas = response.body()
                    orderListAdpter!!.updateAdapter(datas, context)

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<ArrayList<OrderListRequest>?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun ordercancel() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<OrderListRequest> = RetrofitClient.getInstance().getapi()
            .ordercancel(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id)
            )
        call.enqueue(object : Callback<OrderListRequest?> {
            override fun onResponse(
                call: Call<OrderListRequest?>,
                response: Response<OrderListRequest?>
            ) {
                val p: OrderListRequest? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    Comman.getToast(
                        context,
                        resources.getString(R.string.credential_wrong)
                    )
                } else if (response.code() == 403) {
                    myDialog.dismiss()
                }
            }

            override fun onFailure(call: Call<OrderListRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }
}