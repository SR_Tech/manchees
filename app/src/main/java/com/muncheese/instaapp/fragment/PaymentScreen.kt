package com.muncheese.instaapp.fragment


import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.gson.JsonObject
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.activity.LoginScreen
import com.muncheese.instaapp.model.request.CustomerDataRequest
import com.muncheese.instaapp.model.request.FreeRequest
import com.muncheese.instaapp.model.request.OrderDetailRequest
import com.muncheese.instaapp.model.request.ShipmentRequest
import com.muncheese.instaapp.model.sendModel.AddFree
import com.muncheese.instaapp.model.sendModel.PrintOrder
import com.muncheese.instaapp.model.sendModel.UpdateFree
import com.muncheese.instaapp.model.sendModel.UpdateOrder
import com.muncheese.instaapp.util.*
import com.whiteelephant.monthpicker.MonthPickerDialog
import kotlinx.android.synthetic.main.fragment_payment_screen.view.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class PaymentScreen : Fragment() {

    private var tip = 0
    private var customTip = 0
    private var shippingId: kotlin.String? = "0"
    private var orderId: kotlin.String? = "0"
    private var subTotal: kotlin.String? = "0"
    private var phone_no: kotlin.String? = ""
    private var emailid: kotlin.String? = ""
    var count = 0
    var firstnamelast: String? = null
    var orderDetailScreen: OrderDetailRequest? = null
    var house_no: String = ""
    var pastalcode: String = ""
    var state: String = ""
    var city: String = ""
    var xaddress: String = ""
    var country: String = ""
    var name: String = ""

    var coupon_code: String = ""
    var isCouponApplied: Boolean = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_payment_screen, container, false)
        init(view)
        return view
    }

    fun init(view: View) {
        subTotal = Preferences.gettotal()
        country = requireArguments().getString("cuntry").toString()
        city = requireArguments().getString("city").toString()
        state = requireArguments().getString("state").toString()
        pastalcode = requireArguments().getString("zip").toString()
        xaddress = requireArguments().getString("address").toString()
        house_no = requireArguments().getString("house_no").toString()
        name = requireArguments().getString("name").toString()

        Log.e("Payment screen", "Name: "+name )
        Log.e("Payment screen", "House no: "+house_no )

        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "Payment"
        view.findViewById<View>(R.id.title_bar_left).setVisibility(View.VISIBLE)
        view.findViewById<View>(R.id.title_bar_left)
            .setOnClickListener({ v: View? -> activity?.onBackPressed() })

        if (Comman.isConnectingToInternet(context)) {
            getfrees(view)
            getshipmentfrees()
        }
//        view.txt_change_tip.setOnClickListener {
//            dialogCustomTip(view)
//        }

        view.btn_paynext.setOnClickListener {
            validation(view)
        }

        view.btn_apply_coupon.setOnClickListener {

            if(view.edt_coupon_code.getText().toString().isEmpty())
            {
                Comman.getToast(
                    context,
                    getString(R.string.coupon_validate),
                )
            }
            else
            {
                if(isCouponApplied)
                {
                    coupon_code = ""
                    Log.e("Coupon", "init: coupon_code:"+ coupon_code )
                    applyCouponCall(view)
                }
                else
                {
                    coupon_code = view.edt_coupon_code.getText().toString()
                    Log.e("Coupon", "init: coupon_code:"+ coupon_code )
                    applyCouponCall(view)
                }
            }
        }
        view.edt_month.setOnClickListener {

            datePicker("Month", view)
        }
        view.edt_year.setOnClickListener {
            datePicker("Year", view);
        }

        view.lly_cardchange.setOnClickListener {
            view.payment_details.visibility = View.VISIBLE
            view.lly_cardchange.setBackgroundResource(R.drawable.call_background_black)
            view.lly_cashchange.setBackgroundResource(R.drawable.call_background_stroke)
        }

        view.lly_cashchange.setOnClickListener {
            view.lly_cashchange.setBackgroundResource(R.drawable.call_background_black)
            view.lly_cardchange.setBackgroundResource(R.drawable.call_background_stroke)
            view.payment_details.visibility = View.GONE
        }
        getcustomerdate(view)

        view.lly_cardchange.setBackgroundResource(R.drawable.call_background_black)
        view.lly_cashchange.setBackgroundResource(R.drawable.call_background_stroke)

    }

    private fun datePicker(type: String, view: View) {
        val myCalendar = Calendar.getInstance()
        val builder: MonthPickerDialog.Builder =
            MonthPickerDialog.Builder(context, object : MonthPickerDialog.OnDateSetListener {
                override fun onDateSet(selectedMonth: Int, selectedYear: Int) {

                    val strSelectedMonth =
                        if (selectedMonth.toString().length > 1) (selectedMonth + 1).toString() + "" else "0" + (selectedMonth + 1)
                    view.edt_month.setText(strSelectedMonth)
                    view.edt_year.setText(selectedYear.toString() + "")

                }
            }, myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH])
        builder.setActivatedMonth(Calendar.MONTH)
            .setMinYear(1990)
            .setActivatedYear(myCalendar[Calendar.YEAR])
            .setMaxYear(2030)
            .setMinMonth(Calendar.JANUARY)
            .setTitle("Select $type")
            .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER) // .setMaxMonth(Calendar.OCTOBER)
            // .setYearRange(1890, 1890)
            // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
            //.showMonthOnly()
            // .showYearOnly()
            .setOnMonthChangedListener(object : MonthPickerDialog.OnMonthChangedListener {
                override fun onMonthChanged(selectedMonth: Int) {

                    val strSelectedMonth =
                        if (selectedMonth + "".length > 1) (selectedMonth + 1).toString() + "" else "0" + (selectedMonth + 1)
                    view.edt_month.setText(strSelectedMonth)
                }
            })
            .setOnYearChangedListener(object : MonthPickerDialog.OnYearChangedListener {
                override fun onYearChanged(selectedYear: Int) {

                    view.edt_year.setText(selectedYear.toString() + "")
                }
            })
            .build()
            .show()
    }



    fun validation(view: View) {
        if (view.payment_details.visibility == View.GONE) if (count == 0) {
            getorderdetail(view)
            count++
        } else {
            payment(view)
        } else {
            if (view.edt_card_holdern.getText().toString().isEmpty()) {
                Comman.getToast(
                    context,
                    resources.getString(R.string.card_holder_name_validate)
                )
            } else if (view.edt_credit_card_no.getText().toString().isEmpty()) {
                Comman.getToast(
                    context,
                    getString(R.string.card_no_validate),
                )

            } else if (view.edt_security_code.getText().toString().isEmpty()) {
                Comman.getToast(
                    context,
                    getString(R.string.security_code_validate),
                )

            } else if (view.edt_month.getText().toString().isEmpty()) {
                Comman.getToast(
                    context,
                    getString(R.string.month_validate),
                )

            } else if (view.edt_year.getText().toString().isEmpty()) {
                Comman.getToast(
                    context,
                    getString(R.string.year_validate),
                )

            }
            else {
                if (count == 0) {
                    getorderdetail(view)
                    count++
                } else {
                    payment(view)
                }
            }
        }

    }

    fun dialogCustomTip(view: View) {
        val dialog = context?.let { Dialog(it) }
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog?.setContentView(R.layout.dialog_delevery_tip)
        val radioGroup = dialog?.findViewById<View>(R.id.radio_grup) as RadioGroup
        val edtTip = dialog.findViewById<View>(R.id.edt_tip) as EditText
        (dialog?.findViewById<View>(R.id.amt_1) as RadioButton).text =
            "15%"
        (dialog.findViewById<View>(R.id.amt_2) as RadioButton).text =
            "20%"
        dialog.setCancelable(true)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            val radioButton =
                dialog.findViewById<View>(checkedId) as RadioButton // find the radiobutton by returned id
            if (radioButton.text.toString() == "Other amount") {
                edtTip.isEnabled = true
            } else {
                edtTip.isEnabled = false
            }
        }
        (dialog.findViewById<View>(R.id.btn_submit) as Button).setOnClickListener {
            // get selected radio button from radioGroup
            val selectedId = radioGroup.checkedRadioButtonId
            if (selectedId == -1) {
                Comman.getToast(context, resources.getString(R.string.select_tip_option))
            } else {
                // find the radiobutton by returned id
                val radioButton =
                    dialog.findViewById<View>(selectedId) as RadioButton
                if (radioButton.text == "Other amount") {
                    if (edtTip.text.toString() == "") {
                        Comman.getToast(context, "Please enter Other amount")
                    } else {
                        view.txt_tip.setText(Comman.currencyType + edtTip.text.toString())
                        //  customTip = edtTip.text.toString()
                        // tip = "0"
                        if (Comman.isConnectingToInternet(context)) {
                            updatefrees(view)
                        }
                        dialog.dismiss()
                    }
                } else {
                    if (radioButton.text.toString().contains(Comman.currencyType)) {
                        view.txt_tip.setText(radioButton.text.toString())
                        //   customTip = radioButton.text.toString().substring(1)
                        //  tip = "0"
                    } else {
                        view.txt_tip.setText(
                            Comman.currencyType + radioButton.text.toString().split("%".toRegex())
                                .toTypedArray()[0]
                        )
                        //  customTip = "0"
                        // tip = radioButton.text.toString().split("%".toRegex()).toTypedArray()[0]
                    }
                    dialog.dismiss()
                    if (Comman.isConnectingToInternet(context)) {
                        getfrees(view)
                    }
                }
            }
        }
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        dialog.window!!.attributes = lp
    }

    fun applyCouponCall(view: View)
    {

            var shipid = Preferences.getshippingid()

            if (shipid.equals("35")) {
                shipid = ""
            } else if (shipid.equals("34")) {
                shipid = "34"
            }
            val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

            val call: Call<FreeRequest> = RetrofitClient.getInstance().getapi()
                .getfreex(
                    "Token " + Preferences.getToken(),
                    AddFree(
                        0,
                        shipid,
                        getResources().getString(R.string.restaurant_id).toInt(),
                        Preferences.getUserId(), subTotal,
                        0, 0, coupon_code

                    )
                )
            call.enqueue(object : Callback<FreeRequest?> {
                override fun onResponse(
                    call: Call<FreeRequest?>,
                    response: Response<FreeRequest?>
                ) {
                    val p: FreeRequest? = response.body()

               //     print("Test discount"+p?.discount.toString())

                   //     Log.e("Response tag", "error: " +p?.error.toString())
                        Log.e("Response tag", "subTotal: " +p?.subTotal.toString())
                        Log.e("Response tag", "shippingFee: " +p?.shippingFee.toString())
                        Log.e("Response tag", "discount: " +p?.discount.toString())
                        Log.e("Response tag", "total: " +p?.total.toString())



                    if (response.code() == 200) {
                        myDialog.dismiss()
                        view.txt_subtotal.setText(Comman.currencyType + p?.subTotal)
                        view.txt_vat.setText(Comman.currencyType + p?.shippingFee)
                        view.txt_discount.setText(Comman.currencyType + p?.discount)
                        view.txt_tip.setText(Comman.currencyType + p?.tip)
                        view.txt_total.setText(Comman.currencyType + p?.total)

                        if(isCouponApplied)
                        {
                            isCouponApplied = false
                            view.btn_apply_coupon.setText("Apply coupon")
                            view.edt_coupon_code.setText("")
                            view.edt_coupon_code.isEnabled = true
                        }else
                        {
                            isCouponApplied = true
                            view.btn_apply_coupon.setText("Cancel coupon")
                            view.edt_coupon_code.isEnabled = false

                        }
                    } else if (response.code() == 401) {
                        myDialog.dismiss()
                        CustomDialogs.dialogSessionExpire(context)
                    } else if (response.code() == 400) {
                        myDialog.dismiss()
                        val errorObj = JSONObject(response.errorBody()!!.string())

                                if(errorObj.getString("error") == "Invalid Coupon")
                                {
                                    Comman.getToast(context,errorObj.getString("error"))

                                }
                            else if(errorObj.getJSONArray("error").get(0) == "This coupon is valid for Min. Purchase of £10.00")
                                {
                                var arrErr = errorObj.getJSONArray("error")
                              Comman.getToast(context, ""+arrErr.get(0))
                               // Log.e("Response tag", "Error: " +arrErr.get(0))
                            }
                            else
                            {
                                Comman.getToast(context,errorObj.getString("error"))
                            }



                    }else {
                        myDialog.dismiss()
                        Toast.makeText(
                            context,
                            resources.getString(R.string.no_response),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onFailure(call: Call<FreeRequest?>, t: Throwable) {
                    myDialog.dismiss()
                }
            })
        }
    fun getfrees(view: View) {
        var shipid = Preferences.getshippingid()

        if (shipid.equals("35")) {
            shipid = ""
        } else if (shipid.equals("34")) {
            shipid = "34"
        }
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<FreeRequest> = RetrofitClient.getInstance().getapi()
            .getfreex(
                "Token " + Preferences.getToken(),
                AddFree(
                    0,
                    shipid,
                    getResources().getString(R.string.restaurant_id).toInt(),
                    Preferences.getUserId(), subTotal,
                    0, 0, ""

                )
            )
        call.enqueue(object : Callback<FreeRequest?> {
            override fun onResponse(
                call: Call<FreeRequest?>,
                response: Response<FreeRequest?>
            ) {
                val p: FreeRequest? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    view.txt_subtotal.setText(Comman.currencyType + p?.subTotal)
                    view.txt_vat.setText(Comman.currencyType + p?.shippingFee)
                    view.txt_discount.setText(Comman.currencyType + p?.discount)
                    view.txt_tip.setText(Comman.currencyType + p?.tip)
                    view.txt_total.setText(Comman.currencyType + p?.total)

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<FreeRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }


    fun updatefrees(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<FreeRequest> = RetrofitClient.getInstance().getapi()
            .updatefree(
                "Token " + Preferences.getToken(),
                UpdateFree(
                    subTotal,
                    "0",
                    getResources().getString(R.string.restaurant_id),
                    Preferences.getUserId(), 0.toString()
                )

            )
        call.enqueue(object : Callback<FreeRequest?> {
            override fun onResponse(
                call: Call<FreeRequest?>,
                response: Response<FreeRequest?>
            ) {
                val p: FreeRequest? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    view.txt_subtotal.setText(Comman.currencyType + p?.subTotal)
                    view.txt_vat.setText(Comman.currencyType + p?.tax)
                    view.txt_discount.setText(Comman.currencyType + p?.discount)
                    view.txt_tip.setText(Comman.currencyType + p?.tip)
                    view.txt_total.setText(Comman.currencyType + p?.total)

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<FreeRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun getcustomerdate(view: View) {

        Log.e("TAG", "Userid: "+Preferences.getUserId() )
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")
        val call: Call<CustomerDataRequest> = RetrofitClient.getInstance().getapi()
            .getcustumer(
                "Token " + Preferences.getToken(),
                Preferences.getUserId()

            )


        call.enqueue(object : Callback<CustomerDataRequest?> {
            override fun onResponse(
                call: Call<CustomerDataRequest?>,
                response: Response<CustomerDataRequest?>
            ) {
                val p: CustomerDataRequest? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    firstnamelast =
                        p?.results?.get(0)?.customer?.firstName + p?.results?.get(0)?.customer?.lastName
                    phone_no = p?.results?.get(0)?.phoneNumber
                    emailid = p?.results?.get(0)?.customer?.email

                    //Log.e("Payment page", "onResponse: first name: "+ firstnamelast)

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<CustomerDataRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun getorderdetail(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        var subtotal = java.lang.Double.valueOf(
            view.txt_subtotal.text.toString().replace(Comman.currencyType, "")
        )
        var total = java.lang.Double.valueOf(
            view.txt_total.text.toString().replace(Comman.currencyType, "")
        )
        var tax = java.lang.Double.valueOf(
            view.txt_vat.text.toString().replace(Comman.currencyType, "")
        )
        var tip = java.lang.Double.valueOf(
            view.txt_tip.text.toString().replace(Comman.currencyType, "")
        )
        var discount = java.lang.Double.valueOf(
            view.txt_discount.text.toString().replace(Comman.currencyType, "")
        )
        var shipid = Preferences.getshippingid()
        var address = "";
        var shipaddress = "";
        if (shipid.equals("35")) {
            address =
                name + ", " + xaddress + ", " + house_no + ", " + city + ", " + state + ", " + country + ", " + pastalcode
            shipaddress = ""
        } else if (shipid.equals("34")) {
            address =
                name + ", " + xaddress + ", " + house_no + ", " + city + ", " + state + ", " + country + ", " + pastalcode
            shipaddress =
                name + ", " + xaddress + ", " + house_no + ", " + city + ", " + state + ", " + country + ", " + pastalcode
        }

     var addtest =   name + ", " + xaddress + ", " + house_no + ", " + pastalcode+ ", " + city + ", " + state + ", " + country

        //Zach Rahman, 38 Cavendish Rd, , LU3 1JQ, Luton, England (Bedfordshire), United Kingdom
        Log.e("Payment Page", "Address : "+address )
        Log.e("Payment Page", "Shipping Address : "+shipaddress )

        Log.e("Payment Page", "New Address : "+addtest )


        val call: Call<OrderDetailRequest> = RetrofitClient.getInstance().getapi()
            .getorderdetailx(
                "Token " + Preferences.getToken(),
                UpdateOrder(
                    view.edt_extra_annotation.text.toString(),
                    tip.toString(),
                    tax.toString(),
                    Preferences.getcartId(),
                    "active",
                    total.toString(),
                    Preferences.getUserId(),
                    "0",
                    subtotal.toString(),
                    Comman.paymenycurrencyType,
                    "0",
                    discount.toString(), shipaddress, address
                )
            )
        call.enqueue(object : Callback<OrderDetailRequest?> {
            override fun onResponse(
                call: Call<OrderDetailRequest?>,
                response: Response<OrderDetailRequest?>
            ) {
                val p: OrderDetailRequest? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    orderId = p?.orderId.toString()
                    orderDetailScreen = p
                    payment(view)

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<OrderDetailRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun getshipmentfrees() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<ShipmentRequest> = RetrofitClient.getInstance().getapi()
            .getshipmentmethod(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id)
            )
        call.enqueue(object : Callback<ShipmentRequest?> {
            override fun onResponse(
                call: Call<ShipmentRequest?>,
                response: Response<ShipmentRequest?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    val p: ShipmentRequest? = response.body()
                    if (p?.results?.size == 0) {
                        Comman.getToast(context, "No shipping method available")
                    } else {
                        shippingId = p?.results?.get(0)?.id.toString()
                    }

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<ShipmentRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun payment(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        var price = java.lang.Double.valueOf(
            view.txt_total.text.toString().replace(Comman.currencyType, "")
        )


        val reqObj = JsonObject()
        var carddata: JsonObject = JsonObject()
        var metadata: JsonObject = JsonObject()
        var address: JsonObject = JsonObject()
        var billingDetails: JsonObject = JsonObject()


        metadata.addProperty("order_id", orderId)
        metadata.addProperty("shippingmethod_id", Preferences.getshippingid())
        metadata.addProperty("restaurant_id", getResources().getString(R.string.restaurant_id))
        metadata.addProperty("phone", phone_no)
        metadata.addProperty("customer_id", Preferences.getUserId())
        metadata.addProperty("special_instruction", view.edt_extra_annotation.getText().toString())
        metadata.addProperty("name", name)


        address.addProperty("city", city)
//        address.addProperty("country", country)
        address.addProperty("state", country)
        address.addProperty("line1", xaddress)
        address.addProperty("line2", "")
        address.addProperty("postal_code", pastalcode)

        billingDetails.add("address", address)

        if (view.payment_details.visibility == View.VISIBLE) {
            carddata.addProperty("number", view.edt_credit_card_no.getText().toString())
            carddata.addProperty("exp_month", view.edt_month.getText().toString())
            carddata.addProperty("exp_year", view.edt_year.getText().toString())
            carddata.addProperty("cvc", view.edt_security_code.getText().toString())
            reqObj.addProperty("type", "card")
        } else {
            carddata.addProperty("number", "")
            carddata.addProperty("exp_month", "")
            carddata.addProperty("exp_year", "")
            carddata.addProperty("cvc", "")
            reqObj.addProperty("type", "cash")
        }


        Log.d("TAG", "payment: " + view.edt_month.getText().toString())
        Log.d("TAG", "payment: " + view.edt_year.getText().toString())

        reqObj.addProperty("amount", price)
        reqObj.addProperty("currency", Comman.paymenycurrencyType)
        reqObj.addProperty("receipt_email", emailid)
        reqObj.addProperty("source", "mobile");


        reqObj.add("card", carddata)
        reqObj.add("metadata", metadata)
//        reqObj.add("address", address)
        reqObj.add("billing_details", billingDetails)

        val call: Call<ResponseBody> = RetrofitClient.getInstance().getapi()
            .payment(
                "Token " + Preferences.getToken(),
                reqObj
            )
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    removeFragFromBackStack()
                    Comman.getToast(context, resources.getString(R.string.order_placed))
                    val myActivity = context as Dashbord
                    val fragment2 = OrderDetailScreen()
                    val bundle = Bundle()
                    bundle.putString("order_no", orderDetailScreen?.orderId.toString())
                    bundle.putString("order_date", orderDetailScreen?.createdAt)
                    bundle.putString("subtotal", orderDetailScreen?.subtotal)
                    bundle.putString("tip", orderDetailScreen?.tip)
                    bundle.putString("tax", orderDetailScreen?.tax)
                    bundle.putString("discount", orderDetailScreen?.discount)
                    bundle.putString("order_total", orderDetailScreen?.total)
                    bundle.putString("status", "payment")
                    fragment2.arguments = bundle

                    val fragmentManager = myActivity.supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.parent_foter1, fragment2)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else if (response.code() == 400) {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.incorrect_cart_details),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    private fun removeFragFromBackStack(): Boolean {
        try {
            val myActivity = context as Dashbord
            val manager: FragmentManager = myActivity.supportFragmentManager
            val fragsList: List<Fragment> = manager.getFragments()
            if (fragsList.size == 0) {
                return true
            }
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            return true
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }
}