package com.muncheese.instaapp.fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.gauravk.bubblenavigation.BubbleNavigationLinearView
import com.gauravk.bubblenavigation.BubbleToggleView
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.model.request.ProfileRequest
import com.muncheese.instaapp.model.response.UpadateProfileResponse
import com.muncheese.instaapp.model.sendModel.ProfileData
import com.muncheese.instaapp.util.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class Profile : Fragment() {

    private var countryCodeArray: ArrayList<String>? = null
    var username: String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_profile, container, false)
        init(view)
        return view

    }


    fun init(view: View) {

//        countryCodeArray = ArrayList()
//        countryCodeArray!!.add("+1")
//        countryCodeArray!!.add("+91")

        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "Profile"
//        view.findViewById<View>(R.id.title_bar_left).setVisibility(View.VISIBLE)
//        view.findViewById<View>(R.id.title_bar_left)
//            .setOnClickListener(View.OnClickListener { v: View? ->
//                val intent = Intent(activity, Dashbord::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                startActivity(intent)
//            })

//        view.edtprofile_code.setOnClickListener {
//            setCountryCode(view)
//        }

        view.changeprofile_password.setOnClickListener {
            val myActivity = context as Dashbord
            val fragment2 = ChangePassword()
            val fragmentManager: FragmentManager = myActivity.supportFragmentManager
            val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.parent_foter, fragment2)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
        click(view)
        getprofile(view)
    }

    //countryCode dialog
    private fun setCountryCode(view: View) {
        // setup the alert builder
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Select Salutation")
        // add a list
        builder.setItems(Comman.GetStringArray(countryCodeArray),
            DialogInterface.OnClickListener { dialog, position ->
                view.edtprofile_code.setText(countryCodeArray!![position])
                dialog.dismiss()
            })
        // create and show the alert dialog
        val dialog = builder.create()
        dialog.show()
    }

    fun click(view: View) {
        view.btn_profilesave.setOnClickListener {
            if (view.edt_profilesalutation.getText().toString().isEmpty()) {
                Comman.getToast(
                    context,
                    resources.getString(R.string.salutation_validate)
                )
            } else if (view.profile_fName.getText().toString().isEmpty()) {
                Comman.getToast(
                    context,
                    getString(R.string.fname_validate),
                )

            } else if (view.profile_lName.getText().toString().isEmpty()) {
                Comman.getToast(
                    context,
                    getString(R.string.lname_validate),
                )

            } else if (view.edtprofile_m_no.getText().toString().isEmpty()) {
                Comman.getToast(
                    context,
                    getString(R.string.mb_validate),
                )

            } else if (view.profile_email.getText().toString().isEmpty()) {
                Comman.getToast(
                    context,
                    getString(R.string.enter_email_validate),
                )

            } else {
                updateprofile(view)
            }
        }


    }


    fun getprofile(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<ProfileRequest> = RetrofitClient.getInstance().getapi()
            .getprofile(
                "Token " + Preferences.getToken(),
                Preferences.getUserId()
            )
        call.enqueue(object : Callback<ProfileRequest?> {
            override fun onResponse(
                call: Call<ProfileRequest?>,
                response: Response<ProfileRequest?>
            ) {
                val p: ProfileRequest? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    username = p?.results?.get(0)?.customer?.username.toString()
                    view.edt_profilesalutation.setText(p?.results?.get(0)?.salutation)
                    view.profile_fName.setText(p?.results?.get(0)?.customer?.firstName)
                    view.profile_lName.setText(p?.results?.get(0)?.customer?.lastName)
                    Preferences.setusername(p?.results?.get(0)?.customer?.username)

                    var strMNo: String? = null
                    if (p?.results?.get(0)?.phoneNumber?.contains("+1") == true) {
                        strMNo = p?.results?.get(0)?.phoneNumber.toString().replace("+1", "")
                        view.edtprofile_code.setText("+1")
                    } else {
                        strMNo = p?.results?.get(0)?.phoneNumber.toString().replace("+91", "")
                        view.edtprofile_code.setText("+91")
                    }
                    view.edtprofile_m_no.setText(strMNo)
                    view.profile_email.setText(p?.results?.get(0)?.customer?.email)

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<ProfileRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun updateprofile(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<UpadateProfileResponse> = RetrofitClient.getInstance().getapi()
            .updateprofile(
                "Token " + Preferences.getToken(),
                Preferences.getUserId(),
                ProfileData(
                    view.edt_profilesalutation.getText().toString(),
                    view.profile_fName.getText().toString(),
                    view.profile_lName.getText().toString(),
                    username,
                    view.profile_email.getText().toString(),
                    Comman.CurrentTimeStamp(),
                    view.edtprofile_code.getText().toString() +
                            view.edtprofile_m_no.getText().toString(),


                    )
            )
        call.enqueue(object : Callback<UpadateProfileResponse?> {
            override fun onResponse(
                call: Call<UpadateProfileResponse?>,
                response: Response<UpadateProfileResponse?>
            ) {
                val p: UpadateProfileResponse? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    updatecustumer(view)
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else if (response.code() == 400) {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<UpadateProfileResponse?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }
    fun updatecustumer(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<UpadateProfileResponse> = RetrofitClient.getInstance().getapi()
            .custumerprofile(
                "Token " + Preferences.getToken(),
                Preferences.getUserId(),
                ProfileData(
                    view.edt_profilesalutation.getText().toString(),
                    view.profile_fName.getText().toString(),
                    view.profile_lName.getText().toString(),
                    username,
                    view.profile_email.getText().toString(),
                    Comman.CurrentTimeStamp(),
                    view.edtprofile_code.getText().toString() +
                            view.edtprofile_m_no.getText().toString(),


                    )
            )
        call.enqueue(object : Callback<UpadateProfileResponse?> {
            override fun onResponse(
                call: Call<UpadateProfileResponse?>,
                response: Response<UpadateProfileResponse?>
            ) {
                val p: UpadateProfileResponse? = response.body()
                if (response.code() == 200) {
                    myDialog.dismiss()
                    Comman.getToast(context, "Profile updated successfully")
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else if (response.code() == 400) {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<UpadateProfileResponse?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }
}