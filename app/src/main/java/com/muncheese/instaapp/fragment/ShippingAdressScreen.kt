package com.muncheese.instaapp.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.muncheese.instaapp.Dashbord
import com.muncheese.instaapp.R
import com.muncheese.instaapp.model.request.AddressUpdateRequest
import com.muncheese.instaapp.model.request.AddresslistRequest
import com.muncheese.instaapp.model.request.ShipmentRequest
import com.muncheese.instaapp.model.request.StatelistRequest
import com.muncheese.instaapp.model.response.AddresslistResponse
import com.muncheese.instaapp.model.sendModel.AddBillingAddress
import com.muncheese.instaapp.util.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.fragment_address_screen.view.*
import kotlinx.android.synthetic.main.fragment_shipping_adress_screen.view.*
import kotlinx.android.synthetic.main.fragment_shipping_adress_screen.view.billing_house_no
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ShippingAdressScreen : Fragment() {
    var contex: Context? = null
    var isshipAddrAvail = false
    var isbillshipping = false
    var isshipaddavail = false
    var ispickup = false
    var usebillasshipping = false
    private var stateNameList: ArrayList<String>? = null
    var shipaddressid: String = ""
    var billaddressid: String = ""
    lateinit var addresslist: ArrayList<AddresslistResponse>
    var pastalcode: String = ""
    var state: String = ""
    var city: String = ""
    var xaddress: String = ""
    var country: String = "UK"
    var name: String = ""
    var houseno: String = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View =
            inflater.inflate(R.layout.fragment_shipping_adress_screen, container, false)

        init(view)
        return view
    }

    fun init(view: View) {
        Log.d("TAG", "init: "+Preferences.getpizzacolle())
        Log.d("TAG", "init: "+Preferences.getpizz2adeliver())

        Log.d("TAG", "init: "+Preferences.getpizzadeliver())

        view.radio_grup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val rb = group.findViewById<View>(checkedId) as RadioButton
            if (checkedId > -1) {
                if (rb.text.equals("PickUp")) {
//                    Log.d("TAG", "init123: "+Preferences.getpizzadeliver())
//
//                    Log.d("TAG", "init123: "+Preferences.getpizz2adeliver().equals("20734"))
//                    Log.d("TAG", "init123: "+Preferences.getpizzadeliver().equals("20735"))

                    if (Preferences.getpizzadeliver().equals("20734")) {
                        dialog("Because you have a 'Pizza Delivery Deal' or 'Any 2 Pizzas Delivery Deal' item in your cart, you are unable to choose the Pickup option. You can either delete this item or choose the Delivery option.")
                        rb.isChecked = false

                    } else if(Preferences.getpizz2adeliver().equals("20735")){
                        dialog("Because you have a 'Pizza Delivery Deal' or 'Any 2 Pizzas Delivery Deal' item in your cart, you are unable to choose the Pickup option. You can either delete this item or choose the Delivery option.")
                        rb.isChecked = false
                    }else {
                        ispickup = true
                        isshipAddrAvail = false
                        Preferences.setpickup("PICKUP")
                        view.rlt_n.visibility = View.GONE
                        Preferences.setshippingid("35") //production
//                        Preferences.setshippingid("26") //dev


                        isshipaddavail = true
                    }

                } else if (rb.text.equals("Delivery")) {
                    var prise: Double = Preferences.gettotal().toDouble()
                    if (prise < 10.0) {
                        dialog("Delivery option is available for a minimum purchase of £10")
                        rb.isChecked = false

                    } else if (Preferences.getpizzacolle().equals("20733")) {
                        dialog("Because you have a 'Pizza Collection Deal' item in your cart, you are unable to choose the Delivery option. You can either delete this item or choose the Pickup option.")

                        rb.isChecked = false
                    } else {
                        ispickup = false
                        isshipAddrAvail = true
                        isshipaddavail = false
                        Preferences.setpickup("SHIPMENT")
                        view.rlt_n.visibility = View.VISIBLE
                        Preferences.setshippingid("34")//production
//                        Preferences.setshippingid("25") //dev

                        getshippingaddress(view)
                        getbillingaddress(view)
//                        getstatelist()
                    }


                }
//                else if (rb.text.equals("Delivery - GTA - $12 (free above $50)")) {
//                    ispickup = false
//                    isshipAddrAvail = true
//                    isshipaddavail = false
//                    Preferences.setpickup("SHIPMENT")
//                    view.rlt_n.visibility = View.VISIBLE
//                    Preferences.setshippingid("25")
//                    getshippingaddress(view)
//                    getbillingaddress(view)
//                    getstatelist()
//                }

                // Toast.makeText(context, rb.text, Toast.LENGTH_SHORT).show()
            }
        })
        if(Preferences.getpizzadeliver()==null) {
            Preferences.setpizzdeliver("0")

        }
        if (Preferences.getpizz2adeliver()==null){
            Preferences.setpizz2deliver("0")

        }
        if (Preferences.getpizzacolle()==null){
            Preferences.setpizzacolle("0")

        }
        if (Preferences.getpizzacolle().equals("20733")) {
            dialog("Because you have a 'Pizza Collection Deal' item in your cart, you are unable to choose the Delivery option. You can either delete this item or choose the Pickup option.")
        } else if (Preferences.getpizz2adeliver().equals("20734") || Preferences.getpizzadeliver()
                .equals("20735")
        ) {
            dialog("Because you have a 'Pizza Delivery Deal' or 'Any 2 Pizzas Delivery Deal' item in your cart, you are unable to choose the Pickup option. You can either delete this item or choose the Delivery option.")
        }
        if (Preferences.getpostcode().equals("Pickup")) {
            (view.radio_grup.getChildAt(0) as RadioButton).isChecked = true

            (view.radio_grup.getChildAt(1) as RadioButton).isChecked = false

        } else {
            view.billing_edt_zip_scode.setText(Preferences.getpostcode())
            (view.radio_grup.getChildAt(1) as RadioButton).isChecked = true
            (view.radio_grup.getChildAt(0) as RadioButton).isChecked = false

        }
        ////////////ad//
        contex = context

        val headerText: TextView = view.findViewById<TextView>(R.id.toolbar_header_text)
        headerText.text = "Shipping Address"

        view.findViewById<View>(R.id.title_bar_left).setVisibility(View.VISIBLE)
        view.findViewById<View>(R.id.title_bar_left)
            .setOnClickListener(View.OnClickListener { v: View? -> activity?.onBackPressed() })


        getshipmentfrees(view)
        click(view)

        view.billing_edt_shcity.setOnClickListener {
//            ["Luton", "Charlton", "Sundon", "Toddington", "Barton-le-Clay", "Harlington", "Streatley", "Houghton Regis"
            stateNameList = ArrayList()
            stateNameList!!.add("Luton")
            stateNameList!!.add("Charlton")
            stateNameList!!.add("Sundon")
            stateNameList!!.add("Toddington")
            stateNameList!!.add("Barton-le-Clay")
            stateNameList!!.add("Harlington")
            stateNameList!!.add("Streatley")
            stateNameList!!.add("Houghton Regis")


            setstateDialog("billing", view)

        }
    }

    private fun dialog(str: String) {
        val dialog: Dialog = CustomDialogs.dialogRequestTimeOut(context, str)
        val okbtn = dialog.findViewById<Button>(R.id.btn_ok)
        okbtn.text = "Ok"

        okbtn.setOnClickListener { v1: View? ->
            if (Comman.isConnectingToInternet(context)) {

                dialog.dismiss()
            }
        }

    }

    //state dialog
    private fun setstateDialog(type: String, view: View) {

        // setup the alert builder
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Select City")
        // add a list
        builder.setItems(Comman.GetStringArray(stateNameList),
            DialogInterface.OnClickListener { dialog, position ->
                view.billing_edt_shcity.setText(stateNameList?.get(position))
                dialog.dismiss()
            })

        // create and show the alert dialog
        val dialog = builder.create()
        dialog.show()
    }


    fun truefase(view: View, check: String) {

        if (check.equals("true")) {
            view.billing_edt_sname.setEnabled(true);
            view.billing_edt_address_sline.setEnabled(true);
            view.billing_edt_shcity.setEnabled(true);
            view.billing_edt_zip_scode.setEnabled(true);
            view.billing_edt_shstate.setEnabled(true);
            view.billing_edt_shcountry.setEnabled(true);
            view.billing_edt_house_sno.setEnabled(true);
        } else {
            view.billing_edt_sname.setEnabled(false);
            view.billing_edt_address_sline.setEnabled(false);
            view.billing_edt_shcity.setEnabled(false);
            view.billing_edt_zip_scode.setEnabled(false);
            view.billing_edt_shstate.setEnabled(false);
            view.billing_edt_shcountry.setEnabled(false);
            view.billing_edt_house_sno.setEnabled(false);
        }

    }

    fun validation(view: View) {
        if (view.billing_edt_sname.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.name)
            )
        } else if (view.billing_edt_address_sline.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.address)
            )

        } else if (view.billing_edt_shcity.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.city)
            )

        } else if (view.billing_edt_zip_scode.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.zip_postal)
            )

        } else if (view.billing_edt_shstate.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.state)
            )

        } else if (view.billing_edt_shcountry.getText().toString().isEmpty()) {
            Comman.getToast(
                context, "Enter " +
                        resources.getString(R.string.country)
            )

        } else {
            isshipaddavail = true
            if (addresslist.size != 0) {
                Log.d("TAG", "click: " + "12")
                updateshippingaddress(view)
            } else {
                addshippingaddress(view)
                Log.d("TAG", "click: " + "11")
            }
        }

    }

    fun click(view: View) {

        view.checkbox_current_saddress.setOnClickListener {


            if (view.checkbox_current_saddress.isChecked) {
                truefase(view, "false")
                isshipAddrAvail = true


            } else {
                isshipAddrAvail = false;
                truefase(view, "true")
            }

        }
//        view.checkbox_useasbilling.setOnClickListener {
//
//            if(isshipaddavail){
//                if (view.checkbox_useasbilling.isChecked) {
//                    isshipaddavail = true
//                } else {
//                    isshipaddavail = false;
//
//                }
//            }else{
//                view.checkbox_useasbilling.isChecked=false
//                Toast.makeText(
//                    context,
//                    "Please  add billing Address first",
//                    Toast.LENGTH_LONG
//                ).show()
//            }
//
//
//        }
        view.checkbox_useasbilling.setOnClickListener {

            if (view.checkbox_useasbilling.isChecked) {
                usebillasshipping = true
            } else {
                usebillasshipping = false;

            }

        }


        view.btn_shnext.setOnClickListener {

//            Log.e("Ship", "click: Name "+ name )
//            Log.e("Ship", "click: Houseno "+ houseno )
//            Log.e("Ship", "click: xaddress "+ xaddress )



            if (isshipaddavail) {
                if (isshipAddrAvail) {
                    Log.d("TAG", "click: " + "1")
                    if (usebillasshipping) {
                        if (isbillshipping) {
                            Log.e("TAG", "click: " + "2")

                            updatebillingaddress(
                                view, country,
                                city,
                                state,
                                pastalcode,
                                xaddress, name, houseno
                            )
                        } else {
                            Log.e("TAG", "click: " + "3")
                            addbillingaddress(
                                view, country,
                                city,
                                state,
                                pastalcode,
                                xaddress, name, houseno
                            )
                        }

                    } else {
                        Log.e("TAG", "click: " + "4")
                        Log.e("Ship", "click: Name "+ name )
                        Log.e("Ship", "click: Houseno "+ houseno )
                        Log.e("Ship", "click: xaddress "+ xaddress )
                        val myActivity = context as Dashbord
                        val fragment2 = AddressScreen()
                        // String backStateName = fragment2.getClass().getName();
                        val bundle = Bundle()
                        bundle.putString("cuntry", country)
                        bundle.putString("city", city)
                        bundle.putString("state", state)
                        bundle.putString("zip", pastalcode)
                        bundle.putString("address", xaddress)
                        bundle.putString("house_no", houseno)
                        bundle.putString("name", name)
                        fragment2.arguments = bundle


                        val fragmentManager = myActivity.supportFragmentManager
                        val fragmentTransaction = fragmentManager.beginTransaction()
                        fragmentTransaction.replace(R.id.parent_foter, fragment2)
                        fragmentTransaction.addToBackStack(null)
                        fragmentTransaction.commit()

                    }

                } else if (ispickup) {
                    Log.e("TAG", "click: " + "5")

                    val myActivity = context as Dashbord
                    val fragment2 = AddressScreen()
                    // String backStateName = fragment2.getClass().getName();
                    val bundle = Bundle()
                    bundle.putString("cuntry", country)
                    bundle.putString("city", city)
                    bundle.putString("state", state)
                    bundle.putString("zip", pastalcode)
                    bundle.putString("address", xaddress)
                    bundle.putString("house_no", houseno)
                    bundle.putString("name", name)
                    fragment2.arguments = bundle

                    val fragmentManager = myActivity.supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.parent_foter, fragment2)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                } else {
                    Log.e("TAG", "click: " + "6")
                    if (view.rlt_n.visibility == View.VISIBLE) {
                        validation(view)
                    } else {
                        Toast.makeText(
                            context,
                            "Please select Delivary or Pickup",
                            Toast.LENGTH_LONG
                        ).show()
                    }

                }
            } else {
                validation(view)
            }

        }


    }


    fun getshipmentfrees(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<ShipmentRequest> = RetrofitClient.getInstance().getapi()
            .get_shipmentmethod(
                "Token " + Preferences.getToken(),
                getResources().getString(R.string.restaurant_id)
            )
        call.enqueue(object : Callback<ShipmentRequest?> {
            override fun onResponse(
                call: Call<ShipmentRequest?>,
                response: Response<ShipmentRequest?>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    val p: ShipmentRequest? = response.body()
                    if (p?.results?.size == 0) {
                        Comman.getToast(context, "No shipping method available")
                    } else {
//                        view.rdi_extra.setText(p?.results?.get(0)?.name)
//                        view.rdi_pickup.setText(p?.results?.get(1)?.name)
//                        view.rdi_dilivery.setText(p?.results?.get(2)?.name)


//                        Log.d(
//                            "0",
//                            "onResponse: " + p?.results?.get(0)?.name + " " + p?.results?.get(1)?.name + " " + p?.results?.get(
//                                2
//                            )?.name
//                        )
                    }

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<ShipmentRequest?>, t: Throwable) {
                myDialog.dismiss()
            }
        })
    }

    fun getshippingaddress(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<AddresslistRequest> = RetrofitClient.getInstance().getapi()
            .getshippingaddress(
                "Token " + Preferences.getToken(),
                Preferences.getUserId()
            )

        call.enqueue(object : Callback<AddresslistRequest> {
            override fun onResponse(
                call: Call<AddresslistRequest>,
                response: Response<AddresslistRequest>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    addresslist = ArrayList(Arrays.asList(*response.body()!!.results))
                    Log.d("TAG", "onResponse: " + addresslist.size)
                    if (addresslist.size > 0) {
                        Log.e("TAG", "onResponse: " +addresslist.get(0))

                        isshipaddavail = true
                        view.ll_current_saddress.visibility = View.VISIBLE
                        view.txt_snote.visibility = View.VISIBLE
                        view.checkbox_current_saddress.isChecked = true
                        Log.d("TAG", "onResponse: " + addresslist.get(0).name)
                        shipaddressid = addresslist.get(0).id.toString()
                        state = addresslist.get(0).state
                        pastalcode = addresslist.get(0).zip
                        city = addresslist.get(0).city
                        xaddress = addresslist.get(0).address
                        name = addresslist.get(0).name
                        houseno = addresslist.get(0).houseNumber
//                        + datas.get(0).state + ", "
                        view.edt_current_saddress.setText(
                            addresslist.get(0).name + ",\n" + addresslist.get(0).houseNumber + ",\n" +xaddress+
                                    ",\n" +addresslist.get(0).city +", "+ addresslist.get(0).country + ",\n" + addresslist.get(
                                0
                            ).zip
                        )

                        //addresslist.get(0).name + ",\n" + addresslist.get(0).houseNumber + ",\n" +
                        //                                    addresslist.get(0).city + ",\n " + addresslist.get(0).country + ",\n" + addresslist.get(
                        //                                0
                        //                            ).zip
                        Log.e("TAG", "onResponse: " +addresslist.get(0).name + ",\n" + addresslist.get(0).houseNumber + ",\n" +xaddress+
                                 ",\n " +addresslist.get(0).city +", "+ addresslist.get(0).country + ",\n" + addresslist.get(
                            0
                        ).zip)

                        //  isshipAddrAvail = true;
                        truefase(view, "false")



                        isshipAddrAvail = true

                    } else {
                        truefase(view, "true")
                        isshipaddavail = false
                        Log.d("TAG", "onResponse: " + "")
                    }


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<AddresslistRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    fun getstatelist() {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")


        stateNameList = ArrayList()
        val call: Call<StatelistRequest> = RetrofitClient.getInstance().getapi()
            .getstatelist("Token " + Preferences.getToken())

        call.enqueue(object : Callback<StatelistRequest> {
            override fun onResponse(
                call: Call<StatelistRequest>,
                response: Response<StatelistRequest>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var datas = ArrayList(Arrays.asList(*response.body()!!.results))
                    if (datas.size > 0) {

                        for (i in 0 until datas.size) {
//                            stateNameList!!.add(datas.get(i).state)
                        }
                    } else {

                    }

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<StatelistRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    fun addshippingaddress(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<AddressUpdateRequest> = RetrofitClient.getInstance().getapi()
            .addshippingaddress(
                "Token " + Preferences.getToken(),
                AddBillingAddress(
                    "",
                    country,
                    view.billing_edt_house_sno.text.toString(),
                    view.billing_edt_shcity.text.toString(),
                    view.billing_edt_zip_scode.text.toString(),
                    view.billing_edt_address_sline.text.toString(),
                    Preferences.getUserId(),
                    view.billing_edt_shstate.text.toString(),
                    1,
                    view.billing_edt_sname.text.toString()
                )
            )

        call.enqueue(object : Callback<AddressUpdateRequest> {
            override fun onResponse(
                call: Call<AddressUpdateRequest>,
                response: Response<AddressUpdateRequest>
            ) {

                if (response.code() == 201) {
                    myDialog.dismiss()
                    Log.e("TAG", "ship Name: "+view.billing_edt_sname.text.toString() )
                    Log.e("TAG", "ship House: "+view.billing_edt_house_sno.text.toString() )
                    if (usebillasshipping) {
                        if (isbillshipping) {
                            updatebillingaddress(
                                view,
                                country,
                                view.billing_edt_shcity.text.toString(),
                                view.billing_edt_shstate.text.toString(),
                                view.billing_edt_zip_scode.text.toString(),
                                view.billing_edt_address_sline.text.toString(),
                                view.billing_edt_sname.text.toString(),
                                view.billing_edt_house_sno.text.toString()
                            )
                        } else {
                            addbillingaddress(
                                view, country,
                                view.billing_edt_shcity.text.toString(),
                                view.billing_edt_shstate.text.toString(),
                                view.billing_edt_zip_scode.text.toString(),
                                view.billing_edt_address_sline.text.toString(),
                                view.billing_edt_sname.text.toString(),
                                view.billing_edt_house_sno.text.toString()
                            )
                        }


                    } else {
                        val myActivity = context as Dashbord
                        val fragment2 = PaymentScreen()
                        val bundle = Bundle()
                        bundle.putString("name", view.billing_edt_sname.getText().toString() )
                        bundle.putString("cuntry", country)
                        bundle.putString("city", view.billing_edt_shcity.getText().toString())
                        bundle.putString("state", view.billing_edt_shstate.text.toString())
                        bundle.putString("zip", view.billing_edt_zip_scode.getText().toString())
                        bundle.putString(
                            "address",
                            view.billing_edt_address_sline.getText().toString()
                        )
                        bundle.putString("house_no", view.billing_house_no.getText().toString())

                        Log.e("Shipping screen", "P3 Name: "+view.billing_edt_sname.getText().toString() )
                        Log.e("Shipping screen", "P3 House no: "+view.billing_house_no.getText().toString() )

                        fragment2.arguments = bundle
                        val fragmentManager = myActivity.supportFragmentManager
                        val fragmentTransaction = fragmentManager.beginTransaction()
                        fragmentTransaction.replace(R.id.parent_foter, fragment2)
                        fragmentTransaction.addToBackStack(null)
                        fragmentTransaction.commit()
                    }


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<AddressUpdateRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    fun updateshippingaddress(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")


        val call: Call<AddressUpdateRequest> = RetrofitClient.getInstance().getapi()
            .updateshippingaddress(
                "Token " + Preferences.getToken(), shipaddressid,
                AddBillingAddress(
                    "",
                    country,
                    view.billing_edt_house_sno.text.toString(),
                    view.billing_edt_shcity.text.toString(),
                    view.billing_edt_zip_scode.text.toString(),
                    view.billing_edt_address_sline.text.toString(),
                    Preferences.getUserId(),
                    view.billing_edt_shstate.text.toString(),
                    1,
                    view.billing_edt_sname.text.toString()
                )
            )

        call.enqueue(object : Callback<AddressUpdateRequest> {
            override fun onResponse(
                call: Call<AddressUpdateRequest>,
                response: Response<AddressUpdateRequest>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()

                    if (usebillasshipping) {

                        if (isbillshipping) {
                            updatebillingaddress(
                                view,
                                country,
                                view.billing_edt_shcity.text.toString(),
                                view.billing_edt_shstate.text.toString(),
                                view.billing_edt_zip_scode.text.toString(),
                                view.billing_edt_address_sline.text.toString(),
                                view.billing_edt_sname.text.toString(),
                                view.billing_edt_house_sno.text.toString(),
                            )
                        } else {
                            addbillingaddress(
                                view, country,
                                view.billing_edt_shcity.text.toString(),
                                view.billing_edt_shstate.text.toString(),
                                view.billing_edt_zip_scode.text.toString(),
                                view.billing_edt_address_sline.text.toString(),
                                view.billing_edt_sname.text.toString(),
                                view.billing_edt_house_sno.text.toString()
                            )
                        }


                    } else {
                        val myActivity = context as Dashbord
                        val fragment2 = AddressScreen()
                        val bundle = Bundle()
                        bundle.putString("cuntry", country)
                        bundle.putString("city", view.billing_edt_shcity.getText().toString())
                        bundle.putString("state", view.billing_edt_shstate.text.toString())
                        bundle.putString("zip", view.billing_edt_zip_scode.getText().toString())
                        bundle.putString(
                            "address",
                            view.billing_edt_address_sline.getText().toString()
                        )
                        fragment2.arguments = bundle
                        val fragmentManager = myActivity.supportFragmentManager
                        val fragmentTransaction = fragmentManager.beginTransaction()
                        fragmentTransaction.replace(R.id.parent_foter, fragment2)
                        fragmentTransaction.addToBackStack(null)
                        fragmentTransaction.commit()

                    }

                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<AddressUpdateRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }


    fun getbillingaddress(view: View) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<AddresslistRequest> = RetrofitClient.getInstance().getapi()
            .getbillingaddress(
                "Token " + Preferences.getToken(),
                Preferences.getUserId()
            )

        call.enqueue(object : Callback<AddresslistRequest> {
            override fun onResponse(
                call: Call<AddresslistRequest>,
                response: Response<AddresslistRequest>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()
                    var addresslist = ArrayList(Arrays.asList(*response.body()!!.results))
                    if (addresslist.size > 0) {
                        billaddressid = addresslist.get(0).id.toString()
                        Log.e("Billing", "name: "+addresslist.get(0).name)
                        isbillshipping = true;


                    } else {

                    }


                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<AddresslistRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    fun addbillingaddress(
        view: View,
        country: String,
        city: String,
        state: String,
        passcode: String,
        address: String,
        name: String,
        house: String
    ) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")

        val call: Call<AddressUpdateRequest> = RetrofitClient.getInstance().getapi()
            .addaddress(
                "Token " + Preferences.getToken(),
                AddBillingAddress(
                    "",
                    country,
                    house,
                    city,
                    passcode,
                    address,
                    Preferences.getUserId(),
                    country,
                    1,
                    name
                )
            )

        call.enqueue(object : Callback<AddressUpdateRequest> {
            override fun onResponse(
                call: Call<AddressUpdateRequest>,
                response: Response<AddressUpdateRequest>
            ) {

                if (response.code() == 201) {
                    myDialog.dismiss()
                    val myActivity = context as Dashbord
                    val fragment2 = PaymentScreen()
                    // String backStateName = fragment2.getClass().getName();
                    val bundle = Bundle()
                    bundle.putString("cuntry", country)
                    bundle.putString("city", city)
                    bundle.putString("state", state)
                    bundle.putString("zip", pastalcode)
                    bundle.putString("address", xaddress)
                    bundle.putString("house_no", houseno)
                    bundle.putString("name", name)
                    Log.e("Shipping screen", "P1 Name: "+name )
                    Log.e("Shipping screen", "P2 House no: "+houseno )
                    fragment2.arguments = bundle

                    val fragmentManager = myActivity.supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.parent_foter, fragment2)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<AddressUpdateRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }

    fun updatebillingaddress(
        view: View,
        country: String,
        city: String,
        state: String,
        passcode: String,
        address: String,
        name: String,
        house: String
    ) {
        val myDialog = DialogsUtils.showProgressDialog(context, "Loading...")


        val call: Call<AddressUpdateRequest> = RetrofitClient.getInstance().getapi()
            .updateaddress(
                "Token " + Preferences.getToken(), billaddressid,
                AddBillingAddress(
                    "",
                    country,
                    house,
                    city,
                    passcode,
                    address,
                    Preferences.getUserId(),
                    state,
                    1,
                    name
                )
            )

        call.enqueue(object : Callback<AddressUpdateRequest> {
            override fun onResponse(
                call: Call<AddressUpdateRequest>,
                response: Response<AddressUpdateRequest>
            ) {

                if (response.code() == 200) {
                    myDialog.dismiss()

                    val myActivity = context as Dashbord
                    val fragment2 = PaymentScreen()
                    // String backStateName = fragment2.getClass().getName();
                    val bundle = Bundle()
                    bundle.putString("cuntry", country)
                    bundle.putString("city", city)
                    bundle.putString("state", state)
                    bundle.putString("zip", pastalcode)
                    bundle.putString("address", xaddress)
                    bundle.putString("name", name)
                    bundle.putString("house_no", houseno)

                    Log.e("Shipping screen", "P2 Name: "+name )
                    Log.e("Shipping screen", "P2 House no: "+houseno )

                    fragment2.arguments = bundle

                    val fragmentManager = myActivity.supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.parent_foter, fragment2)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                } else if (response.code() == 401) {
                    myDialog.dismiss()
                    CustomDialogs.dialogSessionExpire(context)
                } else {
                    myDialog.dismiss()
                    Toast.makeText(
                        context,
                        resources.getString(R.string.no_response),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<AddressUpdateRequest>, t: Throwable) {
                myDialog.dismiss()
            }
        })

    }


}