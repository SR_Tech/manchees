package com.muncheese.instaapp.model

data class ChangePassword(
    val message: String,
    val msg: String,
    val status: String
)