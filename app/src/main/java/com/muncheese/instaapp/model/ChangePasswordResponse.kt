package com.muncheese.instaapp.model

data class ChangePasswordResponse(
    val message: String,
    val msg: String,
    val status: String
)