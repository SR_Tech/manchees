package com.muncheese.instaapp.model;

import org.json.JSONObject;

public class SelectedModel {
    private int id;
    private boolean isChecked;
    private String qty;
    private double price;
    private String type;

    public SelectedModel(int id, boolean isChecked, String qty, double price, String type) {
        this.id = id;
        this.isChecked = isChecked;
        this.qty = qty;
        this.price = price;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
