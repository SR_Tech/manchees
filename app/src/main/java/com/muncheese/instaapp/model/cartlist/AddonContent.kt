package com.muncheese.instaapp.model.cartlist

data class AddonContent(
    val addon_content: AddonContentX,
    val cartitem: Cartitem,
    val cartitem_addon_id: Int
)