package com.muncheese.instaapp.model.cartlist

data class AddonContentX(
    val addon_content_id: Int,
    val parent_addon: ParentAddon,
    val price: String,
    val title: String
)