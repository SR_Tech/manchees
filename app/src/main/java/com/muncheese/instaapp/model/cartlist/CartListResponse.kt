package com.muncheese.instaapp.model.cartlist

data class CartListResponse(
    val count: Int,
    val results: List<Result>,
    val total_cost: String
)