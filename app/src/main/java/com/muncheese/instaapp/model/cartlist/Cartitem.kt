package com.muncheese.instaapp.model.cartlist

data class Cartitem(
    val cart: Int,
    val cart_item_id: Int,
    val extra: String,
    val price: String,
    val product: Int,
    val quantity: Int,
    val size: Int
)