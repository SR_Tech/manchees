package com.muncheese.instaapp.model.cartlist

data class Result(
    val addon_content: List<AddonContent>,
    val cart: Int,
    val cart_item_id: Int,
    val extra: String,
    val price: String,
    val product: Product,
    val product_price: String,
    val quantity: Int,
    val size: Size
)