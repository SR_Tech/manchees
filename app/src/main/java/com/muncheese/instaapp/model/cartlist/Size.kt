package com.muncheese.instaapp.model.cartlist

data class Size(
    val category: Category,
    val category_size_id: Int,
    val size: String
)