package com.muncheese.instaapp.model.fetchsize

data class FetchSizeItem(
    val price: String,
    val product: Product,
    val product_size_price_id: Int,
    val size: Size
)