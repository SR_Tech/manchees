package com.muncheese.instaapp.model.fetchsize

data class Product(
    val caption: String,
    val category: Int,
    val extra: String,
    val media: String,
    val optional: Boolean,
    val price: String,
    val product_id: Int,
    val product_name: String,
    val product_url: String,
    val restaurant: Int,
    val status: String,
    val tax_exempt: Boolean
)