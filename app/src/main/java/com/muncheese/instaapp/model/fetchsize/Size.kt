package com.muncheese.instaapp.model.fetchsize

data class Size(
    val category: Int,
    val category_size_id: Int,
    val size: String
)