package com.muncheese.instaapp.model.newcartmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartRequest {
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("results")
    @Expose
    private CartResponse[] results = null;
    @SerializedName("total_cost")
    @Expose
    private String totalCost;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public CartResponse[] getResults() {
        return results;
    }

    public void setResults(CartResponse[] results) {
        this.results = results;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }
}
