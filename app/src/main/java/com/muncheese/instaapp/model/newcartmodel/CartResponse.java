package com.muncheese.instaapp.model.newcartmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.util.List;

public class CartResponse {
    @SerializedName("cart_item_id")
    @Expose
    private Integer cartItemId;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("extra")
    @Expose
    private String extra;
    @SerializedName("cart")
    @Expose
    private Integer cart;
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("size")
    @Expose
    private Size size;
    @SerializedName("addon_content")
    @Expose
    private List<addon_content> addon_content = null;


    public Integer getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(Integer cartItemId) {
        this.cartItemId = cartItemId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public Integer getCart() {
        return cart;
    }

    public void setCart(Integer cart) {
        this.cart = cart;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public List<com.muncheese.instaapp.model.newcartmodel.addon_content> getAddon_content() {
        return addon_content;
    }

    public void setAddon_content(List<com.muncheese.instaapp.model.newcartmodel.addon_content> addon_content) {
        this.addon_content = addon_content;
    }
}
