package com.muncheese.instaapp.model.newcartmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParentAddon {
    @SerializedName("addon_id")
    @Expose
    private Integer addonId;
    @SerializedName("addon_title")
    @Expose
    private String addonTitle;
    @SerializedName("selection_method")
    @Expose
    private String selectionMethod;
    @SerializedName("restaurant")
    @Expose
    private Integer restaurant;

    public Integer getAddonId() {
        return addonId;
    }

    public void setAddonId(Integer addonId) {
        this.addonId = addonId;
    }

    public String getAddonTitle() {
        return addonTitle;
    }

    public void setAddonTitle(String addonTitle) {
        this.addonTitle = addonTitle;
    }

    public String getSelectionMethod() {
        return selectionMethod;
    }

    public void setSelectionMethod(String selectionMethod) {
        this.selectionMethod = selectionMethod;
    }

    public Integer getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Integer restaurant) {
        this.restaurant = restaurant;
    }
}
