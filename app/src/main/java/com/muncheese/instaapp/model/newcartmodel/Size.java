package com.muncheese.instaapp.model.newcartmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Size {
    @SerializedName("category_size_id")
    @Expose
    private Integer categorySizeId;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("size")
    @Expose
    private String size;

    public Integer getCategorySizeId() {
        return categorySizeId;
    }

    public void setCategorySizeId(Integer categorySizeId) {
        this.categorySizeId = categorySizeId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
