package com.muncheese.instaapp.model.newcartmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class addon_content {

    @SerializedName("addon_content_id")
    @Expose
    private Integer addonContentId;
    @SerializedName("parent_addon")
    @Expose
    private ParentAddon parentAddon;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("price")
    @Expose
    private String price;

    public Integer getAddonContentId() {
        return addonContentId;
    }

    public void setAddonContentId(Integer addonContentId) {
        this.addonContentId = addonContentId;
    }

    public ParentAddon getParentAddon() {
        return parentAddon;
    }

    public void setParentAddon(ParentAddon parentAddon) {
        this.parentAddon = parentAddon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
