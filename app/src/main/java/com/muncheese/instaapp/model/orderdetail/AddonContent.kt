package com.muncheese.instaapp.model.orderdetail

data class AddonContent(
    val addon_content: AddonContentX
)