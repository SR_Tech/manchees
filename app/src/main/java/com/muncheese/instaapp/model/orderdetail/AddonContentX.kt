package com.muncheese.instaapp.model.orderdetail

data class AddonContentX(
    val addon_content_id: Int,
    val parent_addon: ParentAddon,
    val price: String,
    val title: String
)