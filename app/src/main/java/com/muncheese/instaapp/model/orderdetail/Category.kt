package com.muncheese.instaapp.model.orderdetail

data class Category(
    val category: String,
    val category_id: Int,
    val category_url: String,
    val restaurant_id: Int
)