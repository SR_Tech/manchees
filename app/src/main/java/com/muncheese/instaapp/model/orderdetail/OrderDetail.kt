package com.muncheese.instaapp.model.orderdetail

data class OrderDetail(
    val count: Int,
    val results: List<Result>,
    val total_cost: String
)