package com.muncheese.instaapp.model.orderdetail

data class Result(
    val addon_content: List<AddonContent>,
    val extra: String,
    val order: Int,
    val order_item_id: Int,
    val price: String,
    val product: Product,
    val product_price: String,
    val quantity: Int,
    val size: Size
)