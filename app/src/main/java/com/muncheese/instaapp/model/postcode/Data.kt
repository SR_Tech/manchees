package com.muncheese.instaapp.model.postcode

data class Data(
    val id: Int,
    val restaurant: Int,
    val zip_code: String
)