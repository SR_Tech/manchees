package com.muncheese.instaapp.model.postcode

data class Postcoderesponse(
    val `data`: List<Data>,
    val message: String,
    val status: Boolean
)