package com.muncheese.instaapp.model.productaddon

data class Addon(
    val addon_id: Int,
    val addon_title: String,
    val restaurant: Int,
    val selection_method: String
)