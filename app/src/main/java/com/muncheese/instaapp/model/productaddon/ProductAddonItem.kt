package com.muncheese.instaapp.model.productaddon

data class ProductAddonItem(
    val addon: Addon,
    val addon_quantity: Int,
    val addon_sequence: Int,
    val product: Int,
    val product_addon_id: Int,
    val size: Size
)