package com.muncheese.instaapp.model.productcontain

data class ParentAddon(
    val addon_id: Int,
    val addon_title: String,
    val restaurant: Int,
    val selection_method: String
)