package com.muncheese.instaapp.model.productcontain

data class ProductContainItem(
    val addon_content_id: Int,
    val parent_addon: ParentAddon,
    val price: String,
    val title: String
)