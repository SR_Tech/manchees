package com.muncheese.instaapp.model.productserch

data class Category(
    val category: String,
    val categoryId: String
)