package com.muncheese.instaapp.model.productserch

data class Data(
    val productSearch: List<ProductSearch>
)