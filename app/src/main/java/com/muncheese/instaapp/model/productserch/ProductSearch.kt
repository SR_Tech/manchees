package com.muncheese.instaapp.model.productserch

data class ProductSearch(
    val category: Category,
    val extra: String,
    val price: Double,
    val productName: String,
    val productUrl: String,
    val taxExempt: Boolean,
    var productId: Int

)