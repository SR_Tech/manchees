package com.muncheese.instaapp.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.muncheese.instaapp.model.response.OrderDetailPageResponse;

public class OrderDetailPageRequest {
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("results")
    @Expose
    private OrderDetailPageResponse[] results = null;
    @SerializedName("total_cost")
    @Expose
    private String totalCost;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }


    public OrderDetailPageResponse[] getResults() {
        return results;
    }

    public void setResults(OrderDetailPageResponse[] results) {
        this.results = results;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }
}
