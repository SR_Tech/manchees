package com.muncheese.instaapp.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetailRequest {
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("tip")
    @Expose
    private String tip;
    @SerializedName("service_fee")
    @Expose
    private String serviceFee;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("shipping_fee")
    @Expose
    private String shippingFee;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("extra")
    @Expose
    private String extra;
    @SerializedName("restaurant_request")
    @Expose
    private String restaurantRequest;
    @SerializedName("shipping_address_text")
    @Expose
    private String shippingAddressText;
    @SerializedName("billing_address_text")
    @Expose
    private String billingAddressText;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("customer")
    @Expose
    private Integer customer;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(String serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(String shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getRestaurantRequest() {
        return restaurantRequest;
    }

    public void setRestaurantRequest(String restaurantRequest) {
        this.restaurantRequest = restaurantRequest;
    }

    public String getShippingAddressText() {
        return shippingAddressText;
    }

    public void setShippingAddressText(String shippingAddressText) {
        this.shippingAddressText = shippingAddressText;
    }

    public String getBillingAddressText() {
        return billingAddressText;
    }

    public void setBillingAddressText(String billingAddressText) {
        this.billingAddressText = billingAddressText;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getCustomer() {
        return customer;
    }

    public void setCustomer(Integer customer) {
        this.customer = customer;
    }
}
