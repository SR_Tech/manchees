package com.muncheese.instaapp.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.muncheese.instaapp.model.response.OrderListResponse;
import com.muncheese.instaapp.model.response.OrderListRestaurantResponse;
import com.muncheese.instaapp.model.response.OrderListShippingmethodResponse;

public class OrderListRequest {
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("order")
    @Expose
    private OrderListResponse order;
    @SerializedName("shippingmethod")
    @Expose
    private OrderListShippingmethodResponse shippingmethod;
    @SerializedName("restaurant")
    @Expose
    private OrderListRestaurantResponse restaurant;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("payout_status")
    @Expose
    private String payoutStatus;
    @SerializedName("payout_message")
    @Expose
    private String payoutMessage;
    @SerializedName("payout_id")
    @Expose
    private String payoutId;
    @SerializedName("correlation_id")
    @Expose
    private String correlationId;
    @SerializedName("signature")
    @Expose
    private Object signature;

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public OrderListResponse getOrder() {
        return order;
    }

    public void setOrder(OrderListResponse order) {
        this.order = order;
    }

    public OrderListShippingmethodResponse getShippingmethod() {
        return shippingmethod;
    }

    public void setShippingmethod(OrderListShippingmethodResponse shippingmethod) {
        this.shippingmethod = shippingmethod;
    }

    public OrderListRestaurantResponse getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(OrderListRestaurantResponse restaurant) {
        this.restaurant = restaurant;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPayoutStatus() {
        return payoutStatus;
    }

    public void setPayoutStatus(String payoutStatus) {
        this.payoutStatus = payoutStatus;
    }

    public String getPayoutMessage() {
        return payoutMessage;
    }

    public void setPayoutMessage(String payoutMessage) {
        this.payoutMessage = payoutMessage;
    }

    public String getPayoutId() {
        return payoutId;
    }

    public void setPayoutId(String payoutId) {
        this.payoutId = payoutId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public Object getSignature() {
        return signature;
    }

    public void setSignature(Object signature) {
        this.signature = signature;
    }

}
