package com.muncheese.instaapp.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.muncheese.instaapp.model.response.SerchDataResponse;

public class SerchRequest {
    @SerializedName("data")
    @Expose
    private SerchDataResponse data;

    public SerchDataResponse getData() {
        return data;
    }

    public void setData(SerchDataResponse data) {
        this.data = data;
    }
}
