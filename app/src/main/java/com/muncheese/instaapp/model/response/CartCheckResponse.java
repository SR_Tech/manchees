package com.muncheese.instaapp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartCheckResponse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("extra")
    @Expose
    private String extra;
    @SerializedName("billing_address_id")
    @Expose
    private Object billingAddressId;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("shipping_address_id")
    @Expose
    private Object shippingAddressId;
    @SerializedName("restaurant")
    @Expose
    private Integer restaurant;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public Object getBillingAddressId() {
        return billingAddressId;
    }

    public void setBillingAddressId(Object billingAddressId) {
        this.billingAddressId = billingAddressId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Object getShippingAddressId() {
        return shippingAddressId;
    }

    public void setShippingAddressId(Object shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    public Integer getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Integer restaurant) {
        this.restaurant = restaurant;
    }
}
