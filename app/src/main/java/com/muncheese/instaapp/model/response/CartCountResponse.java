package com.muncheese.instaapp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartCountResponse {

    @SerializedName("cartItemCount")
    @Expose
    private CartCount2Response cartItemCount;

    public CartCount2Response getCartItemCount() {
        return cartItemCount;
    }

    public void setCartItemCount(CartCount2Response cartItemCount) {
        this.cartItemCount = cartItemCount;
    }
}
