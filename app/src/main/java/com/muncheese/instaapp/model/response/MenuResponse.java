package com.muncheese.instaapp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuResponse {

    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_url")
    @Expose
    private String productUrl;
    @SerializedName("price")
    @Expose
    private String price;
//    @SerializedName("media")
//    @Expose
//    private String media;
//    @SerializedName("caption")
//    @Expose
//    private String caption;
    @SerializedName("extra")
    @Expose
    private String extra;
//    @SerializedName("image")
//    @Expose
//    private Object image;
//    @SerializedName("status")
//    @Expose
//    private String status;
//    @SerializedName("tax_exempt")
//    @Expose
//    private Boolean taxExempt;
//    @SerializedName("source_id")
//    @Expose
//    private Object sourceId;
//    @SerializedName("restaurant")
//    @Expose
//    private Integer restaurant;
    @SerializedName("category")
    @Expose
    private Integer category;


    @SerializedName("productId")
    @Expose
    private String productid;
    @SerializedName("productName")
    @Expose
    private String productName1;
    @SerializedName("productUrl")
    @Expose
    private String productUrl1;


//    @SerializedName("category")
//    @Expose
//    private Category category1;
//
//    public Category getCategory1() {
//        return category1;
//    }
//
//    public void setCategory1(Category category1) {
//        this.category1 = category1;
//    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProductName1() {
        return productName1;
    }

    public void setProductName1(String productName1) {
        this.productName1 = productName1;
    }

    public String getProductUrl1() {
        return productUrl1;
    }

    public void setProductUrl1(String productUrl1) {
        this.productUrl1 = productUrl1;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

//    public String getMedia() {
//        return media;
//    }
//
//    public void setMedia(String media) {
//        this.media = media;
//    }
//
//    public String getCaption() {
//        return caption;
//    }
//
//    public void setCaption(String caption) {
//        this.caption = caption;
//    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

//    public Object getImage() {
//        return image;
//    }
//
//    public void setImage(Object image) {
//        this.image = image;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public Boolean getTaxExempt() {
//        return taxExempt;
//    }
//
//    public void setTaxExempt(Boolean taxExempt) {
//        this.taxExempt = taxExempt;
//    }
//
//    public Object getSourceId() {
//        return sourceId;
//    }
//
//    public void setSourceId(Object sourceId) {
//        this.sourceId = sourceId;
//    }
//
//    public Integer getRestaurant() {
//        return restaurant;
//    }
//
//    public void setRestaurant(Integer restaurant) {
//        this.restaurant = restaurant;
//    }
//
//    public Integer getCategory() {
//        return category;
//    }
//
//    public void setCategory(Integer category) {
//        this.category = category;
//    }
}
