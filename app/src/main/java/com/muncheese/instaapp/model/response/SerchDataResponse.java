package com.muncheese.instaapp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SerchDataResponse {

    @SerializedName("productSearch")
    @Expose
    private ArrayList<MenuResponse> productSearch = null;

    public ArrayList<MenuResponse> getProductSearch() {
        return productSearch;
    }

    public void setProductSearch(ArrayList<MenuResponse> productSearch) {
        this.productSearch = productSearch;
    }
}
