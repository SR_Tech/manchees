package com.muncheese.instaapp.model.sendModel;

public class AddFree {

    final Integer no_tax_total;
    final String shipping_id;

    final Integer restaurant_id;

    final String customer_id;
    final String sub_total;
    final Integer tip;
    final Integer custom_tip;
    final String coupon;


    public AddFree(Integer no_tax_total, String shipping_id, Integer restaurant_id, String customer_id, String sub_total, Integer tip, Integer custom_tip, String coupon_code) {
        this.no_tax_total = no_tax_total;
        this.shipping_id = shipping_id;
        this.restaurant_id = restaurant_id;
        this.customer_id = customer_id;
        this.sub_total = sub_total;
        this.tip = tip;
        this.custom_tip = custom_tip;
        this.coupon = coupon_code;

    }
}