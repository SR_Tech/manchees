package com.muncheese.instaapp.model.sendModel;

public class AddtoCart {
    final String updated_at;
    final String sequence_id;
    final String extra;
    final String product_id;
    final String quantity;
    final String ingredient_id;
    final String cart_id;


    public AddtoCart(String updated_at, String sequence_id, String extra, String product_id, String quantity, String ingredient_id, String cart_id) {
        this.updated_at = updated_at;
        this.sequence_id = sequence_id;
        this.extra = extra;
        this.product_id = product_id;
        this.quantity = quantity;
        this.ingredient_id = ingredient_id;
        this.cart_id = cart_id;
    }
}
