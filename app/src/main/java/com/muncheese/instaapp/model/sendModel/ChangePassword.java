package com.muncheese.instaapp.model.sendModel;

public class ChangePassword {
    final String old_password;
    final String username;
    final String new_password1;
    final String new_password2;

    public ChangePassword(String old_password, String username, String new_password1, String new_password2) {
        this.old_password = old_password;
        this.username = username;
        this.new_password1 = new_password1;
        this.new_password2 = new_password2;
    }
}
