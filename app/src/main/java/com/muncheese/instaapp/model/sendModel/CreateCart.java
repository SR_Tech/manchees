package com.muncheese.instaapp.model.sendModel;

public class CreateCart {
    final String customer_id;
    final String restaurant;


    public CreateCart(String customer_id, String restaurant) {
        this.customer_id = customer_id;
        this.restaurant = restaurant;
    }
}