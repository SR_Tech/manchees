package com.muncheese.instaapp.model.sendModel;

public class MainLogin {
    final String username;
    final String password;
    final String restaurant_id;

    public MainLogin(String username, String password, String restaurant_id) {
        this.username = username;
        this.password = password;
        this.restaurant_id = restaurant_id;
    }
}
