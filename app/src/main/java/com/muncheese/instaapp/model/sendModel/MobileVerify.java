
package com.muncheese.instaapp.model.sendModel;

public class MobileVerify {
    final String phone_number;
    final String verification_code;


    public MobileVerify(String phone_number, String verification_code) {
        this.phone_number = phone_number;
        this.verification_code = verification_code;
    }
}
