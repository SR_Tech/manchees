package com.muncheese.instaapp.model.sendModel;

import com.google.gson.JsonObject;

public class PaymentDetail {
    final String receipt_email;
    final JsonObject metadata;
    final JsonObject card;
    final JsonObject billing_details;
    final String currency;
    final String amount;

    public PaymentDetail(String receipt_email, JsonObject metadata, JsonObject card, JsonObject address, String currency, String amount) {
        this.receipt_email = receipt_email;
        this.metadata = metadata;
        this.card = card;
        this.billing_details = address;
        this.currency = currency;
        this.amount = amount;
    }
}
