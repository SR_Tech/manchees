package com.muncheese.instaapp.model.sendModel;

public class PrintOrder {

    final String restaurant_id;
    final String order_id;

    public PrintOrder(String restaurant_id, String order_id )
    {
        this.restaurant_id = restaurant_id;
        this.order_id = order_id;
    }
}
