package com.muncheese.instaapp.model.sendModel;

public class UpdateFree {
    final String sub_total;
    final String no_tax_total;
    final String restaurant_id;
    final String customer_id;
    final String custom_tip;


    public UpdateFree(String sub_total, String no_tax_total, String restaurant_id, String customer_id, String custom_tip) {
        this.sub_total = sub_total;
        this.no_tax_total = no_tax_total;
        this.restaurant_id = restaurant_id;
        this.customer_id = customer_id;
        this.custom_tip = custom_tip;
    }
}