package com.muncheese.instaapp.model.sendModel;

public class UpdateOrder {
    final String extra;
    final String tip;
    final String shipping_fee;
    final String cart_id;
    final String status;
    final String total;
    final String customer;
    final String tax;
    final String subtotal;
    final String currency;
    final String service_fee;
    final String discount;
    final String shipping_address_text;
    final String billing_address_text;


    public UpdateOrder(String extra, String tip, String shipping_fee, String cart_id, String status, String total, String customer, String tax, String subtotal, String currency, String service_fee, String discount, String shipping_address_text, String billing_address_text) {
        this.extra = extra;
        this.tip = tip;
        this.shipping_fee = shipping_fee;
        this.cart_id = cart_id;
        this.status = status;
        this.total = total;
        this.customer = customer;
        this.tax = tax;
        this.subtotal = subtotal;
        this.currency = currency;
        this.service_fee = service_fee;
        this.discount = discount;
        this.shipping_address_text = shipping_address_text;
        this.billing_address_text = billing_address_text;
    }
}