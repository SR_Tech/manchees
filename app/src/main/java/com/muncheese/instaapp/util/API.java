package com.muncheese.instaapp.util;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.muncheese.instaapp.model.ChangePasswordResponse;
import com.muncheese.instaapp.model.MainLoginResponse;
import com.muncheese.instaapp.model.cartlist.CartListResponse;
import com.muncheese.instaapp.model.fetchsize.FetchSize;
import com.muncheese.instaapp.model.orderdetail.OrderDetail;
import com.muncheese.instaapp.model.postcode.Postcoderesponse;
import com.muncheese.instaapp.model.productaddon.ProductAddon;
import com.muncheese.instaapp.model.productcontain.ProductContain;
import com.muncheese.instaapp.model.productserch.ProductSerchResponse;
import com.muncheese.instaapp.model.request.AddressUpdateRequest;
import com.muncheese.instaapp.model.request.AddresslistRequest;
import com.muncheese.instaapp.model.request.CartCheckRequest;
import com.muncheese.instaapp.model.request.CartCountRequest;
import com.muncheese.instaapp.model.request.CartListRequest;
import com.muncheese.instaapp.model.request.CategoryListRequest;
import com.muncheese.instaapp.model.request.CreateCartRequest;
import com.muncheese.instaapp.model.request.CustomerDataRequest;
import com.muncheese.instaapp.model.request.FreeRequest;
import com.muncheese.instaapp.model.request.HourRequest;
import com.muncheese.instaapp.model.request.IngredientListRequest;
import com.muncheese.instaapp.model.request.LocationRequest;
import com.muncheese.instaapp.model.request.MenuRequest;
import com.muncheese.instaapp.model.request.OrderDetailPageRequest;
import com.muncheese.instaapp.model.request.OrderDetailRequest;
import com.muncheese.instaapp.model.request.OrderListRequest;
import com.muncheese.instaapp.model.request.PrintOrderRequest;
import com.muncheese.instaapp.model.request.ProfileRequest;
import com.muncheese.instaapp.model.request.RetrarentRequest;
import com.muncheese.instaapp.model.request.SerchRequest;

import com.muncheese.instaapp.model.request.ShipmentRequest;
import com.muncheese.instaapp.model.request.StatelistRequest;
import com.muncheese.instaapp.model.request.UserCreateRequest;
import com.muncheese.instaapp.model.request.UserRegistrationRequest;
import com.muncheese.instaapp.model.request.UserRestrout;
import com.muncheese.instaapp.model.request.UseridRequest;
import com.muncheese.instaapp.model.response.MobileVerifyResponse;
import com.muncheese.instaapp.model.response.UpadateProfileResponse;
import com.muncheese.instaapp.model.sendModel.AddBillingAddress;
import com.muncheese.instaapp.model.sendModel.AddFree;
import com.muncheese.instaapp.model.sendModel.ChangePassword;
import com.muncheese.instaapp.model.sendModel.CreateCart;
import com.muncheese.instaapp.model.sendModel.EmailLogin;
import com.muncheese.instaapp.model.sendModel.MainLogin;
import com.muncheese.instaapp.model.sendModel.MobileVerification;
import com.muncheese.instaapp.model.sendModel.MobileVerify;
import com.muncheese.instaapp.model.sendModel.PrintOrder;
import com.muncheese.instaapp.model.sendModel.ProfileData;
import com.muncheese.instaapp.model.sendModel.UpdateFree;
import com.muncheese.instaapp.model.sendModel.UpdateOrder;
import com.muncheese.instaapp.model.sendModel.UserCreate;
import com.muncheese.instaapp.model.sendModel.UserRegister;
import com.muncheese.instaapp.model.sendModel.UserResetLogin;
import com.muncheese.instaapp.model.sendModel.usermodel;

import java.util.ArrayList;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface API {

    @Headers("Content-Type: application/json")
    @POST("rest-auth/login/v1/")
    Call<MainLoginResponse> login(
            @Body MainLogin login
    );

    @Headers("Content-Type: application/json")
    @POST("rest-auth/login/v1/")
    Call<MainLoginResponse> loginx(
            @Body MainLogin login
    );


    @POST("userrestaurant/")
    Call<UserRestrout> insertuser(
            @Header("Authorization") String authorization,
            @Body usermodel login
    );


    @Headers("Content-Type: application/json")
    @GET("user/")
    Call<UseridRequest> getuser(
            @Header("Authorization") String authorization,
            @Query("username") String username
    );


    @Headers("Content-Type: application/json")
    @POST("rest-auth/password/reset/v1/")
    Call<ResponseBody> password_reset(
            @Header("Authorization") String authorization,
            @Body UserResetLogin login
    );

    @Headers("Content-Type: application/json")
    @POST("forgot/user/")
    Call<ResponseBody> username_reset(
            @Header("Authorization") String authorization,
            @Body EmailLogin login
    );

    @Headers("Content-Type: application/json")
    @POST("user/")
    Call<UserRegistrationRequest> register_user(
            @Header("Authorization") String authorization,
            @Body UserRegister login
    );

    @Headers("Content-Type: application/json")
    @POST("customer/")
    Call<UserCreateRequest> create_user(
            @Header("Authorization") String authorization,
            @Body UserCreate login
    );

    @Headers("Content-Type: application/json")
    @GET("hour/")
    Call<HourRequest> gethour(
            @Header("Authorization") String authorization,
            @Query("restaurant_id") Integer id
    );

    @POST("graphql/")
    Call<CartCountRequest> cartcount(
            @Body RequestBody query
    );

    @Headers("Content-Type: application/json")
    @GET("category/")
    Call<ArrayList<CategoryListRequest>> getcategory(
            @Header("Authorization") String authorization,
            @Query("restaurant_id") String id
    );



    @Headers("Content-Type: application/json")
    @GET("catalog/?status=ACTIVE")
    Call<MenuRequest> getmenulist(
            @Header("Authorization") String authorization,
            @Query("restaurant_id") String rid,
            @Query("category_id") String cid,
            @Query("page") Integer page
    );

    @Headers("Content-Type: application/json")
    @GET("cart/")
    Call<CartCheckRequest> getcart(
            @Header("Authorization") String authorization,
            @Query("restaurant") String restaurant_id,
            @Query("customer_id") String customer_id
    );

    @Headers("Content-Type: application/json")
    @POST("cart/")
    Call<CreateCartRequest> createcart(
            @Header("Authorization") String authorization,
            @Body CreateCart createCart

    );

    @Headers("Content-Type: application/json")
    @GET("ingredient/?status=ACTIVE")
    Call<IngredientListRequest> getingredient(
            @Header("Authorization") String authorization,
            @Query("product_id") String product_id

    );


    @POST("graphql/")
    Call<ProductSerchResponse> serch_cart(
            @Body RequestBody query
    );

    @Headers("Content-Type: application/json")
    @GET("cart-item/?status=ACTIVE")
    Call<CartListRequest> getcartlist(
            @Header("Authorization") String authorization,
            @Query("cart_id") String rid,
            @Query("restaurant_id") String retroid

    );

    @Headers("Content-Type: application/json")
    @DELETE("cart-item/{id}/")
    Call<HourRequest> deletcartitem(
            @Header("Authorization") String authorization,
            @Path("id") String rid,
            @Query("product_id") String sid,
            @Query("sequence_id") String sequence_id

    );

    @Headers("Content-Type: application/json")
    @GET("billing/")
    Call<AddresslistRequest> getbillingaddress(
            @Header("Authorization") String authorization,
            @Query("customer_id") String rid
    );

    @Headers("Content-Type: application/json")
    @GET("tax/?country_code=US")
    Call<StatelistRequest> getstatelist(
            @Header("Authorization") String authorization
    );

    @Headers("Content-Type: application/json")
    @POST("billing/")
    Call<AddressUpdateRequest> addaddress(
            @Header("Authorization") String authorization,
            @Body AddBillingAddress addBillingAddress

    );

    @Headers("Content-Type: application/json")
    @PUT("billing/{id}/")
    Call<AddressUpdateRequest> updateaddress(
            @Header("Authorization") String authorization,
            @Path("id") String id,
            @Body AddBillingAddress addBillingAddress

    );

    @POST("cart-item/")
    Call<CartListRequest> addtocart(
            @Header("Authorization") String authorization,
            @Body JsonArray login
    );
    @POST("cart-item-post/")
    Call<CartListRequest> addtocartnew(
            @Header("Authorization") String authorization,
            @Body JsonObject login
    );
    @POST("fee/")
    Call<FreeRequest> getfree(
            @Header("Authorization") String authorization,
            @Body AddFree query
    );

    @POST("fee/")
    Call<FreeRequest> updatefree(
            @Header("Authorization") String authorization,
            @Body UpdateFree query
    );

    @GET("customer/")
    Call<CustomerDataRequest> getcustumer(
            @Header("Authorization") String authorization,
            @Query("customer_id") String rid
    );

    @POST("order-detail/")
    Call<OrderDetailRequest> getorderdetail(
            @Header("Authorization") String authorization,
            @Body UpdateOrder query
    );

    @GET("shipping-method/?status=ACTIVE")
    Call<ShipmentRequest> getshipmentmethod(
            @Header("Authorization") String authorization,
            @Query("restaurant") String rid
    );

    @POST("payment/")
    Call<ResponseBody> payment(
            @Header("Authorization") String authorization,
            @Body JsonObject cartUpdate
    );

    @GET("customer-payment/")
    Call<ArrayList<OrderListRequest>> orderlist(
            @Header("Authorization") String authorization,
            @Query("restaurant_id") String restaurant_id,
            @Query("customer_id") String customer_id

    );

    @GET("payment/{id}/")
    Call<OrderListRequest> ordercancel(
            @Header("Authorization") String authorization,
            @Query("id") String id


    );

    @GET("order-item/")
    Call<OrderDetailPageRequest> orderdetail(
            @Header("Authorization") String authorization,
            @Query("order_id") String order_id
    );

    @GET("customer/")
    Call<ProfileRequest> getprofile(
            @Header("Authorization") String authorization,
            @Query("customer_id") String customer_id
    );

    @PUT("user/{id}/")
    Call<UpadateProfileResponse> updateprofile(
            @Header("Authorization") String authorization,
            @Path("id") String id,
            @Body ProfileData rid
    );
    @PUT("customer/{id}/")
    Call<UpadateProfileResponse> custumerprofile(
            @Header("Authorization") String authorization,
            @Path("id") String id,
            @Body ProfileData rid
    );
    @POST("rest-auth/password/reset/confirm/v1/")
    Call<ChangePasswordResponse> changepassword(
            @Header("Authorization") String authorization,
            @Body ChangePassword rid

    );

    @GET("restaurant/{id}/")
    Call<RetrarentRequest> getabout(
            @Header("Authorization") String authorization,
            @Path("id") String id

    );

    @POST("send/code/")
    Call<ResponseBody> getotp(
            @Body MobileVerification mobileVerification

    );

    @POST("guest/verify/")
    Call<MobileVerifyResponse> verifyotp(
            @Body MobileVerify mobileVerification

    );

    @GET("restaurant/?group_name=madurai")
    Call<LocationRequest> location(
            @Header("Authorization") String authorization
    );

    @PUT("cart-item-post/{id}/")
    Call<CartListRequest> updatecart(
            @Header("Authorization") String authorization,
            @Path("id") String id,
            @Body JsonObject cartUpdate
    );

    @POST("rest-auth/logout/")
    Call<ResponseBody> logout(
            @Header("Authorization") String authorization

    );

    /////////new api////
    @Headers("Content-Type: application/json")
    @GET("cart-items-x/")
    Call<CartListResponse> getcartlistx(
            @Header("Authorization") String authorization,
            @Query("cart_id") String rid,
            @Query("restaurant_id") String retroid

    );
    @Headers("Content-Type: application/json")
    @DELETE("cart-item-x/{id}/")
    Call<HourRequest> deletecartitemx(
            @Header("Authorization") String authorization,
            @Path("id") String rid

    );

    @POST("fee-x/")
    Call<FreeRequest> getfreex(
            @Header("Authorization") String authorization,
            @Body AddFree query
    );

    @POST("order-detail-x/")
    Call<OrderDetailRequest> getorderdetailx(
            @Header("Authorization") String authorization,
            @Body UpdateOrder query
    );
    @GET("order-items-x/")
    Call<OrderDetail> orderdetailx(
            @Header("Authorization") String authorization,
            @Query("order_id") String order_id
    );
    @GET("order/")
    Call<OrderDetailPageRequest> orderx(
            @Header("Authorization") String authorization,
            @Query("order_id") String order_id
    );

    @GET("cart-items-x/")
    Call<RequestBody> cartcountx(
            @Header("Authorization") String authorization,
            @Query("cart_id") String cart_id
    );
    @Headers("Content-Type: application/json")
    @GET("shipping/")
    Call<AddresslistRequest> getshippingaddress(
            @Header("Authorization") String authorization,
            @Query("customer_id") String rid
    );

    @Headers("Content-Type: application/json")
    @POST("shipping/")
    Call<AddressUpdateRequest> addshippingaddress(
            @Header("Authorization") String authorization,
            @Body AddBillingAddress addBillingAddress

    );

    @Headers("Content-Type: application/json")
    @PUT("shipping/{id}/")
    Call<AddressUpdateRequest> updateshippingaddress(
            @Header("Authorization") String authorization,
            @Path("id") String id,
            @Body AddBillingAddress addBillingAddress

    );
    @GET("shipping-method/?status=ACTIVE")
    Call<ShipmentRequest> get_shipmentmethod(
            @Header("Authorization") String authorization,
            @Query("restaurant_id") String rid
    );

    @GET("product-size-prices-x/")
    Call<FetchSize> fetch_size(
            @Header("Authorization") String authorization,
            @Query("product_id") String productid
    );
    @GET("product-addons-x/")
    Call<ProductAddon> product_addon(
            @Header("Authorization") String authorization,
            @Query("product_id") String productid,
            @Query("size") int size
    );
    @GET("product-addons-x/")
    Call<ProductAddon> product_addonwithoutsize(
            @Header("Authorization") String authorization,
            @Query("product_id") String productid
    );


    @GET("addon-contents-x/")
    Call<ProductContain> product_contents(
            @Header("Authorization") String authorization,
            @Query("parent_addon_id") int parent_addon_id

    );


    @GET("delivery-location/")
    Call<Postcoderesponse> postcodeapi(
            @Header("Authorization") String authorization,
            @Query("restaurant_id") String restaurant_id,
            @Query("zip_code") String zip_code


    );


    @Headers("Content-Type: application/json")
    @POST("print-order-x/")
    Call<ResponseBody> printOrder(
            @Header("Authorization") String authorization,
            @Body PrintOrder printOrder
    );
}
