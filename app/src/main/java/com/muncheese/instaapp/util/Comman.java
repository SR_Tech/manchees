package com.muncheese.instaapp.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;

import com.muncheese.instaapp.R;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Comman {
    public static String currencyType = "£";
    public static String paymenycurrencyType = "GBP";
    public static void getToast(final Context context, final String txt) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast_custom, ((Activity) context).findViewById(R.id.custom_toast_container));
                TextView text = layout.findViewById(R.id.text);
                text.setText(txt);
                Toast toast = new Toast(context);
                toast.setGravity(Gravity.CENTER, 0, 40);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        });
    }
    public static boolean isConnectingToInternet(Context appContext) {
        // Method to check internet connection
        ConnectivityManager conMgr = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            // Toast.makeText(appContext, "No internet connection.", Toast.LENGTH_SHORT).show();
            showCustomDialog(appContext);
            return false;
        }


    }

    public static String ConvertIntoDateFormat(String old, String newFormat, String strdate) {

        String changeDate = null;
        try {
            DateFormat format = new SimpleDateFormat(old, Locale.ENGLISH);
            Date date = format.parse(strdate);
            SimpleDateFormat newformat = new SimpleDateFormat(newFormat, Locale.ENGLISH);
            String newDate = newformat.format(date);

            changeDate = newDate.split(" ")[1] + " " + newDate.split(" ")[0] + ", " + newDate.split(" ")[2];
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return changeDate;
    }

    @SuppressLint("WrongViewCast")
    public static void showCustomDialog(final Context appContext ) {
        final Dialog dialog = new Dialog(appContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_warning);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
    public static boolean progressbar(Context appContext,String progress) {
        Dialog dialog = null;
        if (progress.equals("diss")) {
            assert dialog != null;
            dialog.dismiss();
            return true;
        } else {
         ProgressDialog.show(appContext, "Please wait", "Loading...");
            return false;
        }


    }

    // Function to convert ArrayList<String> to String[]
    public static String[] GetStringArray(ArrayList<String> arr) {
        // declaration and initialise String Array
        String str[] = new String[arr.size()];
        // ArrayList to Array Conversion
        for (int j = 0; j < arr.size(); j++) {
            // Assign each value to String array
            str[j] = arr.get(j);
        }


        return str;
    }

    public static String CurrentTimeStamp() {
        //Date object
        Date date = new Date();
        //getTime() returns current time in milliseconds
        long time = date.getTime();
        //Passed the milliseconds to constructor of Timestamp class
        Timestamp ts = new Timestamp(time);
        return ts.toString();
    }
    public static String convertToUSDFormat(String value) {

        DecimalFormat numberFormat = new DecimalFormat("######0.00");
        String convertedValue = numberFormat.format(Double.valueOf(value));
        return convertedValue;
    }

    public static final String EMAIL_PATTERN = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+";
    public static Pattern pattern;
    static Matcher matcher;

    public static boolean isEnailValid(String email) {
        // method to check edit text is fill or no
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email.trim());
        if (matcher.matches()) {
            return false;
        }
        return true;
    }
}


