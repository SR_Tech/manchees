package com.muncheese.instaapp.util;

/**
 * Created by JASS-3 on 5/19/2018.
 */

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;


public class Preferences {

    private final static String preferencesName = "The MyPref";//swapnil
    public static Context appContext;

    public static  void cleare(){
        SharedPreferences.Editor pref = appContext.getSharedPreferences("The MyPref", 0).edit();
        pref.clear();
        pref.commit();
    }



    public static void setUserId(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setUser", val);
        editor.commit();
    }
    public static String getUserId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setUser", null);
        return value;
    }
    public static void setUserProfile(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setUserId", val);
        editor.commit();
    }
    public static String getUserProfile() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setUserId", null);
        return value;
    }

    public static void setToken(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("settoken", val);
        editor.commit();
    }
    public static String getToken() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("settoken", null);
        return value;
    }

    public static void setstatus(int val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putInt("setstatus", val);
        editor.commit();
    }
    public static int getstatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        int value = prefs.getInt("setstatus", 0);
        return value;
    }
    public static void setLoginStatus(boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("loginStatus",value);
        //editor.putString("getCurrentUserId", value);
        editor.commit();
    }
    public static boolean getLoginStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("loginStatus", false);
        return value;
    }


    public static void setcartId(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setcartid", val);
        editor.commit();
    }
    public static String getcartId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setcartid", null);
        return value;
    }

    public static void setcarttotal(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setcarttotal", val);
        editor.commit();
    }
    public static String gettotal() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setcarttotal", null);
        return value;
    }

    public static void setusername(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setusername", val);
        editor.commit();
    }
    public static String getusername() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setusername", null);
        return value;
    }


    public static void setcartcount(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setcartcount", val);
        editor.commit();
    }
    public static String getcartcount() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setcartcount", null);
        return value;
    }
    public static void setpickup(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setpickup", val);
        editor.commit();
    }
    public static String getpickup() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setpickup", null);
        return value;
    }

    public static void setshippingid(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setshippingid", val);
        editor.commit();
    }
    public static String getshippingid() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setshippingid", null);
        return value;
    }

    public static void setpostcode(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setpostcode", val);
        editor.commit();
    }
    public static String getpostcode() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setpostcode", null);
        return value;
    }


    public static void setpizzacolle(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setpizzacolle", val);
        editor.commit();
    }
    public static String getpizzacolle() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setpizzacolle", null);
        return value;
    }

    public static void setpizzdeliver(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setpizzdeliver", val);
        editor.commit();
    }
    public static String getpizzadeliver() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setpizzdeliver", null);
        return value;
    }

    public static void setpizz2deliver(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setpizz2deliver", val);
        editor.commit();
    }
    public static String getpizz2adeliver() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setpizz2deliver", null);
        return value;
    }


    public static void setdips(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setdips", val);
        editor.commit();
    }
    public static String getdips() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setdips", null);
        return value;
    }
}

